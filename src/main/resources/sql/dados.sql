INSERT INTO auth.OAUTH_CLIENT_DETAILS(CLIENT_ID, RESOURCE_IDS, CLIENT_SECRET, SCOPE, AUTHORIZED_GRANT_TYPES, AUTHORITIES, ACCESS_TOKEN_VALIDITY, REFRESH_TOKEN_VALIDITY)
 VALUES ('ecge', 'resource-server-rest-api', '$2a$04$soeOR.QFmClXeFIrhJVLWOQxfHjsJLSpWrU1iGxcMGdu.a5hvfY4W','read,write', 'password,authorization_code,refresh_token,implicit', 'USER', 10800, 2592000);
-- /*spring-security-oauth2-read-write-client-password1234*/'$2a$04$soeOR.QFmClXeFIrhJVLWOQxfHjsJLSpWrU1iGxcMGdu.a5hvfY4W',


INSERT INTO auth.Usuario
(IdUsuario, NomUsuario,                  DescLogin,                      EmlUsuario,                 ChaveApi,                           FlgAtivo, PwdSenha)
VALUES
--senha: admin1234 = $2a$08$qvrzQZ7jJ7oy2p/msL4M0.l83Cd0jNsX6AJUitbgRXGzge4j035ha
(1,        'Renan',                      'renanlf',                  'email@cgu.gov.br',             null,                                1,       '$2a$08$qvrzQZ7jJ7oy2p/msL4M0.l83Cd0jNsX6AJUitbgRXGzge4j035ha'),
(2,        'Gustavo Gomes Teixeira',     'ggomest',                  'gustavo.teixeira@cgu.gov.br',  '0fadfb8fd4f7cc48f04eafa048c32602',  1,       '$2a$08$qvrzQZ7jJ7oy2p/msL4M0.l83Cd0jNsX6AJUitbgRXGzge4j035ha'),
(3,        'Iuri de Moura Carneiro',     'iurimc',                   'email@cgu.gov.br',             null,                                1,       '$2a$08$qvrzQZ7jJ7oy2p/msL4M0.l83Cd0jNsX6AJUitbgRXGzge4j035ha'),
(4,        'Rodrigo',                    'rodrigovfs',               'email@cgu.gov.br',             null,                                1,       '$2a$08$qvrzQZ7jJ7oy2p/msL4M0.l83Cd0jNsX6AJUitbgRXGzge4j035ha'),
(5,        'Vinicius',                   'viniciusmab',              'email@cgu.gov.br',             null,                                1,       '$2a$08$qvrzQZ7jJ7oy2p/msL4M0.l83Cd0jNsX6AJUitbgRXGzge4j035ha'),
(6,        'Anselmo Júlio da Rocha',     'anselmojr',                'anselmo@cgu.gov.br',           'a51a2147060988aeb0a80a42847b759d',  1,       '$2a$08$qvrzQZ7jJ7oy2p/msL4M0.l83Cd0jNsX6AJUitbgRXGzge4j035ha'),
(7,        'Marcelo Abranches',          'marceloca',                'marcelo.abranches@cgu.gov.br', null,                                1,       '$2a$08$qvrzQZ7jJ7oy2p/msL4M0.l83Cd0jNsX6AJUitbgRXGzge4j035ha'),
(8,        'Marcelo Polo',               'marcelopf',                'marcelo.faria@cgu.gov.br',     null,                                1,       '$2a$08$qvrzQZ7jJ7oy2p/msL4M0.l83Cd0jNsX6AJUitbgRXGzge4j035ha'),
(9,        'Fulano Desativado',          'fulanodesativado',         'fulano.desativado@cgu.gov.br', null,                                0,       '$2a$08$qvrzQZ7jJ7oy2p/msL4M0.l83Cd0jNsX6AJUitbgRXGzge4j035ha'),
(25,       'Gilberto',                   'gilbertoss',               'email@cgu.gov.br',             null,                                1,       '$2a$08$qvrzQZ7jJ7oy2p/msL4M0.l83Cd0jNsX6AJUitbgRXGzge4j035ha'),
(26,       'Fábio Sampaio da Costa',     'fabiosc',                  'fabio.costa@cgu.gov.br',       '0fadfb8fd4f7cc48f04eafa048c32603',  1,       '$2a$08$qvrzQZ7jJ7oy2p/msL4M0.l83Cd0jNsX6AJUitbgRXGzge4j035ha'),
(27,       'SISTEMA ACESSO',             'sistema_acesso',           'cosis@cgu.gov.br',             '48649b9b64ceab783f5104221b219fcd',  1,       '$2a$08$qvrzQZ7jJ7oy2p/msL4M0.l83Cd0jNsX6AJUitbgRXGzge4j035ha'),
(11,       'Teste ADMIN 1',              'USER_CGSIS_TESTE_01',      'cosis@cgu.gov.br',             null,                                1,       '$2a$08$qvrzQZ7jJ7oy2p/msL4M0.l83Cd0jNsX6AJUitbgRXGzge4j035ha'),
(12,       'Teste CGSIS 2',              'USER_CGSIS_TESTE_02',      'cosis@cgu.gov.br',             null,                                1,       '$2a$08$qvrzQZ7jJ7oy2p/msL4M0.l83Cd0jNsX6AJUitbgRXGzge4j035ha'),
(13,       'Teste CGSIS 3',              'USER_CGSIS_TESTE_03',      'cosis@cgu.gov.br',             null,                                1,       '$2a$08$qvrzQZ7jJ7oy2p/msL4M0.l83Cd0jNsX6AJUitbgRXGzge4j035ha'),
(14,       'Teste CGSIS 4',              'USER_CGSIS_TESTE_04',      'cosis@cgu.gov.br',             null,                                1,       '$2a$08$qvrzQZ7jJ7oy2p/msL4M0.l83Cd0jNsX6AJUitbgRXGzge4j035ha'),
(15,       'Teste CGSIS 5',              'USER_CGSIS_TESTE_05',      'cosis@cgu.gov.br',             null,                                1,       '$2a$08$qvrzQZ7jJ7oy2p/msL4M0.l83Cd0jNsX6AJUitbgRXGzge4j035ha'),
(16,       'Teste CGSIS 6',              'USER_CGSIS_TESTE_06',      'cosis@cgu.gov.br',             null,                                1,       '$2a$08$qvrzQZ7jJ7oy2p/msL4M0.l83Cd0jNsX6AJUitbgRXGzge4j035ha'),
(17,       'Teste CGSIS 7',              'USER_CGSIS_TESTE_07',      'cosis@cgu.gov.br',             null,                                1,       '$2a$08$qvrzQZ7jJ7oy2p/msL4M0.l83Cd0jNsX6AJUitbgRXGzge4j035ha'),
(18,       'Teste CGSIS 8',              'USER_CGSIS_TESTE_08',      'cosis@cgu.gov.br',             null,                                1,       '$2a$08$qvrzQZ7jJ7oy2p/msL4M0.l83Cd0jNsX6AJUitbgRXGzge4j035ha'),
(19,       'Teste CGSIS 9',              'USER_CGSIS_TESTE_09',      'cosis@cgu.gov.br',             null,                                1,       '$2a$08$qvrzQZ7jJ7oy2p/msL4M0.l83Cd0jNsX6AJUitbgRXGzge4j035ha'),
(20,       'Teste CGSIS 10 (expirado)',  'USER_CGSIS_TESTE_10',      'cosis@cgu.gov.br',             null,                                1,       '$2a$08$qvrzQZ7jJ7oy2p/msL4M0.l83Cd0jNsX6AJUitbgRXGzge4j035ha'),
(21,       'Márcio Pinto Ávalos',        'mavalos',                  'marcio.avalos@cgu.gov.br',     null,                                1,       '$2a$08$qvrzQZ7jJ7oy2p/msL4M0.l83Cd0jNsX6AJUitbgRXGzge4j035ha'),
(22,       'Heleno',                     'helenob',                  'heleno.borges@cgu.gov.br',     null,                                1,       '$2a$08$qvrzQZ7jJ7oy2p/msL4M0.l83Cd0jNsX6AJUitbgRXGzge4j035ha'),
(23,       'Daniel Wu',                  'danielyhw',                'daniel.wu@cgu.gov.br',         null,                                1,       '$2a$08$qvrzQZ7jJ7oy2p/msL4M0.l83Cd0jNsX6AJUitbgRXGzge4j035ha'),
(24,       'Teste CGTEC 1',              'USER_CGTEC_TESTE_01',      'cgtec@cgu.gov.br',             null,                                1,       '$2a$08$qvrzQZ7jJ7oy2p/msL4M0.l83Cd0jNsX6AJUitbgRXGzge4j035ha');

insert into auth.Sistema
(IdSistema, NomSistema,         FlgAtivo)
values
(1,         'ECGE Autorizador', 1),
(2,         'ECGE Nota fiscal', 1),
(3,         'ECGE Contabil',    1),
(4,         'ECGE Inativo',     0);

INSERT INTO auth.Permissao
(IdPermissao, DescPermissao,                IdSistema, FlgAtivo)
VALUES
(0,         'CONSULTAR_USUARIOS',             1,         1),
(1,         'GERENCIAR_USUARIOS',            1,         1),
(2,         'CONSULTAR_PERMISSOES',           1,         1),
(3,         'GERENCIAR_PERMISSOES',          1,         1),
(4,         'CONSULTAR_SISTEMAS',             1,         1),
(5,         'GERENCIAR_SISTEMAS',            1,         1),
(6,         'MENU_AUDITORIA_CONTINUA',       2,         1),
(7,         'MENU_UNIVERSO_AUDITAVEL',       3,         1),
(8,         'MENU_PLANEJAMENTO',             3,         0);


INSERT INTO auth.PermissoesDoUsuario
(IdUsuario, IdPermissao)
VALUES
(1,         0),
(1,         1),
(1,         2),
(1,         3),
(1,         4),
(1,         5),
(1,         6),
(1,         7),
(1,         8),
(2,         0),
(2,         1),
(2,         2),
(3,         0),
(3,         1),
(4,         0),
(4,         1),
(4,         2),
(5,         0),
(5,         1),
(5,         2),
(6,         0),
(6,         1),
(7,         0),
(8,         0),
(11,        0),
(11,        1),
(18,        3),
(19,        4),
(22,        0),
(22,        1),
(23,        0),
(23,        1),
(23,        2),
(23,        3),
(24,        2),
(25,        0),
(26,        0),
(27,        2);

