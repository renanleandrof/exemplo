const dataTables             = window.dataTables || {};
dataTables.filtrosDaPesquisa = {};
dataTables.filtrosIgnorados  = "[data-ignorar-filtro-utilizado='true'], div[id^=filtroComplementar] :input";

dataTables.prepararRequest = function(data) {
    data.colunaOrdenacao = data.columns[data.order[0].column].name;
    data.direcaoOrdenacao = data.order[0].dir.toUpperCase();
    data.tamanhoPagina = data.length;
    data.offset = data.start;

    delete data.length;
    delete data.start;
    delete data.columns;
    delete data.order;
    delete data.search;
    delete data.draw;
};

dataTables.bindPesquisaDatatables = function() {
    $("[data-datatables-refresh]").click(function () {
        dataTables.click($(this).data("datatables-refresh"));
    });
};

dataTables.click = function(id) {
    $(id).dataTable().fnPageChange(0);
    dataTables.atualizarUrl(id);
    dataTables.atualizarFiltrosDaPesquisa();
};

dataTables.atualizarUrl = function(idDataTables) {
    const $lista = $(idDataTables);
    const queryString = $.param($lista.DataTable().ajax.params());
    window.history.pushState(queryString, window.title, window.location.href.split("?")[0] + "?" + queryString);
    window.location.href = "#lista";
};

/* Function necessária pra que a ordenação do Datatables clientside funcione para colunas com input's.
 Adicionar orderDataType: "dom-input"  na definição da column.
 */
$.fn.dataTable.ext.order["data-src"] = function () {
    const that = this;
    return this.api().data().map(function (v) { return v[that.api().column().dataSrc()]; });
};

dataTables.defaultOptions = function () {
    const options = {
        "searching": false,
        "lengthChange": true,
        "language": {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Página _PAGE_ de _PAGES_",
            "sInfoEmpty": "",
            "sInfoFiltered": "",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_",
            "sLoadingRecords": `Carregando... 
                        <div class="sk-cube-grid">
                           <div class="sk-cube sk-cube1"></div>
                           <div class="sk-cube sk-cube2"></div>
                           <div class="sk-cube sk-cube3"></div>
                           <div class="sk-cube sk-cube4"></div>
                           <div class="sk-cube sk-cube5"></div>
                           <div class="sk-cube sk-cube6"></div>
                           <div class="sk-cube sk-cube7"></div>
                           <div class="sk-cube sk-cube8"></div>
                           <div class="sk-cube sk-cube9"></div>
                        </div>`,
            "sProcessing": `Carregando... 
                        <div class="sk-cube-grid">
                           <div class="sk-cube sk-cube1"></div>
                           <div class="sk-cube sk-cube2"></div>
                           <div class="sk-cube sk-cube3"></div>
                           <div class="sk-cube sk-cube4"></div>
                           <div class="sk-cube sk-cube5"></div>
                           <div class="sk-cube sk-cube6"></div>
                           <div class="sk-cube sk-cube7"></div>
                           <div class="sk-cube sk-cube8"></div>
                           <div class="sk-cube sk-cube9"></div>
                        </div>`,
            "sZeroRecords": "Nenhum registro encontrado",
            "oPaginate": {
                "sNext": "Próxima <i class=\"fa fa-fw fa-chevron-right\"></i> ",
                "sPrevious": "<i class=\"fa fa-fw fa-chevron-left\"></i> Anterior</a>",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            "oAria": {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            },
            select: {rows: {0: "", _: "%d itens selecionados", 1: "1 item selecionado"}}
        },
        "serverSide": true,
        processing: true,
        "orderMulti": false,
        "lengthMenu": [[15, 30, 50], ["15 resultados por página", "30 resultados por página", "50 restultados por página"]],
        "deferRender": true,
        autoWidth: true,
        dom: "tr<'row'<'col-sm-3'i><'col-sm-7'p><'col-sm-2'l>>",
        filtrador: function () {
        },
        initComplete: function () {
            dataTables.bindPesquisaDatatables();
            dataTables.atualizarFiltrosDaPesquisa();
        }
    };

    const singleSelection = cgu.getQueryStringParam("singleSelection");
    const botaoSelecionarTodos = $("#btnSelectAll");
    if (!cgu.isNullOuUndefined(singleSelection) && !cgu.isNullOuUndefined(botaoSelecionarTodos)) {
        options.select = {style: "multi"};
        if (singleSelection === "true") {
            botaoSelecionarTodos.hide();
            options.select = {style: "single"};
        }
    }

    options.ajax = {
        data: function(data) {
            dataTables.prepararRequest(data);
            options.filtrador(data);
        }
    };

    return options;
};

dataTables.eventoSelectAll = function (idDaTabela, element) {
    let $i = $(element).find("i");
    const selecionado = $i.hasClass("fa-check-square");

    if (selecionado) {
        $i.attr("class","far fa-square");
        $("#" + idDaTabela).DataTable().rows().deselect();
    } else {
        $i.attr("class","fas fa-check-square");
        $("#" + idDaTabela).DataTable().rows().select();
    }
};


dataTables.isSelecionado = function (coluna) {
    let $colunasSelecionadas = $("#colunasSelecionadas");
    if ($colunasSelecionadas.size() > 0) {
        return $colunasSelecionadas.val().indexOf(coluna) !== -1;
    }
    return true;
};

dataTables.defaultOptionsSemAjax = function () {
    const options = dataTables.defaultOptions();
    options.serverSide = false; //Não queremos ajax nessa tabela
    delete options.ajax;
    delete options.initComplete; //faz nada. Não queremos que esse datatables atualize a url.
    options.data = [];
    options.order = [[0, "asc"]];
    options.paging = false;
    options.dom = "<'row'<'col-sm-12'l><'col-sm-12'f>><'row'<'col-sm-12'tr>>";
    return options;
};


//renderer recebe (data, type, row, meta )
dataTables.dateRenderer = function (data) {
    if (cgu.isNullOuUndefined(data)) {
        return "";
    }
    const date = Date.parseExact(data[2] + "/" + data[1] + "/" + data[0], "d/M/yyyy");
    return date.toString("dd/MM/yyyy");
};

dataTables.dateTimeRenderer = function (data) {
    if (cgu.isNullOuUndefined(data)) {
        return "";
    }
    const date = Date.parseExact(data[2] + "/" + data[1] + "/" + data[0] + " " + data[3] + ":" + data[4] + ":" + data[5], "d/M/yyyy H:m:s");
    return date.toString("dd/MM/yyyy HH:mm:ss");
};

dataTables.timeRenderer = function (data) {
    if (cgu.isNullOuUndefined(data)) {
        return "";
    }
    const time = Date.parse(data.hourOfDay + ":" + data.minuteOfHour + ":" + data.secondOfMinute);
    return time.toString("HH:mm:ss");
};

dataTables.linkRenderer = function (link, descricao) {
     return "<a href='" + link + "'>" + descricao + "</a>";
};

dataTables.codigoEDescricaoRenderer = function (codigo, descricao) {
    if (codigo[0] === "-") {
        return descricao;
    }
    return codigo + " - " + descricao;
};

dataTables.valorComCifraoRender = function(data) {
    if (cgu.isNullOuUndefined(data) || data === "") {
        return "";
    }
    return "R$ " + cgu.formatarNumber(data);
};

dataTables.selectRender = function(data, options, onChange, atributos) {
    atributos = atributos || "";
    onChange = onChange || "";
    let optionsGeradas = "";
    options.forEach(function (v) {
        let selecionado = v.id == data ? " selected='selected'":"";
        optionsGeradas += cgu.format("<option value='{0}' {1}>{2}</option>", v.id, selecionado, v.value);
    });
    return `<span>
                <label>
                    <select ${atributos} onchange="${onChange}" class="form-control">
                    ${optionsGeradas}
                    </select>
                </label>                                
            </span>`;
};

dataTables.checkboxRender = function(data, onChange, atributos) {
    atributos = atributos || "";
    onChange = onChange || "";
    return `<span class="checkbox c-checkbox needsclick c-checkbox-no-font">
                <label class="needsclick">
                    <input class="needsclick" type="checkbox" ${data ? "checked='checked'" : ""} ${atributos} onchange="${onChange}">
                    <span class="glyphicon glyphicon-ok"></span>
                </label>                                
            </span>`;
};

dataTables.colunaDeSelecao = function(exibirSelecao) {
    return {name:"selecionar", sortable:false, visible:exibirSelecao, className:"select-checkbox dt-center", width:"10px", render:function(){return "";}};
};

//TODO: Fazer todas telas usarem isso aqui ao inves só do render.
dataTables.colunaTarefaFavorita = ()=> {
    return {name: "favorita", render: dataTables.marcacaoTarefaFavoritaRender, sortable:false, className:"dt-center", width:"15px", visible: dataTables.isSelecionado("id")};
};

dataTables.usarSelecionados = function(idDataTables){
    const idsSelecionados = $("#" + idDataTables).DataTable().rows({selected: true}).data().toArray().map(function (v) {
        return cgu.isNullOuUndefined(v.id) ? v[0] : v.id;
    });
    window.opener.postMessage({windowName: window.name, idsSelecionados : idsSelecionados}, "*");
    window.close();
};

dataTables.marcacaoTarefaFavoritaRender = function(data, type, row){
    return cgu.format(
        "<span id='alternar-favorita-{1}' title='Clique para {3}marcar como favorita' class='favorito' data-id-tarefa='{1}' data-favorita='{2}' " +
        "data-toggle='tooltip' data-placement='right'><i aria-hidden='true' class='fa{4} fa-star fa-lg'></i></span>",
        springUrl, row.id, row.favorita, row.favorita ? "des" : "", row.favorita ? "s" : "r");
};

dataTables.starabilityRender = function(data, type, id){
    let htmlNota = `
        <fieldset class="starability-growRotate">
            <input type="radio" data-itemId="${id}" id="nota_${id}_no-rate" name="nota_${id}_rating" value="0" class="input-no-rate" checked=""
                    aria-label="No rating."  disabled="disabled" />
        `;

        for (let i = 1; i <= 6; i++) {
            htmlNota += `
                    <input type="radio" data-itemId="${id}" id="nota_${id}_rate${i}" name="nota_${id}_rating" value="${i}"
                        ${data == i ? 'checked' : ''} disabled="disabled" />
                    <label for="nota_${id}_rate${i}">${i} star${i > 1 ? "s" : ""}</label>
            `;
        }

        htmlNota += `</fieldset>`;

    return htmlNota;
};

dataTables.idTarefaComLinkRender = function(data, type, row){
    const label = `<span class='label label-id'>#${data}</span>`;
    if (window.location.href.indexOf("/popup/") !== -1) {
        return label;
    } else {
        return `<a href='${springUrl}auth/tarefa/${data}' title='Clique para ver os detalhes' data-toggle='tooltip' data-placement='right'>${label}</a>`;
    }
};

dataTables.situacaoRender = function(data){
    return "<span class='label label-situacao'>" + data + "</span>";
};

dataTables.assuntoRender = function(data){
    return "<span class='label label-assunto'>" + data + "</span>";
};

dataTables.tipoInteracaoRender = function(data){
    return "<span class='label label-purple tipo-interacao-tarefa'>" + data + "</span>";
};

dataTables.prepararBtnSalvarFiltro = function() {
    let $btnSalvarFiltro = $("#btnSalvarFiltro");
    $btnSalvarFiltro.hide();
    $btnSalvarFiltro.click(function () {
        if (window.sidebar && window.sidebar.addPanel) {
            window.sidebar.addPanel(document.title, window.location.href, '');
        } else if (window.external && ('AddFavorite' in window.external)) {
            window.external.AddFavorite(location.href, document.title);
        } else if (window.opera && window.print) {
            this.title = document.title;
            return true;
        } else {
            alert("Pressione " + (navigator.userAgent.toLowerCase().indexOf('mac') !== -1 ? "Command/Cmd" : "CTRL") + " + D para adicionar aos favoritos.");
        }
    });
};

dataTables.atualizarFiltrosDaPesquisa = function() {
    let painelFiltrosDaPesquisa = $("#painel-filtros-da-pesquisa");
    if(painelFiltrosDaPesquisa) {
        dataTables.filtrosDaPesquisa = {};

        let form = $("#formFiltros");
        dataTables.atualizarCamposTexto(form);
        dataTables.atualizarCamposComboBox(form);
        dataTables.atualizarCamposCheckBox(form);
        dataTables.atualizarCamposMultiselect(form);
        dataTables.atualizarCamposAutocomplete(form);
        dataTables.atualizarCamposFiltrosDinamicos(form);
        dataTables.atualizarSumarioFiltrosDaPesquisa(painelFiltrosDaPesquisa);
    }
};

dataTables.atualizarCamposTexto = function(form) {
    let camposTexto = form.find("input.form-control").not(".multiselect-search, [data-autocomplete-tipo], " + dataTables.filtrosIgnorados);
    camposTexto.each(function() {
        let campo = $(this);
        if(!cgu.isStringVaziaNullOuUndefined(campo.val())) {
            let htmlId    = campo.attr("id");
            let label     = $("label[for='"+  htmlId  + "']").toArray().reverse();
            let descricao = $("[data-descricao-campo-for*='" + htmlId +  "']");

            dataTables.criarItemFiltroDaPesquisa(
                htmlId,
                $(label).first().text(),
                descricao.text(),
                [{ "id": campo.val(), "texto": campo.val() }]
            );
        }
    });
};

dataTables.atualizarCamposComboBox = function(form) {
    let camposComboBox = form.find("select.form-control").not(dataTables.filtrosIgnorados);
    camposComboBox.each(function() {
        let campo                = $(this);
        let itemSelecionado      = campo.find(":selected");
        let textoItemSelecionado = itemSelecionado.text();
        let idItemSelecionado    = itemSelecionado.val();
        if(!cgu.isStringVaziaNullOuUndefined(idItemSelecionado)) {
            let htmlId    = campo.attr("id");
            let label     = $("label[for='"+  htmlId  + "']").toArray().reverse();
            let descricao = "";

            dataTables.criarItemFiltroDaPesquisa(
                htmlId,
                $(label).first().text(),
                descricao,
                [{ "id": idItemSelecionado, "texto": textoItemSelecionado }]
            );
        }
    });
};

dataTables.atualizarCamposCheckBox = function(form) {
    let camposComboBox = form.find("input.inputSwitch").filter(":checked").not(dataTables.filtrosIgnorados);
    camposComboBox.each(function () {
        let campo = $(this);
        let labelCampo = campo.parent("label").text();
        if (!cgu.isStringVaziaNullOuUndefined(labelCampo)) {
            let htmlId = campo.attr("id");
            dataTables.criarItemFiltroDaPesquisa(htmlId, "", "", [{"id": labelCampo, "texto": labelCampo}]);
        }
    });
};

dataTables.atualizarCamposMultiselect = function(form) {
    let camposMultiselect = form.find("select[multiple]").not(dataTables.filtrosIgnorados);
    camposMultiselect.each(function() {
        let campo = $(this);
        let itensSelecionados = campo.find(":selected").map(function(i, item) {
            return { "id": $(item).val(), "texto": $(item).text() };
        }).toArray();

        if(!cgu.isArrayVazioNullOuUndefined(itensSelecionados)) {
            let htmlId    = campo.attr("id");
            let label     = $("label[for='"+  htmlId  + "']").toArray().reverse();

            dataTables.criarItemFiltroDaPesquisa(htmlId, $(label).first().text(), "", itensSelecionados);
        }
    });
};

dataTables.atualizarCamposAutocomplete = function(form) {
    let camposAutocomplete = form.find("[data-autocomplete-tipo]").not(dataTables.filtrosIgnorados);
    camposAutocomplete.each(function() {
        let campo   = $(this);
        let itensSelecionados = campo.tokenInput("get").map(function(item) {
            return { "id": item.id, "texto": item.name };
        });

        if(!cgu.isArrayVazioNullOuUndefined(itensSelecionados)) {
            let htmlId    = campo.attr("id");
            let label     = $("label[for='"+  htmlId  + "']").toArray().reverse();

            dataTables.criarItemFiltroDaPesquisa(htmlId, $(label).first().text(), "", itensSelecionados);
        }
    });
};

dataTables.atualizarCamposFiltrosDinamicos = function(form) {
    let filtrosDinamicos = form.find("div[id^=filtroComplementar]");
    filtrosDinamicos.each(function() {
        let grupoFiltro = $(this);
        let campoFiltro =  grupoFiltro.find("[id^=campo_]").not("div");
        let valorCampo  = dataTables.determinarValorCampoDinamico(campoFiltro);

        if(valorCampo.length > 0) {
            let htmlId = grupoFiltro.attr("id");
            let labelCampo = grupoFiltro.find("input[id^=campoFiltro]").tokenInput("get").map(function(item) {
                return item.name;
            }).toString();
            let operadorCampo = grupoFiltro.find("select[id^=operadorFiltro] :selected").text();

            dataTables.criarItemFiltroDaPesquisa(htmlId, labelCampo + " - " + operadorCampo, "", valorCampo);
        }
    });
};

dataTables.determinarValorCampoDinamico = function(campo) {
  if(campo.is("[data-autocomplete-tipo]")) {
      return campo.tokenInput("get").map(function(item) {
          return { "id": item.id, "texto": item.name };
      });
  }

  if(campo.is(":checkbox")) {
      return [{"id": "","texto": campo.is(":checked") ? "SIM" : "NÃO" }];
  }

  if(campo.is("select")) {
      return campo.find(":selected").map(function(i, v) {
          return { "id": $(v).val(), "texto": $(v).text() };
      });
  }

  if(campo.is("input") || campo.is("textarea")) {
      if(!cgu.isStringVaziaNullOuUndefined(campo.val())) {
          return [{"id": campo.val(), "texto": campo.val()}];
      }
      return [];
  }
};

dataTables.criarItemFiltroDaPesquisa = function(id, label, descricao, valores) {
    dataTables.filtrosDaPesquisa[id]            = {};
    dataTables.filtrosDaPesquisa[id].label      = label;
    dataTables.filtrosDaPesquisa[id].descricao  = descricao;
    dataTables.filtrosDaPesquisa[id].valores    = valores;
};

dataTables.atualizarSumarioFiltrosDaPesquisa = function(painel) {
    painel.empty();
    $.each(dataTables.filtrosDaPesquisa, function(id, filtro) {
        let descricaoSumario = `${filtro.descricao} ${filtro.label}`;
        if(!cgu.isStringVaziaNullOuUndefined(descricaoSumario) && descricaoSumario.length > 1) {
            descricaoSumario += ":";
        }

        let campo = $(`<div class='itens-filtro'>${descricaoSumario} </div>`);
        $.each(filtro.valores, function(index, valor) {
            $(`<button type='button' class='btn btn-sm btn-primary btn-labeled' data-filtro-valor='${valor.id}' 
                            data-filtro-texto='${valor.texto}' data-filtro-name='#${id}'>${valor.texto}
                            <span class='btn-sm btn-label btn-label-right'> <i class='fa fa-times' /></span>
                          </button>`).appendTo(campo);
        });
        campo.appendTo(painel);
    });

    painel.find("button").each(function() {
        let botao = $(this);
        botao.on("click", function() {
            let nameFiltro  = botao.data("filtro-name");
            let valorFiltro = botao.data("filtro-valor");
            let campo       = $(nameFiltro);

            removerFiltroDoSumario(botao);
            limparCampoFiltro(campo, valorFiltro);

            let idDatatables = "#" + $("table.dataTable").attr("id");
            dataTables.click(idDatatables);
        });


        function removerFiltroDoSumario(botao) {
            let listaDeItens = botao.closest("div.itens-filtro");
            let quantidadeDeItens = listaDeItens.find("button").length;
            if(quantidadeDeItens === 1) listaDeItens.remove();
            else botao.remove();
        }

        function limparCampoFiltro(campo, valorFiltro) {
            if(campo.is("select") && campo.attr("multiple")) {
                campo.multiselect("deselect", valorFiltro);
            } else if(!cgu.isStringVaziaNullOuUndefined(campo.data("autocomplete-tipo"))) {
                campo.tokenInput("remove",  {id: valorFiltro.toString()}, {useFocus: false});
            } else if(campo.is("div[id^=filtroComplementar]")) {
                removerCampoFiltroDinamico(campo, valorFiltro);
            } else if (campo.is(":checkbox")) {
                campo.prop("checked", false);
            } else {
                campo.val("");
            }
        }

        function removerCampoFiltroDinamico(campo, valorFiltro) {
            if(campo.find("select[id^=campo_]").is("select[multiple]")) {
                let campoSelect = campo.find("select[id^=campo_]");
                let selecionados = campoSelect.val().filter(function(valorSelecionado) {
                    return Number(valorSelecionado) !== valorFiltro;
                });
                if(selecionados.length === 0) campo.remove();
                else campoSelect.val(selecionados);

                return;
            }

            if(campo.find("input[id^=campo_]").is("[data-autocomplete-tipo]")) {
                let campoAutocomplete = campo.find("input[id^=campo_]");
                campoAutocomplete.tokenInput("remove", {id: valorFiltro.toString()}, {useFocus: false});
                if(campoAutocomplete.tokenInput("get").length === 0) campo.remove();

                return;
            }

            campo.remove();
        }
    });

    dataTables.incluirCheckboxSelecionarTodos = function(idTabela) {
        let htmlSelectAll = "<span id='btnSelectAll'><i class='far fa-square'></i></span>";

        // Adicionando na primeira célula do thead o checkbox.
        $(`#${idTabela} thead tr th:first-child`).append(htmlSelectAll);

        // Adicionando ao checkbox o evento que selecionada todas as linhas da tabela.
        $("#btnSelectAll").attr("onClick", `dataTables.eventoSelectAll("${idTabela}", this)`);

    }
};