const Multiselect = {};

Multiselect.optionsPadrao = {
      enableFiltering: true
    , filterPlaceholder: "Digite para começar a filtrar"
    , nonSelectedText: "Nenhum selecionado"
    , nSelectedText: " selecionados"
    , allSelectedText: "Todos selecionados"
    , maxHeight: 300
    , buttonWidth: "100%"
    , buttonClass: "btn"
    , enableCaseInsensitiveFiltering: true
    , templates : {
        filter : `<li class="multiselect-item filter"><input class="form-control multiselect-search" type="text"></li>`,
        filterClearBtn: `<span class="input-group-btn"><button class="btn btn-default multiselect-clear-filter" type="button">
                        <i class="fas fa-eraser"></i></button></span>`

    }
};

Multiselect.criarMultiselects = function () {
    $("select[multiple]").each(function () {
        let $select = $("#" + this.id);
        $.extend(Multiselect.optionsPadrao, Multiselect.getOnChangeCallback($select));

        $select.multiselect(Multiselect.optionsPadrao);
        $select.siblings().filter(".btn-group").attr('id',   this.id + "_btn-group");
    });
};

Multiselect.getOnChangeCallback = function ($select) {
    let onChange = $select.data("on-change-multiselect");
    if (!cgu.isNullOuUndefined(onChange) && onChange !== "") {
        /* jshint -W061*/
        let callback = eval(onChange);
        if (typeof callback === "function") {
            return {onChange: callback}
        } else {
            console.error("Callback do autocomplete inválido!");
        }
    }

    return {};
}
