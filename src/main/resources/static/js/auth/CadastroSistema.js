/* exported CadastroSistema */

const CadastroSistema = {
    configurarValidacao: function () {
        //Validação
        $("#form").validate({
            rules: {
                nome: {required: true, maxlength: 1000}
            }
        });
    },

    configurarSubmit: function () {
        $("#btnSalvar").click(CadastroSistema.salvar);
    },

    salvar : function () {
        const $form = $("#form");
        if (!$form.valid()) {
            return;
        }

        const form = $form.serializeObject();

        $.ajax({
            type: "POST",
            url: springUrl + "api/auth/sistemas/salvar",
            data: JSON.stringify(form),
            contentType: "application/json",
            dataType: "json",
            success: function (id) {
                $("#id").val(id);
                cgu.cookies.create("showSuccessMessage", "Operação realizada com sucesso.", 1);
                window.location.href = springUrl + "auth/sistemas/" + id;
            }
        });
    },
    configurarBotoesAlternarAtivacao: function() {
        $("#btnAtivar, #btnDesativar").on("click", function() {
            let id = $(this).data("id");
            $.ajax({
                type: "POST",
                contentType: "application/json",
                dataType: "text",
                url: springUrl + "auth/sistemas/alternar-ativacao/" + id,
                success: () => {
                    cgu.cookies.create("showSuccessMessage", "Operação realizada com sucesso.", 1);
                    window.location.href = springUrl + "auth/sistemas/" + id;
                }
            });
        });
    },

    init: function () {
        this.configurarValidacao();
        this.configurarSubmit();
        this.configurarBotoesAlternarAtivacao();
        $("#nome").focus();
    }
};

$(function(){
    CadastroSistema.init();
});
