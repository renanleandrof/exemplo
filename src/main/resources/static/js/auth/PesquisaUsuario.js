/* exported PesquisaUsuario */

const PesquisaUsuario = {
    configurarFiltrador: function(data) {
        data.nome = $("#nome").val();
        data.login = $("#login").val();
        data.ativo = $("#ativo").val();
    }

    , configurarGrid: function(modoPopup) {
        const gridOptions = dataTables.defaultOptions();
        gridOptions.ajax.url = springUrl + "api/auth/usuarios";
        gridOptions.columns = [
            dataTables.colunaDeSelecao(modoPopup)
            ,{name: "nome", data: "nome", title: "Nome"}
            ,{name: "login", data: "login", title: "Login"}
            ,{name: "id", data: "id", title:"Ações", visible: !(modoPopup), sortable:false, className:"dt-center", width: "200px", render: function(data, type, row) {
                let botaoAtivarDesativar;
                if (row.ativo) {
                    botaoAtivarDesativar = cgu.format("<button id='btnAlternar__{0}' data-id='{0}' class='btn btn-danger' " +
                        "data-toggle='tooltip' title='Inativar'><i class='fa fa-power-off'></i></button>", data);
                } else {
                    botaoAtivarDesativar = cgu.format("<button id='btnAlternar__{0}' data-id='{0}' class='btn btn-success' " +
                        "data-toggle='tooltip' title='Ativar'><i class='fa fa-power-off'></i></button>", data);
                }

                return `<span class='btn-group btn-group-sm'>${botaoAtivarDesativar}
                            <a href='${springUrl}auth/usuarios/${data}' class='btn btn-warning' data-toggle='tooltip' title='Editar'>
                                <i class='far fa-edit'></i>
                            </a>
                        </span>`;
            }}
        ];

        gridOptions.order = [[0, "asc"]];
        gridOptions.initComplete = function(settings) {
            dataTables.defaultOptions().initComplete(settings);
            cgu.tooltip();
        };
        if(modoPopup) {
            gridOptions.rowId = "id";
            gridOptions.select = {style: "multi"};
        }

        gridOptions.filtrador = this.configurarFiltrador;

        $("#lista").DataTable(gridOptions);
    }

    , filtrar: function(modoPopup) {
        const $lista = $("#lista");
        $lista.DataTable().destroy();
        $lista.html("");
        $lista.DataTable(PesquisaUsuario.configurarGrid(modoPopup));
        dataTables.atualizarUrl("#lista");
        dataTables.atualizarFiltrosDaPesquisa();
    }

    , configurarBotoesAlternarAtivacao: function() {
        $("#lista").on("click", "button[id^=btnAlternar__]", function() {
            let idUsuario = $(this).data("id");
            $.ajax({
                type: "POST",
                contentType: "application/json",
                dataType: "text",
                url: springUrl + "auth/usuarios/alternar-ativacao/" + idUsuario,
                success: (data) => {
                    cgu.exibirSucesso(data);
                    $("#lista").dataTable().api().ajax.reload(null, false);
                }
            });
        });
    }

    , init: function(modoPopup) {
        this.configurarGrid(modoPopup);
        this.configurarBotoesAlternarAtivacao();

        $("#btnFiltrar").click(function () {
            PesquisaUsuario.filtrar(modoPopup);
        });
    }
};