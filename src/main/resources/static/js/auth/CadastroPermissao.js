/* exported CadastroPermissao */

const CadastroPermissao = {
    configurarValidacao: function () {
        //Validação
        $("#form").validate({
            rules: {
                nome: {required: true, maxlength: 1000},
                "sistema.id": {required: true}
            }
        });
    },

    configurarSubmit: function () {
        $("#btnSalvar").click(function () {
            const $form = $("#form");
            if (!$form.valid()) {
                return;
            }

            const form = $form.serializeObject();
            $.ajax({
                type: "POST",
                url: springUrl + "api/auth/permissoes/salvar",
                data: JSON.stringify(form),
                contentType: "application/json",
                dataType: "json",
                success: function (id) {
                    $("#id").val(id);
                    cgu.cookies.create("showSuccessMessage", "Operação realizada com sucesso.", 1);
                    window.location.href = springUrl + "auth/permissoes/novo?idSistema=" + form.idSistema;
                }
            });
        });
    },

    init: function () {
        this.configurarValidacao();
        this.configurarSubmit();
        $("#nome").focus();
    }
};

$(function(){
    CadastroPermissao.init();
});
