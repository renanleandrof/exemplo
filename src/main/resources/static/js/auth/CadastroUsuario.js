/* exported CadastroUsuario */

const CadastroUsuario = {
    configurarValidacao: function () {
        //Validação
        $("#form").validate({
            rules: {
                nome: {required: true, maxlength: 1000}
                , login: {required: true, maxlength: 500}
                , email: {required: false, maxlength: 200, email: true}
            }
        });
    },

    configurarSubmit: function () {
        $("#btnSalvar").click(function () {
            const $form = $("#form");
            if (!$form.valid()) {
                return;
            }

            const form = $form.serializeObject();

            if (!cgu.isNullOuUndefined(form.permissoes)) {
                form.permissoes = JSON.parse("[" + form.permissoes + "]");
            }

            $.ajax({
                type: "POST",
                url: springUrl + "api/auth/usuarios/salvar",
                data: JSON.stringify(form),
                contentType: "application/json",
                dataType: "json",
                success: function (id) {
                    $("#id").val(id);
                    cgu.cookies.create("showSuccessMessage", "Operação realizada com sucesso.", 1);
                    window.location.href = springUrl + "auth/usuarios/" + id;
                }
            });
        });
    },

    configurarBotoesAlternarAtivacao: function() {
        $("#btnAtivar, #btnDesativar").on("click", function() {
            let idUsuario = $(this).data("id");
            $.ajax({
                type: "POST",
                contentType: "application/json",
                dataType: "text",
                url: springUrl + "auth/usuarios/alternar-ativacao/" + idUsuario,
                success: () => {
                    cgu.cookies.create("showSuccessMessage", "Operação realizada com sucesso.", 1);
                    window.location.href = springUrl + "auth/usuarios/" + idUsuario;
                }
            });
        });
    },

    configurarBotaoResetarSenha: function () {
        $("#btnSenha").on("click", function() {
            let idUsuario = $(this).data("id");
            $.ajax({
                type: "POST",
                url: springUrl + "api/auth/usuarios/" + idUsuario + "/resetar-senha",
                success: (data) => {
                    cgu.exibirSucesso("Foi gerada uma nova senha: <b>" + data + "</b>");
                }
            });
        });
    }

    ,init: function () {
        this.configurarValidacao();
        this.configurarSubmit();
        this.configurarBotoesAlternarAtivacao();
        this.configurarBotaoResetarSenha();
        $("#nome").focus();
    }
};

$(function(){
    CadastroUsuario.init();
});
