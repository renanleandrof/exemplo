/* exported PesquisaSistema */

const PesquisaSistema = {
    configurarFiltrador: function(data) {
        data.nome = $("#nome").val();
        data.ativo = $("#ativo").val();
    }

    , configurarGrid: function(modoPopup) {
        const gridOptions = dataTables.defaultOptions();
        gridOptions.ajax.url = springUrl + "api/auth/sistemas";
        gridOptions.columns = [
            {name: "id", data: "id", title: "Id"}
            ,{name: "nome", data: "nome", title: "Nome"}
            ,{name: "acoes", data: "id", title:"Ações", sortable:false, className:"dt-center", width: "200px", render: function(data, type, row) {
                let botaoAtivarDesativar;
                if (row.ativo) {
                    botaoAtivarDesativar = cgu.format("<button id='btnAlternar__{0}' data-id='{0}' class='btn btn-danger' " +
                        "data-toggle='tooltip' title='Inativar'><i class='fa fa-power-off'></i></button>", data);
                } else {
                    botaoAtivarDesativar = cgu.format("<button id='btnAlternar__{0}' data-id='{0}' class='btn btn-success' " +
                        "data-toggle='tooltip' title='Ativar'><i class='fa fa-power-off'></i></button>", data);
                }

                return `<span class='btn-group btn-group-sm'>${botaoAtivarDesativar}
                            <a href='${springUrl}auth/sistemas/${data}' class='btn btn-warning' data-toggle='tooltip' title='Editar'>
                                <i class='far fa-edit'></i>
                            </a>
                        </span>`;
            }}
        ];

        gridOptions.order = [[0, "asc"]];
        gridOptions.initComplete = function(settings) {
            dataTables.defaultOptions().initComplete(settings);
            cgu.tooltip();
        };
        gridOptions.filtrador = this.configurarFiltrador;

        $("#lista").DataTable(gridOptions);
    }

    , filtrar: function() {
        const $lista = $("#lista");
        $lista.DataTable().destroy();
        $lista.html("");
        $lista.DataTable(PesquisaSistema.configurarGrid());
        dataTables.atualizarUrl("#lista");
        dataTables.atualizarFiltrosDaPesquisa();
    }

    , configurarBotoesAlternarAtivacao: function() {
        $("#lista").on("click", "button[id^=btnAlternar__]", function() {
            let id = $(this).data("id");
            $.ajax({
                type: "POST",
                contentType: "application/json",
                dataType: "text",
                url: springUrl + "auth/sistemas/alternar-ativacao/" + id,
                success: (data) => {
                    cgu.exibirSucesso(data);
                    $("#lista").dataTable().api().ajax.reload(null, false);
                }
            });
        });
    }

    , init: function() {
        this.configurarGrid();
        this.configurarBotoesAlternarAtivacao();

        $("#btnFiltrar").click(function () {
            PesquisaSistema.filtrar();
        });
    }
};