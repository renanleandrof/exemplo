const Autocomplete = {};

Autocomplete.optionsPadrao = {
      searchDelay: 1000
    , minChars: 3
    , hintText: "Comece a digitar para pesquisar"
    , noResultsText: "Nenhum resultado encontrado"
    , searchingText: "Carregando..."
    , preventDuplicates: true
    , buscaAvancada: false
};

Autocomplete.initAutocomplete = function (input, options) {
    Autocomplete.prepararBuscaAvancada(input, options);
    Autocomplete.prepararCriacaoEntidade(input, options);

    if (!cgu.isStringVaziaNullOuUndefined(options.criacaoEntidade)) {
        options.tokenFormatter = function(item){
            return "<li><p>" + item.name + "</p><span class='edicao-entidade' title='Editar' " +
                "onclick='Autocomplete.editarEntidade(" + item.id + ",\"" + options.tipo + "\")'>" + "<i class='fa fa-edit'></i></span></li>";
        };
    }

    let url = input.data("autocomplete-url");
    if (cgu.isStringVaziaNullOuUndefined(url)) {
        url = springUrl + input.data("autocomplete-tipo") + "/autocomplete";
    } else {
        url = springUrl + url;
    }

    let urlPorIds = input.data("autocomplete-url-por-ids");
    if (cgu.isStringVaziaNullOuUndefined(urlPorIds)) {
        urlPorIds = url + "/ids"
    } else {
        urlPorIds = springUrl + urlPorIds;
    }

    const idsSelecionados = input.val();
    if (idsSelecionados === "") {
        input.tokenInput(url, options);
        return;
    } else {
        if (idsSelecionados.split(",").length > 10) {
            cgu.exibirAlerta("Máximo 10 parâmetros do tipo " + input.data("autocomplete-tipo"));
            input.val("");
            return;
        }
    }

    $.get(urlPorIds, {ids: idsSelecionados}, function (data) {
        options.prePopulate = data;
        input.tokenInput(url, options);
    });
};

Autocomplete.editarEntidade = function (id, tipo) {
    const windowName = "popUpEdicaoEntidade" + Math.random().toString(36).substring(5);
    window.open(springUrl + "popup/" + tipo + "/" + id, windowName, "height=700,width=1024,location=no,menubar=no,resizable=yes,status=no,toolbar=no");
};

Autocomplete.criarAutocomplete = function(input) {
    let options = {};
    $.extend(options, Autocomplete.optionsPadrao);

    let onSelect = input.data("autocomplete-on-select");
    if (!cgu.isNullOuUndefined(onSelect) && onSelect !== "") {
        /* jshint -W061*/
        let callback = eval(onSelect);
        if (typeof callback === "function") {
            options.onAdd = callback;
        } else {
            console.error("Callback do autocomplete inválido!");
        }
    }

    let onDelete = input.data("autocomplete-on-delete");
    if (!cgu.isNullOuUndefined(onDelete) && onDelete !== "") {
        /* jshint -W061*/
        let callback = eval(onDelete);
        if (typeof callback === "function") {
            options.onDelete = callback;
        } else {
            console.error("Callback do autocomplete inválido!");
        }
    }

    options.tokenLimit = input.data("autocomplete-maximo-itens");
    options.placeholder = input.attr("placeholder");
    options.buscaAvancada = input.data("busca-avancada");
    options.criacaoEntidade = input.data("criacao-entidade");
    options.tipo = input.data("autocomplete-tipo");
    options.minChars = cgu.isNullOuUndefined(input.data("autocomplete-qtd-caracteres-pesquisa")) ? options.minChars : input.data("autocomplete-qtd-caracteres-pesquisa");
    options.allowFreeTagging = input.data("autocomplete-permite-criar-valores");

    Autocomplete.initAutocomplete(input, options);
};

Autocomplete.criarAutocompletes = function () {
    $("input[data-autocomplete-tipo]").each(function () {
        Autocomplete.criarAutocomplete($(this));
    });
};

Autocomplete.prepararBuscaAvancada = function(input, options) {
    if (options.buscaAvancada === "") {
        return;
    }

    const singleSelection = options.tokenLimit <= 1;

    input.parent().find(".input-group-btn").off("click").on("click", function() {
        //verifica se o campo está ativo para abrir a janela de busca avançada
        if (!$(`#${input.prop("id")}`).prop("disabled")) {
            const windowName = "popUpBuscaAvancada" + Math.random().toString(36).substring(5);
            window.open(springUrl + "popup/" + options.buscaAvancada + `?singleSelection=${singleSelection}` + (cgu.isNullOuUndefined(input.data("outros-parametros")) ? "" : input.data("outros-parametros")), windowName, "height=700,width=1024,location=no,menubar=no,resizable=yes,status=no,toolbar=no");
            window["callback" + windowName] = Autocomplete.callbacks(input);
        }
    });
};

Autocomplete.prepararCriacaoEntidade = function(input, options) {
    if (options.criacaoEntidade === "") {
        return;
    }

    input.parent().parent().find(".btn-autocomplete-criar").click(function() {
        const windowName = "popUpCriacaoEntidade" + Math.random().toString(36).substring(5);
        window.open(springUrl + "popup/" + options.tipo + "/novo", windowName, "height=700,width=1024,location=no,menubar=no,resizable=yes,status=no,toolbar=no");
        window["callback" + windowName] = Autocomplete.callbacks(input);
    });
};

Autocomplete.callbacks = function(input) {
    return function (idsSelecionados) {
        const url = springUrl + input.data("autocomplete-tipo") + "/autocomplete";
        $.get(url + "/ids", {ids: idsSelecionados.join(",")}, function (data) {
            $.each(data, function (i, v) {
                if (input.data("autocomplete-maximo-itens") === 1) {
                    input.tokenInput("clear");
                }
                input.tokenInput("add", v);
                input.change();
            });
        });
    }
};

window.addEventListener("message", function(event){
    if (event.data.idsSelecionados !== undefined) {
        window["callback" + event.data.windowName](event.data.idsSelecionados);
    }
    if (event.data.id !== undefined) {
        window["callback" + event.data.windowName]([event.data.id]);
    }
}, false);