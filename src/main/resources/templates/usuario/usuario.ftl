<#ftl output_format="HTML">
<#include "../_formulario.ftl"/>
<#-- @ftlvariable name="usuario" type="br.com.ecge.autorizador.negocio.Usuario" -->
<#-- @ftlvariable name="permissoesPorSistema" type="java.util.Map<br.com.ecge.autorizador.negocio.Sistema, java.util.List<br.com.ecge.autorizador.negocio.Permissao>>" -->
<#assign scriptContent>
<@autocompleteScripts/>
<script src="<@spring.url "/static/js/auth/CadastroUsuario.js"/>"></script>
</#assign>

<#assign breadcrumb>
<i class="fa fa-user"></i> Cadastro de Usuário
</#assign>
<@layout.portal script=scriptContent titulo="Cadastro de Usuário" breadcrumb=breadcrumb>

<#assign disabled=(usuario.ativo?? && !usuario.ativo)/>

    <form id="form" method="post">
        <input type="hidden" id="id" name="id" value="${usuario.id!}" class="form-control"/>

        <@bs.panel id="panelDadosUsuario" titulo="Dados do Usuário">
            <fieldset>
                <legend class="hidden">Dados do Usuário</legend>
                <@campoTextoFormulario label="Nome" id="nome" obrigatorio=true value=usuario.nome! disabled=disabled />

                <@campoTextoFormulario label="Login" id="login" obrigatorio=true value=usuario.login! disabled=disabled />

                <@campoTextoFormulario label="E-mail" id="email" obrigatorio=false value=usuario.email! disabled=disabled />
            </fieldset>
            <fieldset>
                <legend>Permissões</legend>
                    <#list permissoesPorSistema as sistema, permissoes>
                        <div class="form-group">
                            <div class="col-md-12 ">
                                <h3>${sistema}</h3>

                                <#list permissoes as permissao>
                                    <@campoCheckbox label="${permissao.nome}" id="permissao_${permissao.id}"
                                    isChecked=usuario.hasPermissao(permissao)
                                    dataName="permissoes" dataValue="${permissao.id}" />
                                </#list>
                            </div>
                        </div>
                    </#list>


            </fieldset>

            <a class="btn btn-default btn-labeled" href='<@spring.url "/auth/usuarios"/>'><span class="btn-label"><i class="fa fa-arrow-left"></i></span> Voltar</a>

            <span class="pull-right">
                <#if !disabled>
                    <#if usuario.id??>
                        <button type="button" class="btn btn-danger btn-labeled" id="btnDesativar" data-id="${usuario.id}"><span class="btn-label"><i class="fa fa-power-off"></i></span>Inativar</button>

                        <button type="button" class="btn btn-purple btn-labeled" id="btnSenha" data-id="${usuario.id}"><span class="btn-label"><i class="fa fa-lock"></i></span>Gerar Nova Senha</button>

                    </#if>
                    <button type="button" id="btnSalvar" class="btn btn-primary btn-labeled"><span class="btn-label"><i class="fa fa-save"></i></span>Salvar</button>
                <#else>
                    <button type="button" class="btn btn-success btn-labeled" id="btnAtivar" data-id="${usuario.id}"><span class="btn-label"><i class="fa fa-power-off"></i></span>Ativar</button>
                </#if>
            </span>
        </@bs.panel>
    </form>

</@layout.portal>