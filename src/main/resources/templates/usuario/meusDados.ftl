<#ftl output_format="HTML">
<#include "../_formulario.ftl"/>
<#-- @ftlvariable name="usuarioLogado" type="br.com.ecge.autorizador.negocio.Usuario" -->
<#assign matrizFrequenciaRecebimentoEmail = [   [0, " Não receber e-mail"],
                                                [1, " Receber um e-mail para cada notificação/pendência"],
                                                [2, " Receber lista do dia"],
                                                [3, " Receber lista da semana"]] />

<#assign scriptContent>
<script>
    $(function () {
        // Validação do formulário de dados do usuário logado
        $("#formUsuarioLogado").validate({
            rules: {
                emailUsuarioLogado: {required: false, maxlength: 200, email: true},
                novaSenha : {minlength:8},
                novaSenha2 : {equalTo:"#novaSenha"}
            }
        });

        $("#btnSalvarUsuarioLogado").click(function() {
            const form = $('#formUsuarioLogado');

            if(!form.valid()) {
                return;
            }

            const data = form.serializeArray();

            $.post("<@spring.url '/auth/usuarioLogado/salvar' />", data, function(){
                cgu.cookies.create("showSuccessMessage", "Operação realizada com sucesso.", 1);
                window.location.reload()
            });
        });

        // Botao ajax para gerar nova chave
        $("#btnChaveApiUsuarioLogado").click(function() {
            $.get("<@spring.url '/auth/usuarioLogado/gerarChaveApi' />", function(data){
                $("#chaveApiUsuarioLogado").val(data);
                $("#chaveApiUsuarioLogadoAjuda").show();
            });
        });

        $(".tooltip-large").tooltip({
            template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner large"></div></div>'
        });

        cgu.prepararUploadAjax();

    });
</script>
</#assign>

<#assign breadcrumb>
<i class="fa fa-user"></i> Olá ${usuarioLogado.nome}
</#assign>

<#assign botoesFlutuantes>
    <a id="btnSalvarUsuarioLogado" type="button" class="btn btn-labeled btn-primary">
        <span class="btn-label"><i class="fa fa-check"></i></span>Salvar
    </a>
</#assign>


<@layout.portal script=scriptContent titulo="Meus Dados" breadcrumb=breadcrumb botoesFlutuantes=botoesFlutuantes>

    <form id="formUsuarioLogado" enctype="multipart/form-data" method="POST">

    <@bs.panel id="dados" titulo="Meus Dados">
        <div class="row">

            <div class="col-md-6">
                <div class="form-group">
                    <label for="emailUsuarioLogado">E-mail</label>
                    <input type="text" id="emailUsuarioLogado" name="emailUsuarioLogado" value="${usuarioLogado.email!}" class="form-control" />
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group" data-intro="Chave de API é a sua senha de autenticação para utilizar os webservices do e-AUD.">
                    <label for="chaveApiUsuarioLogado">Chave para uso da API</label>
                    <span class="input-group">
                        <input id="chaveApiUsuarioLogado" name="chaveApiUsuarioLogado" type="text" class="form-control" readonly="readonly"/>
                        <span class="input-group-addon" data-toggle="tooltip" title="Gerar nova chave" id="btnChaveApiUsuarioLogado">
                            <i class="fas fa-sync-alt"></i>
                        </span>
                    </span>
                    <small id="chaveApiUsuarioLogadoAjuda" style="display: none;">Anote essa chave! Não é possível consultá-la depois. Se perder, será necessário gerar uma nova chave. Para confirmar a alteração, clique em salvar no fim da página.</small>
                </div>
            </div>
        </div>
    </@bs.panel>

    <@bs.panel id="minhasPermissoes" titulo="Minhas Permissões">
          <#list usuarioLogado.permissoesPorSistema as sistema, permissoes>
                <div class="row">
                    <div class="col-md-12">
                        <h4>${sistema}</h4>
                    </div>

                    <#list permissoes as permissao>
                        <div class="col-sm-4">
                            <div class="radio c-radio c-radio-nofont">
                                <label for="perfil_${permissao.id}">
                                    <input type="radio" id="perfil_${permissao.id}" name="perfil_${permissao.id}" value="-1" checked="checked">
                                    <span></span>${permissao.nome}
                                </label>
                            </div>
                        </div>
                    </#list>
                </div>
          </#list>
    </@bs.panel>

    <@bs.panel titulo="Alteração de Senha">
        <div class="row">

            <div class="col-md-4">
                <div class="form-group">
                    <label for="senhaAtual">Senha Atual</label>
                    <input type="password" id="senhaAtual" name="senhaAtual" value="" class="form-control" />
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="novaSenha">Nova Senha</label>
                    <input type="password" id="novaSenha" name="novaSenha" value="" class="form-control" />
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="novaSenha2">Repita a nova Senha</label>
                    <input type="password" id="novaSenha2" name="novaSenha2" value="" class="form-control" />
                </div>
            </div>

        </div>
    </@bs.panel>

    </form>

</@layout.portal>