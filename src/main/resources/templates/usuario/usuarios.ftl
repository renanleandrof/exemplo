<#-- @ftlvariable name="filtro" type="br.com.ecge.autorizador.aplicacao.UsuarioFiltro" -->
<#include "../_formulario.ftl"/>
<#include "../_datatables.ftl"/>
<#assign scriptContent>
<@autocompleteScripts/>
<@datatableScripts/>
<@multiSelectScripts />

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="<@spring.url "/static/js/auth/PesquisaUsuario.js"/>"></script>
<script>
    $(function() {
        PesquisaUsuario.init(${(modoPopup??)?c});
    });
</script>
</#assign>

<#assign breadcrumb>
<i class="fa fa-users"></i> Pesquisa de Usuários
</#assign>

<#assign botoesFlutuantes>
    <@botaoUsarSelecionados idTabela="lista"/>
    <@botaoFiltros/>
    <#if !modoPopup??>
    <a class="btn btn-info btn-floating-action botao-flutuante z-depth-3" href="<@spring.url '/auth/usuarios/novo'/>"
       data-toggle="tooltip" title="Novo Usuário"><i class="fa fa-plus"></i></a>
    </#if>
</#assign>

<#assign offside>
    <@boxFiltros>
        <@campoSelectFormulario label="Colunas para exibir" id="colunasSelecionadas" opcoes=colunasDisponiveis tipoOpcao="MultiselectOption" valorSelecionado=filtro.colunasSelecionadas multiplo=true ignorarFiltroUtilizado=true />

        <@campoTextoFormulario label="Nome" id="nome" value=filtro.nome! />
        <@campoTextoFormulario label="Login" id="login" value=filtro.login! />
        <div class="form-group">
            <label for="ativo">Situação </label>
            <select class="form-control" id="ativo" name="ativo">
                <option value="" <#if !(filtro.ativo??)>selected="selected"</#if>>Todos</option>
                <option value="true" <#if filtro.ativo?? && filtro.ativo>selected="selected"</#if>>Ativos</option>
                <option value="false" <#if filtro.ativo?? && !filtro.ativo>selected="selected"</#if>>Inativos</option>
            </select>
        </div>
    </@boxFiltros>
</#assign>

<@layout.portal script=scriptContent titulo="Usuários" breadcrumb=breadcrumb botoesFlutuantes=botoesFlutuantes offside=offside>
    <@filtrosUtilizadosNaPesquisa />

    <@bs.panelTable id="panelTableUsuarios">
        <@datatableHtml id="lista"/>
    </@bs.panelTable>
</@layout.portal>