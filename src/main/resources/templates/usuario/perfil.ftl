<#ftl output_format="HTML">
<#include "../_formulario.ftl"/>
<#-- @ftlvariable name="permissao" type="br.com.ecge.autorizador.negocio.Permissao" -->
<#-- @ftlvariable name="permissoes" type="br.com.ecge.autorizador.negocio.Permissao[]" -->
<#assign scriptContent>
<@autocompleteScripts/>
<script src="<@spring.url "/static/js/auth/CadastroPerfil.js"/>"></script>
</#assign>

<#assign breadcrumb>
<i class="far fa-id-card"></i> Cadastro de Perfil
</#assign>
<@layout.portal script=scriptContent titulo="Cadastro de Perfil" breadcrumb=breadcrumb>

    <form id="form" method="post">
        <input type="hidden" id="id" name="id" value="${permissao.id!}" class="form-control"/>

        <@bs.panel id="panelDadosPerfil" titulo="Dados do Perfil">
            <fieldset>
                <legend class="hidden">Dados do Perfil</legend>
                <@campoTextoFormulario label="Nome" id="nome" obrigatorio=true value=permissao.nome!/>

                <div class="form-group">
                    <label class="control-label" for="tiposPermissoes">Permissões </label>
                    <div id="tiposPermissoes" class="row equal-height">
                        <#list permissoes as p>
                        <div class="col-lg-3 col-md-4 col-sm-6 ">
                            <@campoCheckbox label="${p.nome}" id="permissao_${p.name()}"
                            isChecked=permissao.permissoes?seq_contains(p)
                            dataName="permissoes" dataValue="${p.name()}" />
                        </div>
                        </#list>
                    </div>
                </div>
            </fieldset>

            <a class="btn btn-default btn-labeled" href='<@spring.url "/auth/permissoes"/>'><span class="btn-label"><i class="fa fa-arrow-left"></i></span> Voltar</a>
            <span class="pull-right">
                <button id="btnSalvar" type="button" class="btn btn-primary btn-labeled"><span class="btn-label"><i class="fa fa-save"></i></span>Salvar</button>
            </span>
        </@bs.panel>
    </form>

</@layout.portal>