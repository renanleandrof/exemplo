<#ftl output_format="HTML" auto_esc=true>
<#macro datatableHtml id="lista" titulos=[] tableClass="" colunaDeSelecao=false caption="Dados da tabela" footer="">
    <table id="${id}" class="table table-hover table-striped table-bordered ${tableClass}" width="100%">
        <caption class="hidden">${caption}</caption>
        <#if (titulos?size >0)>
        <thead>
        <tr>
            <#if colunaDeSelecao>
                <th id="colSelecionar" scope="col">
                    <span id="btnSelectAll" onclick="dataTables.eventoSelectAll('${id}', this);" >
                        <i class="far fa-square"></i>
                    </span>
                </th>
            </#if>
            <#list titulos as t>
                <th>${t}</th>
            </#list>
        </tr>
        </thead>
        </#if>
        <tbody>
        </tbody>
        <#if (footer?is_string && footer != "") || !footer?is_string >
            <tfoot>${footer?no_esc}</tfoot>
        </#if>
    </table>
</#macro>

<#macro datatableScripts>
    <script type="text/javascript" src="<@spring.url '/static/libs/dataTables/jquery.dataTables.min.js'/>"></script>
    <script type="text/javascript" src="<@spring.url '/static/libs/dataTables/dataTables.bootstrap.min.js'/>"></script>
    <script type="text/javascript" src="<@spring.url '/static/libs/dataTables/dataTables.select.min.js'/>"></script>
    <script type="text/javascript" src="<@spring.url '/static/js/datatables.js?v=#VERSAO-SISTEMA#'/>"></script>
    <link rel="stylesheet" href="<@spring.url '/static/libs/dataTables/dataTables.bootstrap.min.css'/>" type="text/css"/>
    <link rel="stylesheet" href="<@spring.url '/static/libs/dataTables/select.bootstrap.min.css'/>" type="text/css"/>
</#macro>

<#macro boxFiltros titulo="Filtros" idTabelaAlvo="lista" botaoFiltrarExecutaRefresh=true mostrarBtnSalvarFiltro=false mostrarBtnLimparFiltros=true target="formFiltros">

    <form id="formFiltros">
        <a class="pull-right text-muted text-thin" data-toggle-state="offsidebar-open" data-no-persist="true"><i class="fas fa-times fa-2x"></i></a>
        <h3 class="text-center text-thin">${titulo}</h3>
        <#nested />
    </form>

    <div class="buttons-box-filtros">
        <button id="btnFiltrar" type="button" class="btn btn-primary btn-labeled"
            <#if botaoFiltrarExecutaRefresh >
                data-datatables-refresh="#${idTabelaAlvo}"
            </#if>
                data-toggle-state="offsidebar-open" data-no-persist="true"
        ><span class="btn-label"><i class="fa fa-search"></i></span>Filtrar</button>

        <#if mostrarBtnSalvarFiltro>
            <button type="button" id="btnSalvarFiltro" class="btn btn-warning btn-labeled"><span class="btn-label"><i class="fa fa-star"></i></span>Salvar</button>
        </#if>

        <#if mostrarBtnLimparFiltros>
            <button id="btnLimparFiltros" type="reset" class="btn btn-default btn-labeled" data-target="${target}"><span class="btn-label"><i class="fa fa-eraser"></i></span>Limpar</button>
        </#if>
    </div>
</#macro>

<#macro botaoFiltros target="filtros">
    <span data-toggle="collapse" href="#${target}" aria-expanded="false" data-intro="Clique para abrir/fechar a caixa de filtros">
        <button id="exibeOcultaFiltros" class="btn btn-warning btn-floating-action botao-filtros botao-flutuante z-depth-3 collapsed"
                data-toggle="tooltip" title="Exibir/Esconder Filtros" data-toggle-state="offsidebar-open" data-no-persist="true">
            <i class="fa fa-filter"></i></button>
    </span>
</#macro>

<#macro botaoUsarSelecionados idTabela>
    <#if modoPopup??>
        <a class="btn btn-primary" href="#" onclick="dataTables.usarSelecionados('${idTabela}')"><i class="fa fa-check"></i> Usar Selecionado(s)</a>
    </#if>
</#macro>

<#macro botoesAcoesEmLote modoPopup=false>
    <#if !modoPopup>
        <div class="dropup inline" id="menuAcoesEmLote">
            <span class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <button type="button" class="btn btn-success btn-floating-action botao-flutuante z-depth-3" data-toggle="tooltip" title="Ações em Lote" data-placement="left">
                    <i class="fa fa-tasks"></i>
                </button>
            </span>
            <ul class="dropdown-menu dropup pull-right">
                <li><a href="#" data-tipo="registro-de-interacao" data-target-js="RegistroInteracao" data-target="#modalRegistrarInteracao"><i class="fa fa-plus"></i> Registrar Interação</a></li>
                <li><a href="#" data-tipo="adicao-de-compartilhamento" data-target-js="Compartilhamento" data-target="#modalCompartilhar"><i class="fa fa-share-alt"></i> Adicionar Compartilhamento</a></li>
                <li>
                    <@dialogConfirmacao mensagem="Todos os compartilhamentos das ocorrências marcadas serão redefinidos. Deseja continuar?" disposicao="top"
                        okLabel="Continuar" dadosAdicionais="data-tipo=redefinicao-de-compartilhamento data-target-js=Compartilhamento data-target=#modalCompartilhar">
                        <i class="fas fa-share"></i> Redefinir Compartilhamento
                    </@dialogConfirmacao>
                </li>
            </ul>
        </div>
    </#if>
</#macro>

<#macro botaoNovo id="btnNovoElemento" label="" url="" modoPopup=false>
    <#if !modoPopup>
        <a id="${id}" class="btn btn-info btn-floating-action botao-flutuante z-depth-3" data-toggle="tooltip" title="${label}" href="<@spring.url url/>"><i class="fa fa-plus"></i></a>
    </#if>
</#macro>

<#macro botaoExportar label="" url="" modoPopup=false>
    <#if !modoPopup>
        <a id="btnExportar" class="btn btn-exportar btn-floating-action botao-flutuante z-depth-3" data-toggle="tooltip" title="${label}" href="<@spring.url url/>"><i class="fa fa-download"></i></a>
    </#if>
</#macro>

<#macro filtrosUtilizadosNaPesquisa>
    <div id="painel-filtros-da-pesquisa" class="painel-filtros">

    </div>
</#macro>

<#macro abaGraficos>
    <div role="tabpanel" class="tab-pane" id="graficos">
        <@bs.panel id="panelGraficos">
            <form id="formGraficos">
                <label>Configurações do Gráfico</label>
                <div id="configuracaoGrafico">
                </div>
            </form>

            <div id="chart_div" style="min-height: 500px;width: 100%"></div>

            <div id="divBotaoAdicionarAoPainel" style="display: none">
                <button id="btnAdicionarAoPainel" type="button" class="btn btn-primary btn-labeled" data-options-grafico="">
                    <span class="btn-label"><i class="fa fa-plus"></i></span>Adicionar à um painel
                </button>
            </div>
        </@bs.panel>
    </div>
</#macro>