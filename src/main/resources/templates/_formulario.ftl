<#ftl output_format="HTML" auto_esc=true>
<#macro campoTextoFormulario label id autofocus=false obrigatorio=false value="" disabled=false readonly=false class="" placeholder="" type="text" ignorarFiltroUtilizado=false tooltipAjuda="" linkAjuda="">
    <div class="form-group">
        <label for="${id}">${label}</label>
        <#if obrigatorio><div class="input-group"></#if>
        <#if tooltipAjuda?has_content || linkAjuda?has_content>
            <a style="text-decoration: none;" data-toggle="tooltip" title="${tooltipAjuda}" href="${linkAjuda}" target="_blank">
                <i class="icon-question"></i>
            </a>
        </#if>
        <input type="${type}" id="${id}" name="${id}" value="${value}" class="form-control ${class}" placeholder="${placeholder}"
               <#if autofocus>autofocus</#if> <#if disabled>disabled="disabled"</#if> <#if readonly>readonly="readonly"</#if>
               <#if ignorarFiltroUtilizado>data-ignorar-filtro-utilizado="true"</#if>/>
        <#if obrigatorio>
            <span class="input-group-addon" data-toggle="tooltip" title="Obrigatório"><i class="fa fa-asterisk"></i></span>
        </div>
        </#if>
    </div>
</#macro>

<#macro campoTextAreaFormulario label id autofocus=false obrigatorio=false value="" disabled=false readonly=false class="" placeholder="" linhas=4 ignorarFiltroUtilizado=false>
    <div class="form-group">
        <label for="${id}">${label}</label>
        <#if obrigatorio>
        <div class="input-group">
        </#if>
        <textarea id="${id}" name="${id}" rows="${linhas}" class="form-control ${class}" placeholder="${placeholder}"
               <#if autofocus>autofocus</#if> <#if disabled>disabled="disabled"</#if> <#if readonly>readonly="readonly"</#if>
               <#if ignorarFiltroUtilizado>data-ignorar-filtro-utilizado="true"</#if>>${value}</textarea>
        <#if obrigatorio>
            <span class="input-group-addon" data-toggle="tooltip" title="Obrigatório"><i class="fa fa-asterisk"></i></span>
        </div>
        </#if>
    </div>
</#macro>

<#--
    Lista de opções pode ser um array ou lista de ENUM.
    Se for outro tipo, tem que ser informado via tipoOpcao="X"
     Tipos possiveis:
     - lista de infraestrutura.pojo.MultiselectOption
 -->
<#macro campoSelectFormulario label id opcaoPadrao="" opcoes=[] tipoOpcao="ENUM" valorSelecionado=[] obrigatorio=false disabled=false readonly=false multiplo=false propriedades="" ignorarFiltroUtilizado=false onChangeMultiselect="" hint="">
    <div class="form-group">
        <label for="${id}">${label}
            <#if hint != ""><i class="fas fa-question-circle" data-toggle="tooltip" title="${hint}"></i></#if>
        </label>
        <#if obrigatorio>
        <div class="input-group">
        </#if>
        <select class="form-control" id="${id}" name="${id}"
                <#if ignorarFiltroUtilizado>data-ignorar-filtro-utilizado="true"</#if>
                <#if disabled>disabled="disabled"</#if>
                <#if readonly>readonly="true"</#if>
                <#if multiplo>multiple data-on-change-multiselect="${onChangeMultiselect}"</#if> ${propriedades}>
            <#if opcaoPadrao != "">
            <option value="">${opcaoPadrao}</option>
            </#if>

            <#assign valoresSelecionadosString = []/>

            <#if multiplo>
                <#list valorSelecionado as v >
                    <#if v?is_number>
                        <#assign valoresSelecionadosString = valoresSelecionadosString + [v?c] />
                    <#else>
                        <#assign valoresSelecionadosString = valoresSelecionadosString + [v] />
                    </#if>
                </#list>
            <#else>
                <#assign valoresSelecionadosString = [valorSelecionado] />
            </#if>


            <#list opcoes as o>
                <#if tipoOpcao == "ENUM">
                    <option value="${o.name()}" <#if valoresSelecionadosString?seq_contains(o)>selected="selected"</#if>>${o.toString()}</option>
                <#else>
                    <option value="${o.id}" <#if valoresSelecionadosString?seq_contains(o.id) >selected="selected"</#if>>${o.value}</option>
                </#if>
            </#list>
        </select>
        <#if obrigatorio>
        <span class="input-group-addon" data-toggle="tooltip" title="Obrigatório"><i class="fa fa-asterisk"></i></span>
        </div>
        </#if>
    </div>
</#macro>

<#macro multiSelectScripts>
<script src="<@spring.url "/static/js/multiselect.js"/>"></script>
<script type="text/javascript" src="<@spring.url '/static/libs/bootstrap-multiselect/js/bootstrap-multiselect.js'/>"></script>
<link rel="stylesheet" href="<@spring.url '/static/libs/bootstrap-multiselect/css/bootstrap-multiselect.css'/>" type="text/css"/>
<script>
    $(function(){
        Multiselect.criarMultiselects();
    });
</script>
</#macro>


<#--
tipoValores pode ser IDS ou ENTIDADE. Se for entidade, vai tentar pegar o .id de cada objeto da lista.
-->
<#macro autocomplete id tipo label valores=[] tipoValores="IDS" maximoItens=10 qtdCaracteresPesquisa=3 placeholder="Comece a digitar para ver sugestões" onSelect="" onDelete="" buscaAvancada="" criacaoEntidade="" obrigatorio=false url="" urlPorIds="" permiteCriarValores=false ignorarFiltroUtilizado=false wrapperClass=""
        outrosParametros="" >
    <div class="form-group ${wrapperClass}">
        <#if label != "">
            <label for="${id}">${label}</label>
        </#if>

        <#if criacaoEntidade != "">
            <span class="btn-autocomplete-criar pull-right">
                <a href="#" class="btn btn-primary"><i class="fa fa-plus"></i> Criar</a>
            </span>
        </#if>

        <#assign idValores = [] />

        <#if tipoValores == "ENTIDADE">
            <#list valores as v>
                <#if v != "">
                    <#assign idValores = idValores + [v.id] />
                </#if>
            </#list>
        <#else>
            <#list valores as v>
                <#assign idValores = idValores + [v] />
            </#list>
        </#if>

        <#if buscaAvancada != "" || obrigatorio>
            <div class="input-group">
        <#else>
            <#if criacaoEntidade != "">
            <div class="autocomplete-criar">
            </#if>
        </#if>

        <input class="form-control" id="${id}" name="${id}" value="${idValores?join(",")}"
               data-autocomplete-tipo="${tipo}"
               data-autocomplete-on-select="${onSelect}"
               data-autocomplete-on-delete="${onDelete}"
               data-autocomplete-maximo-itens="${maximoItens}"
               data-autocomplete-url="${url}"
               data-autocomplete-url-por-ids="${urlPorIds}"
               data-autocomplete-permite-criar-valores="${permiteCriarValores?c}"
               data-autocomplete-qtd-caracteres-pesquisa="${qtdCaracteresPesquisa}"
               data-busca-avancada="${buscaAvancada}"
               data-criacao-entidade="${criacaoEntidade}"
               data-outros-parametros="${outrosParametros}"
               placeholder="${placeholder}"
               <#if ignorarFiltroUtilizado>data-ignorar-filtro-utilizado="true"</#if>
        />

        <#if obrigatorio>
            <span class="input-group-addon" data-toggle="tooltip" title="Obrigatório"><i class="fa fa-asterisk"></i></span>
        </#if>
        <#if buscaAvancada != "">
            <span class="input-group-btn" data-toggle="tooltip" title="Busca Avançada" style="width: 1%;">
                <a href="#" class="btn btn-warning"><i class="fa fa-filter"></i></a>
            </span>
        </#if>
        <#if buscaAvancada != "" || criacaoEntidade != "" || obrigatorio>
            </div>
        </#if>
    </div>
</#macro>

<#macro autocompleteScripts>
    <script src="<@spring.url "/static/js/autocomplete.js"/>"></script>
    <script src="<@spring.url "/static/libs/tokeninput/jquery.tokeninput.min.js"/>"></script>
    <link href="<@spring.url '/static/libs/tokeninput/token-input.css'/>" rel="stylesheet">
    <script>
        $(function(){
            Autocomplete.criarAutocompletes();
        });
    </script>
</#macro>


<#macro campoCheckbox label id isChecked dataName="" dataValue="" onChangeFunction="" labelAntes=false>
    <div class="form-group">
        <label class="switch" id="switchLabel_${id}" for="${id}">
            ${labelAntes?then(label,"")}
            <input type="checkbox" id="${id}" name="${dataName}" data-${dataName}="${dataValue}" class="inputSwitch"
                   onchange="${onChangeFunction}" value="${dataValue}" ${isChecked?then("checked='checked'","")}>
            <span></span>
            ${labelAntes?then("",label)}
        </label>
    </div>
</#macro>

<#macro campoRadiobuttons id matriz valorAtual tamanho=12 label="">
    <#if label != "">
        <label for="${id}">${label}</label>
    </#if>
    <div id="${id}">
    <#list matriz as m>
        <#if m[2]?? >
        <span class="col-sm-${m[2]}">
        <#else>
        <span class="col-sm-${tamanho}">
        </#if>
            <div class="radio c-radio c-radio-nofont">
                <label for="${id}_${m[0]}">
                    <input type="radio" id="${id}_${m[0]}" name="${id}" value="${m[0]}" ${(m[0] == valorAtual?number)?then("checked","")}>
                    <span></span>${m[1]}
                </label>
            </div>
        </span>
    </#list>
    </div>
</#macro>

<#macro dialogConfirmacao mensagem dadosAdicionais=[] disposicao="left" titulo="Confirmação" okLabel="Ok" cancelaLabel="Cancelar">
    <a href="#" class="btnPopover" data-toggle="confirmation" data-popout="true" data-placement="${disposicao}" data-title="${titulo}"
       data-btn-ok-label="${okLabel}" data-btn-ok-icon="fas fa-share-alt" data-btn-ok-class="btn-success"
       data-btn-cancel-label="${cancelaLabel}" data-btn-cancel-icon="fas fa-times" data-btn-cancel-class="btn-danger"
       data-content="${mensagem}" ${dadosAdicionais}>
        <#nested />
    </a>
</#macro>

<#macro campoPeriodo idInicio idFim label valorInicio valorFim>
    <div class="form-group">
        <label for="${idInicio}" data-descricao-campo-for="${idInicio}, ${idInicio}">${label}</label>
        <div class="row">
            <div class="col-xs-2">
                <label for="${idInicio}">De</label>
            </div>
            <div class="col-xs-10">
                <input type="date" id="${idInicio}" name="${idInicio}" class="form-control" value="${valorInicio}" />
            </div>
            <div class="col-xs-2">
                <label for="${idFim}">Até</label>
            </div>
            <div class="col-xs-10">
                <input type="date" id="${idFim}" name="${idFim}" class="form-control" value="${valorFim}" />
            </div>
        </div>
    </div>
</#macro>

<#macro campoPeriodoValor label idInicio idFim valorInicio valorFim>
    <div class="row">
        <div class="col-xs-5">
            <@campoTextoFormulario label="${label}" id="${idInicio}" value=valorInicio />
        </div>
        <div class="col-xs-2 align-center">
            <label for="valorFim"><br /><br />até</label>
        </div>
        <div class="col-xs-5">
            <@campoTextoFormulario label="" id="${idFim}" value=valorFim />
        </div>
    </div>
</#macro>

<#macro campoIntervaloMonetario label idInicio idFim valorInicio valorFim>
    <div class="row">
        <div class="col-xs-5">
            <@campoTextoFormulario label="${label}" id="${idInicio}" class="money-mask" value=valorInicio />
        </div>
        <div class="col-xs-2 align-center">
            <label for="valorFim"><br /><br />até</label>
        </div>
        <div class="col-xs-5">
            <@campoTextoFormulario label="" id="${idFim}" class="money-mask" value=valorFim />
        </div>
    </div>
</#macro>

<#--
    Deve-se chamar a função prepararUploadAjax() de cgu.js para tratamento de upload/download de arquivo
    e criar um atributo no Form que irá receber o id do arquivo chamado 'idArquivoEntidade' (onde Entidade = id informado)
-->
<#macro campoUploadDownloadArquivo id label idArquivo codigoArquivo nomeArquivo exibirExclusao=false>
    <div class="row" style="display: inline-flex">
        <@campoDownloadArquivo id="${id}" label="${label}" codigoArquivo="${codigoArquivo}" nomeArquivo="${nomeArquivo}"
            tamanho=4 uploadIncluido=true exibirExclusao=exibirExclusao />
        <div class="col-md-8">
            <div class="form-group">
                <div class="form-group">
                    <div class="col-xs-10">
                        <input type="file" id="${id}" name="upload_${id}" class="form-control file-uploader"
                               data-target-download="download_${id}" data-url-download="<@spring.url '/'/>api/auth/arquivo/{0}/download" />
                        <input type="hidden" id="upload_${id}" name="idArquivo${id?capitalize}"/>
                    </div>
                    <div class="col-xs-2" id="progress_${id}"></div>
                </div>
            </div>
        </div>
    </div>
</#macro>

<#macro campoDownloadArquivo id label codigoArquivo nomeArquivo tamanho=12 uploadIncluido=false exibirExclusao=false>
    <#if !uploadIncluido>
    <div class="row">
    </#if>
        <div class="col-md-${tamanho}">
            <div class="form-group">
                <label for="download_${id}">${label}</label>
                <span class='form-control campo-form'>
                    <#if codigoArquivo?has_content>
                    <a href="<@spring.url '/'/>api/auth/arquivo/${codigoArquivo}/download" id="download_${id}">${nomeArquivo}</a>
                    <#else>
                    -
                    </#if>
                </span>
                <#if exibirExclusao && codigoArquivo != "">
                <span id="btnRemover-${id}" class="input-group-btn" data-codigo-arquivo="${codigoArquivo}">
                    <span class="btn btn-danger" data-toggle="tooltip" title="Remover ${label}"><i class="fas fa-trash"></i></span>
                </span>
                </#if>
            </div>
        </div>
    <#if !uploadIncluido>
    </div>
    </#if>
</#macro>

<#macro botaoRotulado id="" type="button" modelo="" class="" fa="" label="" propriedades="">
    <#if modelo?has_content>
        <#if modelo == "ativar">
            <#assign cl = "btn-success ${class}" /><#assign fig = "fa-power-off" /><#assign lb = "Ativar" />
        <#elseif modelo == "copiar">
            <#assign cl = "btn-copiar ${class}" /><#assign fig = "fa-copy" /><#assign lb = "Copiar" />
        <#elseif modelo == "fechar">
            <#assign cl = "btn-default ${class}" /><#assign fig = "fa-times" /><#assign lb = "Fechar" />
        <#elseif modelo == "inativar">
            <#assign cl = "btn-danger ${class}" /><#assign fig = "fa-power-off" /><#assign lb = "Inativar" />
        <#elseif modelo == "incluir">
            <#assign cl = "btn-info ${class}" /><#assign fig = "fa-plus" /><#assign lb = "Incluir" />
        <#elseif modelo == "salvar">
            <#assign cl = "btn-primary ${class}" /><#assign fig = "fa-save" /><#assign lb = "Salvar" />
        <#elseif modelo == "remover">
            <#assign cl = "btn-danger ${class}" /><#assign fig = "fa-trash" /><#assign lb = "Remover" />
        <#elseif modelo == "voltar">
            <#assign cl = "btn-default ${class}" /><#assign fig = "fa-arrow-left" /><#assign lb = "Voltar" />
        </#if>
    <#else>
        <#assign cl = "${class}" />
        <#assign fig = "${fa}" />
        <#assign lb = "${label}" />
    </#if>

    <button id="${id}" type="${type}" class="btn btn-labeled ${cl}"
        <#if propriedades?has_content>
            ${propriedades}
        </#if>
    >
        <span class="btn-label"><i class="fa ${fig}"></i></span>${lb}
    </button>
</#macro>


<#macro botaoAncoraRotulado id="" modelo="" class="" fa="" label="" hrefRelativa="" hrefAbsoluta="">
    <#if modelo?has_content>
        <#if modelo == "ativar">
            <#assign cl = "btn-success ${class}" /><#assign fig = "fa-power-off" /><#assign lb = "Ativar" />
        <#elseif modelo == "inativar">
            <#assign cl = "btn-danger ${class}" /><#assign fig = "fa-power-off" /><#assign lb = "Inativar" />
        <#elseif modelo == "voltar">
            <#assign cl = "btn-default ${class}" /><#assign fig = "fa-arrow-left" /><#assign lb = "Voltar" />
        </#if>
    <#else>
        <#assign cl = "${class}" />
        <#assign fig = "${fa}" />
        <#assign lb = "${label}" />
    </#if>

    <a id="${id}" class="btn btn-labeled ${cl}"
        <#if hrefRelativa?has_content>
            href = "<@spring.url ''/>${hrefRelativa}"
        <#else>
            href = "${hrefAbsoluta}"
        </#if>
    >
        <span class="btn-label"><i class="fa ${fig}"></i></span>${lb}
    </a>
</#macro>

<#macro starability class="starability-growRotate" prefix="" itemId="" stars=6 selectedStar=0 disabled=false>
    <fieldset class=${class}>
        <input type="radio"
               data-itemId="${itemId}"
               class="input-no-rate" aria-label="No rating."
               id="${prefix}_${itemId}_no-rate" name="${prefix}_${itemId}_rating" value="0" checked  ${disabled?then("disabled", "")} />

        <#list 1..stars as s>
            <input type="radio"
                   data-itemId="${itemId}"
                   id="${prefix}_${itemId}_rate${s}" name="${prefix}_${itemId}_rating" value="${s}" ${(selectedStar == s)?then("checked","")} ${disabled?then("disabled", "")} />

            <#if s == 1>
                <label for="${prefix}_${itemId}_rate${s}">1 star</label>
            <#else>
                <label for="${prefix}_${itemId}_rate${s}">${s} stars</label>
            </#if>
        </#list>
    </fieldset>
</#macro>

<#macro eaudNetworkControles itensParaOcultar=[]>
    <div class="eaudNetworkControles">
        <div>
            <label class="control-label">Ocultar: </label>

            <#list itensParaOcultar as item>
                <label class="checkbox-inline">
                    <input id="ocultar${item.value}" name="grafoItensOcultos" type="checkbox" value="${item.value}">${item.label}
                </label>
            </#list>
        </div>


        <div>
            <label for="niveis">Níveis</label>
            <input id="niveisGrafo" type="number" min="0" max="5" step="1" value="1" size="2">
            <button class="btn btn-success" id="btnAtualizarGrafo"><i class="fas fa-sync-alt" aria-hidden="true"></i></button>
        </div>
    </div>
</#macro>

<#macro filtroApenasTarefasComPendencias checked=false>
    <@campoCheckbox label="Apenas Tarefas com pendênias" id="apenasPendencias" isChecked=checked />
</#macro>