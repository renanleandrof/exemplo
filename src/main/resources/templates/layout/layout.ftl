<#setting number_format="computer">
<#assign url = springMacroRequestContext.getRequestUri() />
<#macro portal titulo="" header="" script="" scriptOnTop="" breadcrumb="" botoesFlutuantes="" offside="">
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="Autorizador ECGE">
    <title>${titulo} - Autorizador ECGE</title>
    <script> FontAwesomeConfig = { searchPseudoElements: true }; </script>

    <#--Favicons-->
    <link rel="apple-touch-icon" sizes="57x57"         href="<@spring.url '/static/'/>favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60"         href="<@spring.url '/static/'/>favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72"         href="<@spring.url '/static/'/>favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76"         href="<@spring.url '/static/'/>favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114"       href="<@spring.url '/static/'/>favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120"       href="<@spring.url '/static/'/>favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144"       href="<@spring.url '/static/'/>favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152"       href="<@spring.url '/static/'/>favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180"       href="<@spring.url '/static/'/>favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<@spring.url '/static/'/>favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32"    href="<@spring.url '/static/'/>favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96"    href="<@spring.url '/static/'/>favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16"    href="<@spring.url '/static/'/>favicon/favicon-16x16.png">
    <link rel="icon"    href="<@spring.url '/static/'/>favicon.ico">
    <link rel="manifest"                               href="<@spring.url '/static/'/>favicon/manifest.json">
    <meta name="msapplication-TileImage"            content="<@spring.url '/static/'/>favicon/ms-icon-144x144.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" href="<@spring.url '/static/libs/fontawesome/css/fontawesome-all.min.css'/>"/>
    <link rel="stylesheet" href="<@spring.url '/static/libs/simple-line-icons/css/simple-line-icons.css'/>"/>
    <link rel="stylesheet" href="<@spring.url '/static/libs/animate.css/animate.min.css'/>"/>
    <link rel="stylesheet" href="<@spring.url '/static/libs/spinkit/css/spinkit.css'/>"/>
    <link rel="stylesheet" href="<@spring.url '/static/libs/whirl/dist/whirl.css'/>"/>
    <link rel="stylesheet" href="<@spring.url '/static/libs/bootstrap/css/bootstrap.min.css'/>" media="screen" id="bscss"/>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" media="screen"/>
    <link rel="stylesheet" href="<@spring.url '/static/libs/googlefonts/SourceSansPro.css'/>"/>
    <link rel="stylesheet" href="<@spring.url '/static/css/app.css'/>" id="maincss"/>
    <link rel="stylesheet" href="<@spring.url '/static/css/azul-black.css'/>"/>
    <link rel="stylesheet" href="<@spring.url '/static/css/cgu.css?v=@project.version@'/>" media="screen"/>
    <link rel="stylesheet" href="<@spring.url '/static/css/starability-all.css' />" />

    <script type="text/javascript">
        let springUrl = "<@spring.url '/'/>".split(";")[0];
        <#if usuarioLogado??>
        let usuarioLogado = "${usuarioLogado.nome}";
        </#if>
    </script>
</head>

<body>
<script type="text/javascript" src="<@spring.url '/static/libs/jquery/jquery-1.12.0.min.js'/>"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<!-- Inclusao dos arquivos JavaScript definidos em cada view que necessitam ser carregados no inicio da pagina -->
${scriptOnTop!}

<div class="wrapper">
    <#if !modoPopup??>
    <header class="topnavbar-wrapper">
        <nav class="navbar topnavbar" role="navigation">
            <div class="hidden">
                <ul id="accessibility">
                    <li>
                        <a accesskey="1" href="#acontent" id="link-conteudo">
                            Ir para o conteúdo
                            <span>1</span>
                        </a>
                    </li>
                    <li>
                        <a accesskey="2" href="#anavigation" id="link-navegacao">
                            Ir para o menu
                            <span>2</span>
                        </a>
                    </li>
                </ul>
            </div>

            <div class="navbar-header">
                <a class="navbar-brand" href="<@spring.url '/'/>">
                    <div class="brand-logo">
                        ECGE
                        <small title="Versão: @project.version@ ---- Build: @buildTimestamp@" data-toggle="tooltip" data-placement="bottom"><i class="fas fa-code-branch"></i> @project.version@</small>
                    </div>
                    <div class="brand-logo-collapsed">ECGE</div>
                </a>
            </div>

            <div class="nav-wrapper">
                <ul class="nav navbar-nav">
                    <li>
                        <!-- Button used to collapse the left sidebar. Only visible on tablet and desktops-->
                        <a class="hidden-xs" href="#" data-trigger-resize="" data-toggle-state="aside-collapsed">
                            <em class="fas fa-bars"></em>
                        </a>
                        <!-- Button to show/hide the sidebar on mobile. Visible on mobile only.-->
                        <a class="visible-xs sidebar-toggle" href="#" data-toggle-state="aside-toggled" data-no-persist="true">
                            <em class="fas fa-bars"></em>
                        </a>
                    </li>
                </ul>

                <ul class="nav navbar-nav navbar-right">

                    <li>
                        <a id="btnAjuda" style="display: none;" data-toggle="tooltip" title="Clique aqui para entender como essa página funciona" data-intro="Em qualquer página do sistema, clique nesse ícone para obter uma ajuda rápida!">
                            <i class="icon-question"></i>
                        </a>
                    </li>

                </ul>
            </div>

            <form class="navbar-form" role="search" id="formBuscaRapida">
                <div class="form-group has-feedback">
                    <input class="form-control" type="text" title="Id de tarefa para acesso rápido" placeholder="Informe um ID de uma tarefa para acesso rápido" id="idBuscaRapida">
                    <div class="fas fa-times form-control-feedback" data-search-dismiss=""></div>
                </div>
                <button class="hidden btn btn-default" type="submit" id="btnBuscaRapida">Pesquisar</button>
            </form>

        </nav>
    </header>

    <aside class="aside">
        <div class="aside-inner">
            <nav class="sidebar" data-sidebar-anyclick-close="">
                <ul class="nav">
                    <li class="has-user-block">
                        <div id="user-block">
                            <div class="item user-block">
                                <div class="user-block-info">
                                    <#if usuarioLogado??>
                                        Olá <b>${usuarioLogado.nome}</b><br />

                                        <a title="Sair do Sistema" href="<@spring.url '/logout' />">
                                            <i class="fas fa-sign-out-alt"></i> Sair
                                        </a>
                                    <#else>
                                        <a href="<@spring.url '/auth' />"><span class="user-block-name">Login <i class="fas fa-sign-in-alt"></i></span></a>
                                    </#if>
                                </div>
                            </div>
                        </div>
                    </li>


                    <li class="nav-heading ">
                        <span id="anavigation">Menu</span>
                    </li>
                    <#if usuarioLogado??>
                        <li class="${(url?ends_with("/auth/"))?then("active","")}">
                            <a href="<@spring.url '/auth/'/>" title="Início"><em class="fas fa-home"></em> <span>Início</span></a>
                        </li>

                        <li class="${(url?ends_with("/auth/sistemas"))?then("active","")}">
                            <a href="<@spring.url '/auth/sistemas'/>" title="Sistemas"><em class="fas fa-desktop"></em> <span>Sistemas</span></a>
                        </li>

                        <li class="${(url?ends_with("/auth/permissoes"))?then("active","")}">
                            <a href="<@spring.url '/auth/permissoes'/>" title="Permissões"><em class="fas fa-lock"></em> <span>Permissões</span></a>
                        </li>

                        <li class="${(url?ends_with("/auth/usuarios"))?then("active","")}">
                            <a href="<@spring.url '/auth/usuarios'/>" title="Usuários"><em class="fas fa-users"></em> <span>Usuários</span></a>
                        </li>
                    </#if>
                    <li><a href="<@spring.url '/swagger-ui.html'/>" title="API Rest"><em class="fas fa-plug"></em> <span>API REST</span></a></li>
                </ul>
            </nav>
        </div>
    </aside>
    </#if>

    <!-- offsidebar-->
    <aside class="offsidebar hide">
        ${offside}
    </aside>

    <#if !modoPopup??>
    <section>
        <div class="content-wrapper">
            <#if (breadcrumb?is_string && breadcrumb!="") || !breadcrumb?is_string >
            <div class="content-heading">
                ${breadcrumb}
            </div>
            </#if>
    </#if>
            <!--Aqui vai o conteudo da pagina-->
            <div id="acontent">
            <#nested/>
            </div>
            <div class="botoes-flutuantes animated slideInRight">${botoesFlutuantes!}</div>
    <#if !modoPopup??>
        </div>
    </section>
    </#if>

</div>

<div id="carregando" style="display: none;">
    <div>
        Carregando...
        <div class="sk-cube-grid">
            <div class="sk-cube sk-cube1"></div>
            <div class="sk-cube sk-cube2"></div>
            <div class="sk-cube sk-cube3"></div>
            <div class="sk-cube sk-cube4"></div>
            <div class="sk-cube sk-cube5"></div>
            <div class="sk-cube sk-cube6"></div>
            <div class="sk-cube sk-cube7"></div>
            <div class="sk-cube sk-cube8"></div>
            <div class="sk-cube sk-cube9"></div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<@spring.url '/static/libs/modernizr/modernizr.custom.js'/>"></script>
<script type="text/javascript" src="<@spring.url '/static/libs/matchMedia/matchMedia.js'/>"></script>
<script type="text/javascript" src="<@spring.url '/static/js/cgu.js?v=@project.version@'/>"></script>
<script type="text/javascript" src="<@spring.url '/static/libs/bootstrap/js/bootstrap.min.js'/>"></script>
<script type="text/javascript" src="<@spring.url '/static/libs/bootstrap-confirmation/bootstrap-confirmation.min.js'/>"></script>
<script src="<@spring.url "/static/libs/jquery-validation/jquery.validate.min.js"/>"></script>
<script src="<@spring.url "/static/libs/jquery-validation/additional-methods.min.js"/>"></script>
<script src="<@spring.url "/static/libs/jquery-validation/jquery.validate.options.js"/>"></script>
<script src="<@spring.url "/static/libs/jquery-mask-plugin/jquery.mask.min.js"/>"></script>
<script src="<@spring.url "/static/libs/jquery-mask-money/jquery.maskMoney.min.js"/>"></script>
<script src="<@spring.url "/static/libs/jquery.easing/js/jquery.easing.js"/>"></script>
<script src="<@spring.url "/static/libs/jQuery-Storage-API/jquery.storageapi.js"/>"></script>
<script src="<@spring.url "/static/libs/animo.js/animo.js"/>"></script>
<script src="<@spring.url "/static/libs/slimScroll/jquery.slimscroll.min.js"/>"></script>
<script src="<@spring.url "/static/libs/angle/app.js"/>"></script>
<script src="<@spring.url "/static/libs/jquery-fileupload/jquery.ui.widget.js"/>"></script>
<script src="<@spring.url "/static/libs/jquery-fileupload/jquery.fileupload.js"/>"></script>
<script src="<@spring.url "/static/libs/bootstrap-filestyle/bootstrap-filestyle.min.js"/>"></script>

<#include "mensagens.ftl"/>

<!-- Inclusao dos arquivos JavaScript definidos em cada view -->
${script!}

<script>
    $(function(){
    });

</script>

<script type="text/javascript" src="<@spring.url '/static/libs/introjs/intro.min.js'/>"></script>
<link rel="stylesheet" href="<@spring.url '/static/libs/introjs/introjs.min.css'/>" media="screen" />

</body>
</html>

</#macro>

