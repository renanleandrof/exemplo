<#setting number_format="computer">
<#assign url = springMacroRequestContext.getRequestUri() />
<#macro layoutSemMenu titulo="Autorizador ECGE" header="" script="" scriptOnTop="" breadcrumb="" botoesFlutuantes="" offside="">
<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="eAUD - CGU">
    <title>${titulo!}</title>
    <script> FontAwesomeConfig = { searchPseudoElements: true }; </script>

<#--Favicons-->
    <link rel="apple-touch-icon" sizes="57x57"         href="<@spring.url '/static/'/>favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60"         href="<@spring.url '/static/'/>favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72"         href="<@spring.url '/static/'/>favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76"         href="<@spring.url '/static/'/>favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114"       href="<@spring.url '/static/'/>favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120"       href="<@spring.url '/static/'/>favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144"       href="<@spring.url '/static/'/>favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152"       href="<@spring.url '/static/'/>favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180"       href="<@spring.url '/static/'/>favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<@spring.url '/static/'/>favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32"    href="<@spring.url '/static/'/>favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96"    href="<@spring.url '/static/'/>favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16"    href="<@spring.url '/static/'/>favicon/favicon-16x16.png">
    <link rel="icon"    href="<@spring.url '/static/'/>favicon.ico">
    <link rel="manifest"                               href="<@spring.url '/static/'/>favicon/manifest.json">
    <meta name="msapplication-TileImage"            content="<@spring.url '/static/'/>favicon/ms-icon-144x144.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" href="<@spring.url '/static/libs/fontawesome/css/fontawesome-all.min.css'/>"/>
    <link rel="stylesheet" href="<@spring.url '/static/libs/simple-line-icons/css/simple-line-icons.css'/>"/>
    <link rel="stylesheet" href="<@spring.url '/static/libs/bootstrap/css/bootstrap.min.css'/>" media="screen" id="bscss"/>
    <link rel="stylesheet" href="<@spring.url '/static/libs/googlefonts/SourceSansPro.css'/>"/>
    <link rel="stylesheet" href="<@spring.url '/static/css/app.css'/>" id="maincss"/>
    <link rel="stylesheet" href="<@spring.url '/static/css/azul-black.css'/>"/>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" media="screen"/>
    <script type="text/javascript" src="<@spring.url '/static/libs/jquery/jquery-1.12.0.min.js'/>"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <link rel="stylesheet" href="<@spring.url '/static/css/cgu.css?v=@project.version@'/>" media="screen"/>
    <script type="text/javascript">
        let springUrl = "<@spring.url '/'/>".split(";")[0];
        <#if usuarioLogado??>
        let usuarioLogado = "${usuarioLogado.nome}";
        </#if>
    </script>
</head>

<body>
    <#include "../layout/mensagens.ftl"/>

<div class="wrapper">
    <#nested/>
</div>

<script type="text/javascript" src="<@spring.url '/static/libs/modernizr/modernizr.custom.js'/>"></script>
<script type="text/javascript" src="<@spring.url '/static/js/cgu.js?v=@project.version@'/>"></script>
<script type="text/javascript" src="<@spring.url '/static/libs/bootstrap/js/bootstrap.min.js'/>"></script>

</body>
</html>
</#macro>

