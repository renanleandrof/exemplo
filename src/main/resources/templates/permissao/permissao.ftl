<#ftl output_format="HTML">
<#include "../_formulario.ftl"/>
<#-- @ftlvariable name="permissao" type="br.com.ecge.autorizador.negocio.Permissao" -->
<#assign scriptContent>
    <@autocompleteScripts/>
    <script src="<@spring.url "/static/js/auth/CadastroPermissao.js"/>"></script>
</#assign>

<#assign breadcrumb>
<i class="fas fa-desktop"></i> Cadastro de Permissao
</#assign>
<@layout.portal script=scriptContent titulo="Cadastro de Permissão" breadcrumb=breadcrumb>

    <form id="form" method="post">
        <input type="hidden" id="id" name="id" value="${permissao.id!}" class="form-control"/>

        <@bs.panel id="panelDadosSistema" titulo="Dados da Permissão">
            <fieldset>
                <legend class="hidden">Dados da Permissão</legend>

                <@campoTextoFormulario label="Nome" id="nome" obrigatorio=true value=permissao.nome!/>
                <#if idSistema??>
                    <@autocomplete label="Sistema" id="idSistema" tipo="br.com.ecge.autorizador.negocio.Sistema" valores=[idSistema] maximoItens=1/>
                <#else>
                    <@autocomplete label="Sistema" id="idSistema" tipo="br.com.ecge.autorizador.negocio.Sistema" valores=[permissao.sistema!] tipoValores="ENTIDADE" maximoItens=1/>
                </#if>

            </fieldset>

            <a class="btn btn-default btn-labeled" href='<@spring.url "/auth/permissoes"/>'><span class="btn-label"><i class="fa fa-arrow-left"></i></span> Voltar</a>
            <span class="pull-right">
                <button id="btnSalvar" type="button" class="btn btn-primary btn-labeled"><span class="btn-label"><i class="fa fa-save"></i></span>Salvar</button>
            </span>
        </@bs.panel>
    </form>

</@layout.portal>