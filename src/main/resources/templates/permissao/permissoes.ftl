<#-- @ftlvariable name="filtro" type="br.com.ecge.autorizador.aplicacao.PermissaoFiltro" -->
<#include "../_formulario.ftl"/>
<#include "../_datatables.ftl"/>
<#assign scriptContent>
    <@autocompleteScripts/>
    <@datatableScripts/>

<script type="text/javascript" src="<@spring.url "/static/js/auth/PesquisaPermissao.js"/>"></script>
<script>
    $(function () {
         PesquisaPermissao.init();
    });
</script>

</#assign>

<#assign breadcrumb>
<i class="fas fa-id-card-alt"></i> Pesquisa de Permissões
</#assign>

<#assign offside>
    <@boxFiltros>
        <@campoTextoFormulario label="Nome" id="nome" value=filtro.nome! />
        <@autocomplete label="Sistema" id="sistema" tipo="br.com.ecge.autorizador.negocio.Sistema" valores=filtro.sistema maximoItens=1/>

        <div class="form-group">
            <label for="ativo">Situação </label>
            <select class="form-control" id="ativo" name="ativo">
                <option value="" <#if !(filtro.ativo??)>selected="selected"</#if>>Todos</option>
                <option value="true" <#if filtro.ativo?? && filtro.ativo>selected="selected"</#if>>Ativos</option>
                <option value="false" <#if filtro.ativo?? && !filtro.ativo>selected="selected"</#if>>Inativos</option>
            </select>
        </div>
    </@boxFiltros>
</#assign>


<#assign botoesFlutuantes>
    <@botaoFiltros/>

    <a class="btn btn-info btn-floating-action botao-flutuante z-depth-3" href="<@spring.url '/auth/permissoes/novo'/>"
       data-toggle="tooltip" title="Nova Permissão"><i class="fa fa-plus"></i></a>
</#assign>

<@layout.portal script=scriptContent titulo="Permissões" breadcrumb=breadcrumb botoesFlutuantes=botoesFlutuantes offside=offside>
    <@filtrosUtilizadosNaPesquisa />

    <@bs.panelTable id="panelTable">
        <@datatableHtml id="lista" titulos=["Id","Nome", "Sistema","Ações"]/>
    </@bs.panelTable>

</@layout.portal>