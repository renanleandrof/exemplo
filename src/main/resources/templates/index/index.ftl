<#include "../layout/layout_sem_menu.ftl"/>
<@layoutSemMenu >
<div class="block-center mt-xl wd-xl">
    <div class="panel panel-dark panel-flat">
        <div class="panel-heading text-center">
            <a class="navbar-brand" href="<@spring.url "/"/>">
                <div class="brand-logo">
                    ECGE
                    <small title="Versão: @project.version@ ---- Build: @buildTimestamp@" data-toggle="tooltip" data-placement="bottom"><i class="fas fa-code-branch"></i> @project.version@</small>
                </div>
            </a>
            <a href="#">
                <img class="block-center img-rounded" alt="Image" src="<@spring.url '/static/'/>favicon/apple-icon-57x57.png"/>
            </a>
        </div>
        <div class="panel-body animated bounceIn">

                <form class="mb-lg" role="form" data-parsley-validate="" novalidate="" method="POST" action="<@spring.url '/login'/>">
                    <h3 class="text-thin text-muted">Login</h3>

                    <div class="form-group has-feedback">
                        <input class="form-control" id="username" type="text" name="username" placeholder="Informe um login" autocomplete="off" required>
                        <span class="fa fa-user form-control-feedback text-muted"></span>
                    </div>

                    <div class="form-group has-feedback">
                        <input class="form-control" id="password" type="password" name="password" placeholder="Senha" autocomplete="off" required>
                        <span class="fa fa-lock form-control-feedback text-muted"></span>
                    </div>

                    <button class="btn btn-block btn-primary mt-lg" type="submit" id="btnSimular">Login</button>

                </form>

        </div>
    </div>

    <div class="p-lg text-center">
        <span>@project.url@</span>
    </div>
</div>
</@layoutSemMenu>

