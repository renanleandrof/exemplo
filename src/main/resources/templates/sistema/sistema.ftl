<#ftl output_format="HTML">
<#include "../_formulario.ftl"/>
<#-- @ftlvariable name="sistema" type="br.com.ecge.autorizador.negocio.Sistema" -->
<#assign scriptContent>
    <@autocompleteScripts/>
    <script src="<@spring.url "/static/js/auth/CadastroSistema.js"/>"></script>
</#assign>

<#assign breadcrumb>
<i class="fas fa-desktop"></i> Cadastro de Sistema
</#assign>
<@layout.portal script=scriptContent titulo="Cadastro de Sistema" breadcrumb=breadcrumb>

    <form id="form" method="post" onsubmit="CadastroSistema.salvar();">
        <input type="hidden" id="id" name="id" value="${sistema.id!}" class="form-control"/>

        <@bs.panel id="panelDadosSistema" titulo="Dados do Sistema">
            <fieldset>
                <legend class="hidden">Dados do Sistema</legend>

                <@campoTextoFormulario label="Nome" id="nome" obrigatorio=true value=sistema.nome!/>

            </fieldset>

            <a class="btn btn-default btn-labeled" href='<@spring.url "/auth/sistemas"/>'><span class="btn-label"><i class="fa fa-arrow-left"></i></span> Voltar</a>
            <span class="pull-right">
                <button id="btnSalvar" type="button" class="btn btn-primary btn-labeled"><span class="btn-label"><i class="fa fa-save"></i></span>Salvar</button>
            </span>
        </@bs.panel>
    </form>

</@layout.portal>