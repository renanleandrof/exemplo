<#-- @ftlvariable name="filtro" type="br.com.ecge.autorizador.web.SistemaFiltro" -->
<#include "../_formulario.ftl"/>
<#include "../_datatables.ftl"/>
<#assign scriptContent>
    <@autocompleteScripts/>
    <@datatableScripts/>

<script type="text/javascript" src="<@spring.url "/static/js/auth/PesquisaSistema.js"/>"></script>
<script>
    $(function () {
         PesquisaSistema.init();
    });
</script>

</#assign>

<#assign breadcrumb>
<i class="fas fa-desktop"></i> Pesquisa de Sistemas
</#assign>

<#assign offside>
    <@boxFiltros>
        <@campoTextoFormulario label="Nome" id="nome" value=filtro.nome! />
        <div class="form-group">
            <label for="ativo">Situação </label>
            <select class="form-control" id="ativo" name="ativo">
                <option value="" <#if !(filtro.ativo??)>selected="selected"</#if>>Todos</option>
                <option value="true" <#if filtro.ativo?? && filtro.ativo>selected="selected"</#if>>Ativos</option>
                <option value="false" <#if filtro.ativo?? && !filtro.ativo>selected="selected"</#if>>Inativos</option>
            </select>
        </div>
    </@boxFiltros>
</#assign>


<#assign botoesFlutuantes>
    <@botaoFiltros/>

    <a class="btn btn-info btn-floating-action botao-flutuante z-depth-3" href="<@spring.url '/auth/sistemas/novo'/>"
       data-toggle="tooltip" title="Novo Sistema"><i class="fa fa-plus"></i></a>
</#assign>

<@layout.portal script=scriptContent titulo="Sistemas" breadcrumb=breadcrumb botoesFlutuantes=botoesFlutuantes offside=offside>
    <@filtrosUtilizadosNaPesquisa />

    <@bs.panelTable id="panelTableSistemas">
        <@datatableHtml id="lista" titulos=["Id","Nome", "Ações"]/>
    </@bs.panelTable>

</@layout.portal>