<#ftl output_format="HTML" auto_esc=true>
<#macro collapsiblePanel id titulo collapsed=true class="panel-default">
<div class="panel ${class}" id="${id}" >
    <div data-toggle="collapse" href="#panel_${id}" aria-expanded="${collapsed?then("false","true")}" class="panel-heading ${collapsed?then("collapsed","")}">
        ${titulo}
        <a class="pull-right" data-toggle="tooltip" title="Abrir/Fechar painel" data-tool="collapse">
            <i class="fas fa-plus"></i><i class="fas fa-minus"></i>
        </a>
    </div>
    <div class="panel-body collapse ${collapsed?then("","in")}" id="panel_${id}" aria-expanded="${collapsed?then("false","true")}">
        <#nested />
    </div>
</div>
</#macro>

<#macro panel id="" titulo="" class="panel-default" footer="" intro="" style="">
    <div class="panel ${class}" id="${id}" style="${style}"
         <#if intro!= "">
         data-intro="${intro}"
         </#if>
    >
        <#if (titulo?is_string && titulo != "") || !titulo?is_string >
        <div class="panel-heading">${titulo?no_esc}</div>
        </#if>
        <div class="panel-body">
            <#nested />
        </div>
        <#if (footer?is_string && footer != "") || !footer?is_string >
        <div class="panel-footer">${footer}</div>
        </#if>
    </div>
</#macro>


<#macro panelTable id="" titulo="" class="panel-default" footer="" intro="" style="" body="">
<div class="panel ${class}" id="${id}" style="${style}"
    <#if intro!= "">
     data-intro="${intro}"
    </#if>
>
    <#if (titulo?is_string && titulo != "") || !titulo?is_string >
        <div class="panel-heading">${titulo?no_esc}</div>
    </#if>
    <#if (body?is_string && body != "") || !body?is_string >
    <div class="panel-body">
        ${body}
    </div>
    </#if>
    <#if (footer?is_string && footer != "") || !footer?is_string >
        <div class="panel-footer">${footer}</div>
    </#if>
    <#nested />
</div>
</#macro>