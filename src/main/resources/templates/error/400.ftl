<#include "../layout/layout_sem_menu.ftl"/>
<@layoutSemMenu>
    <div class="block-center mt-xl wd-xl">
        <div class="panel panel-dark panel-flat">
            <div class="panel-heading text-center">
                <a class="navbar-brand" href="<@spring.url "/"/>">
                    <div class="brand-logo">
                        @project.name@
                        <small title="Versão: @project.version@ ---- Build: @buildTimestamp@" data-toggle="tooltip" data-placement="bottom"><i class="fas fa-code-branch"></i> @project.version@</small>
                    </div>
                </a>
                <a href="#">
                    <img class="block-center img-rounded" alt="Image" src="<@spring.url '/static/'/>favicon/apple-icon-57x57.png"/>
                </a>
            </div>
            <div class="panel-body">
                <div class="text-center">
                    <h1 class="e404"><i class="far fa-file-code animated bounceIn"></i></h1>
                    <p>Solicitação Inválida</p>
                </div>
            </div>
        </div>

    </div>
</@layoutSemMenu>


