<#include "../layout/layout_sem_menu.ftl"/>
<@layoutSemMenu>
    <div class="block-center mt-xl wd-xl">
        <div class="panel panel-dark panel-flat">
            <div class="panel-heading text-center">
                <a class="navbar-brand" href="<@spring.url "/"/>">
                    <div class="brand-logo">
                        @project.name@
                        <small title="Versão: @project.version@ ---- Build: @buildTimestamp@" data-toggle="tooltip" data-placement="bottom"><i class="fas fa-code-branch"></i> @project.version@</small>
                    </div>
                </a>
                <a href="#">
                    <img class="block-center img-rounded" alt="Image" src="<@spring.url '/static/'/>favicon/apple-icon-57x57.png"/>
                </a>
            </div>
            <div class="panel-body">
                <div class="text-center">
                    <h1><i class="far fa-5x fa-frown text-info"></i></h1>
                    <p>Aconteceu um erro no nosso servidor. Tente novamente mais tarde.</p>
                    <p>Os administradores já foram notificados do erro.</p>
                    <p>${.now}</p>
                    <br/>
                    <p><b>Código do erro:</b></p>
                    <h2 style="color:crimson">${uniqueID!}</h2>

                </div>
            </div>
        </div>

        <div class="p-lg text-center">
            <span>@project.url@</span>
        </div>
    </div>
</@layoutSemMenu>
