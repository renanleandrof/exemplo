CREATE TABLE auth.Usuario
(
  IdUsuario  SERIAL NOT NULL,
  NomUsuario VARCHAR(1000)    NOT NULL,
  PwdSenha   VARCHAR(1000)    NOT NULL,
  DescLogin  VARCHAR(500)     NOT NULL UNIQUE,
  EmlUsuario VARCHAR(200),
  ChaveApi   VARCHAR(32),
  FlgAtivo   BOOLEAN              NOT NULL,

  CONSTRAINT PK_Usuario PRIMARY KEY (IdUsuario)
);

CREATE TABLE auth.Sistema
(
  IdSistema  SERIAL NOT NULL,
  NomSistema VARCHAR(1000)    NOT NULL,
  FlgAtivo   BOOLEAN              NOT NULL,

  CONSTRAINT PK_Sistema PRIMARY KEY (IdSistema)
);

CREATE TABLE log.SistemaLog
(
  REV        INT NOT NULL,
  REVTYPE    SMALLINT,
  IdSistema  INTEGER,
  NomSistema VARCHAR(1000),
  FlgAtivo   BOOLEAN
);


CREATE TABLE log.UsuarioLog
(
  REV        INT           NOT NULL,
  REVTYPE    SMALLINT,
  IdUsuario  INTEGER       NOT NULL,
  NomUsuario VARCHAR(1000) NOT NULL,
  PwdSenha   VARCHAR(1000) NOT NULL,
  DescLogin  VARCHAR(500)  NOT NULL,
  EmlUsuario VARCHAR(200),
  ChaveApi   VARCHAR(32),
  FlgAtivo   BOOLEAN           NOT NULL
);

CREATE TABLE auth.Permissao
(
  IdPermissao   SERIAL NOT NULL,
  DescPermissao VARCHAR(500)     NOT NULL,
  IdSistema     integer          not null,
  FlgAtivo   BOOLEAN           NOT NULL,

  CONSTRAINT PK_Permissao PRIMARY KEY (IdPermissao),
  CONSTRAINT FK_Permissao_Sistema FOREIGN KEY (IdSistema) REFERENCES auth.Sistema (IdSistema)
);


CREATE TABLE log.PermissaoLog
(
  REV           INT NOT NULL,
  REVTYPE       SMALLINT,
  IdPermissao   INTEGER,
  DescPermissao VARCHAR(500),
  IdSistema     integer,
  FlgAtivo   BOOLEAN
);


CREATE TABLE auth.PermissoesDoUsuario
(
  IdPermissao INT NOT NULL,
  IdUsuario   INT NOT NULL,

  CONSTRAINT PK_PerfisDoUsuario PRIMARY KEY (IdPermissao, IdUsuario),
  CONSTRAINT FK_PerfisDoUsuario_Permissao FOREIGN KEY (IdPermissao) REFERENCES auth.Permissao (IdPermissao),
  CONSTRAINT FK_PerfisDoUsuario_Usuario FOREIGN KEY (IdUsuario) REFERENCES auth.Usuario (IdUsuario)
);

CREATE TABLE log.PermissoesDoUsuarioLog
(
  REV         INT     NOT NULL,
  REVTYPE     SMALLINT,
  IdPermissao INTEGER NOT NULL,
  IdUsuario   INTEGER NOT NULL
);

CREATE TABLE log.Revisao
(
  id         SERIAL NOT NULL,
  dthRevisao TIMESTAMP     NOT NULL,
  idUsuario  INT,

  CONSTRAINT PK_Revisao PRIMARY KEY (id)
);


create table auth.oauth_client_details (
  client_id               VARCHAR(256) PRIMARY KEY,
  resource_ids            VARCHAR(256),
  client_secret           VARCHAR(256),
  scope                   VARCHAR(256),
  authorized_grant_types  VARCHAR(256),
  web_server_redirect_uri VARCHAR(256),
  authorities             VARCHAR(256),
  access_token_validity   INTEGER,
  refresh_token_validity  INTEGER,
  additional_information  VARCHAR(4096),
  autoapprove             VARCHAR(256)
);

create table auth.oauth_client_token (
  token_id          VARCHAR(256),
  token             BYTEA,
  authentication_id VARCHAR(256) PRIMARY KEY,
  user_name         VARCHAR(256),
  client_id         VARCHAR(256)
);

create table auth.oauth_access_token (
  token_id          VARCHAR(256),
  token             BYTEA,
  authentication_id VARCHAR(256) PRIMARY KEY,
  user_name         VARCHAR(256),
  client_id         VARCHAR(256),
  authentication    BYTEA,
  refresh_token     VARCHAR(256)
);

create table auth.oauth_refresh_token (
  token_id       VARCHAR(256),
  token          BYTEA,
  authentication BYTEA
);

create table auth.oauth_code (
  code           VARCHAR(256),
  authentication BYTEA
);

create table auth.oauth_approvals (
  userId         VARCHAR(256),
  clientId       VARCHAR(256),
  scope          VARCHAR(256),
  status         VARCHAR(10),
  expiresAt      TIMESTAMP,
  lastModifiedAt TIMESTAMP
);