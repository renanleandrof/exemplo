INSERT INTO auth.OAUTH_CLIENT_DETAILS (CLIENT_ID,
                                       RESOURCE_IDS,
                                       CLIENT_SECRET,
                                       SCOPE,
                                       AUTHORIZED_GRANT_TYPES,
                                       AUTHORITIES,
                                       ACCESS_TOKEN_VALIDITY,
                                       REFRESH_TOKEN_VALIDITY)
VALUES ('ecge',
        'resource-server-rest-api',
        '$2a$04$soeOR.QFmClXeFIrhJVLWOQxfHjsJLSpWrU1iGxcMGdu.a5hvfY4W',
        'read,write',
        'password,authorization_code,refresh_token,implicit',
        'USER',
        10800,
        2592000);

INSERT INTO auth.Usuario (NomUsuario, DescLogin, EmlUsuario, ChaveApi, FlgAtivo, PwdSenha)
VALUES (
        'Admin ECGE',
        'admin',
        'ecge@ecge.com.br',
        null,
        true,
        --admin1234
        '$2a$08$qvrzQZ7jJ7oy2p/msL4M0.l83Cd0jNsX6AJUitbgRXGzge4j035ha');

insert into auth.Sistema ( NomSistema, FlgAtivo)
values ('ECGE Autorizador', true);

INSERT INTO auth.Permissao ( DescPermissao, IdSistema, FlgAtivo)
VALUES ('CONSULTAR_USUARIOS', 1, true),
       ('GERENCIAR_USUARIOS', 1, true),
       ('CONSULTAR_PERMISSOES', 1, true),
       ('GERENCIAR_PERMISSOES', 1, true),
       ('CONSULTAR_SISTEMAS', 1, true),
       ('GERENCIAR_SISTEMAS', 1, true);


INSERT INTO auth.PermissoesDoUsuario (IdUsuario, IdPermissao)
VALUES (1, 1),
       (1, 2),
       (1, 3),
       (1, 4),
       (1, 5),
       (1, 6);