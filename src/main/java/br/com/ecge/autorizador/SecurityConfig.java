package br.com.ecge.autorizador;

import br.com.ecge.autorizador.aplicacao.Autenticador;
import br.com.ecge.autorizador.web.APIAuthenticationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired private APIAuthenticationFilter apiAuthenticationFilter;

    @Autowired
    private Autenticador userDetailsService;

    @Autowired
    private PasswordEncoder userPasswordEncoder;

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .userDetailsService(userDetailsService)
                .passwordEncoder(userPasswordEncoder)
        ;
    }

      @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.antMatcher("/**")
                .addFilterBefore(apiAuthenticationFilter, BasicAuthenticationFilter.class)
                .formLogin().loginPage("/login")
                .and().authorizeRequests()
                .regexMatchers("/auth.*", "/api/auth.*").authenticated()
                .and().logout().logoutSuccessUrl("/").permitAll()
                .and().csrf().disable()
                .headers()
                .frameOptions().sameOrigin()
                .xssProtection();
    }

}