package br.com.ecge.autorizador;

public final class Constantes {

    public static final String DB_SCHEMA = "auth";

    public static final String MODEL_SUCESSO = "sucesso";
    public static final String OPERACAO_REALIZADA_COM_SUCESSO = "Operação realizada com sucesso.";
    public static final String QUEBRA_DE_LINHA_DE_MENSAGEM = "<br/>";

    public static final String SWAGGER_TAG_AUTORIZADOR = "API do sistema";
    public static final String SWAGGER_TAG_ENDPOINTS = "Endpoints para clientes";

    private Constantes() {
        /*Hide do construtor de classe utilitaria*/
    }

}
