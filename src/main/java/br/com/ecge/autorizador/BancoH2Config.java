package br.com.ecge.autorizador;

import com.querydsl.sql.H2Templates;
import com.querydsl.sql.SQLTemplates;
import org.h2.tools.Server;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

import javax.sql.DataSource;
import java.sql.SQLException;

@Configuration
@Profile(value = "h2")
public class BancoH2Config {

    public static EmbeddedDatabase datasource;

    @Bean
    public DataSource devDataSource() {
        datasource = new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2)
                .setScriptEncoding("UTF-8")
                .addScripts("classpath:sql/h2options.sql", "classpath:sql/esquema.sql", "classpath:sql/dados.sql")
                .build();
        return datasource;
    }

    @Bean
    public SQLTemplates sqlTemplate() {
        return H2Templates.builder().printSchema().build();
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        return BootApplication.entityManagerFactory(devDataSource(), "true", "infraestrutura.persistencia.jpa.h2.PortalH2Dialect");
    }

    @Bean(initMethod = "start", destroyMethod = "stop")
    public Server h2Server() throws SQLException {
        return Server.createTcpServer("-tcp", "-tcpAllowOthers", "-tcpPort", "9092");
    }
}
