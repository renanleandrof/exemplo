package br.com.ecge.autorizador;

import com.querydsl.sql.SQLTemplates;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

import javax.sql.DataSource;

@Configuration
@Profile(value = "postgres")
public class BancoPostgresConfig {

    @Value("${bd.user}")
    private String user;
    @Value("${bd.pwd}")
    private String pwd;
    @Value("${bd.url}")
    private String url;

    @Bean
    public DataSource dataSourceProd() {
        return BootApplication.dataSource(user,pwd,url);
    }

    @Bean
    public SQLTemplates sqlTemplateProd() {
        return BootApplication.sqlTemplate();
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactoryProd() {
        return BootApplication.entityManagerFactory(dataSourceProd(), "false","org.hibernate.dialect.PostgreSQL95Dialect");
    }
}
