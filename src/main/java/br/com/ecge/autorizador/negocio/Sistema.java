package br.com.ecge.autorizador.negocio;

import br.com.ecge.autorizador.Constantes;
import com.fasterxml.jackson.annotation.JsonIgnore;
import infraestrutura.persistencia.jpa.Entidade;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Audited
@Entity
@Table(name = "Sistema", schema = Constantes.DB_SCHEMA)
public class Sistema implements Entidade<Integer>, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IdSistema")
    private Integer id;

    @Column(name = "NomSistema")
    @NotBlank(message = "Nome do Sistema não pode estar em branco")
    @Size(min = 1, max = 1000, message = "Tamanho máximo do Sistema é de 1000 caracteres")
    private String nome;

    @Column(name = "FlgAtivo")
    private boolean ativo = true;

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Sistema este = (Sistema) o;
        return id.equals(este.id);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @JsonIgnore
    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    @Override
    public String toString() {
        return nome;
    }
}