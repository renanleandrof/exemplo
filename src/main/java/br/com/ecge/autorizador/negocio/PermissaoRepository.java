package br.com.ecge.autorizador.negocio;

import infraestrutura.persistencia.jpa.RepositoryJpa;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PermissaoRepository extends RepositoryJpa<Permissao, Integer> {
    private QPermissao permissao = QPermissao.permissao;

    public List<Permissao> buscarTodosOrdenadosPorNome() {
        return getJPAQuery()
                .selectFrom(permissao)
                .orderBy(permissao.nome.asc())
                .fetch();
    }

    public List<Permissao> getPorIds(List<Integer> ids) {
        return getJPAQuery()
                .selectFrom(permissao)
                .where(permissao.id.in(ids))
                .orderBy(permissao.nome.asc())
                .fetch();
    }

    public List<Permissao> getPorNomes(List<String> nomes) {
        return getJPAQuery()
                .selectFrom(permissao)
                .where(permissao.nome.in(nomes))
                .orderBy(permissao.nome.asc())
                .fetch();
    }
}
