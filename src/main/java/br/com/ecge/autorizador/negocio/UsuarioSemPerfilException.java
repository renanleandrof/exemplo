package br.com.ecge.autorizador.negocio;

public class UsuarioSemPerfilException extends RuntimeException {
    public UsuarioSemPerfilException() {
        super("Usuário sem perfil para realizar a ação. Solicite a regularização a um administrador.");
    }
    
    public UsuarioSemPerfilException(Permissao permissao) {
        super("Somente usuários com a permissão " + permissao.getNome() + " podem realizar essa ação.");
    }
}
