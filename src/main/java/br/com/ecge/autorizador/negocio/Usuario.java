package br.com.ecge.autorizador.negocio;

import br.com.ecge.autorizador.Constantes;
import com.fasterxml.jackson.annotation.JsonIgnore;
import infraestrutura.PasswordUtils;
import infraestrutura.persistencia.jpa.Entidade;
import org.hibernate.envers.Audited;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static org.hibernate.envers.RelationTargetAuditMode.NOT_AUDITED;
import static org.hibernate.internal.util.collections.CollectionHelper.isNotEmpty;

@Audited
@Entity
@Table(name = "Usuario", schema = Constantes.DB_SCHEMA)
public class Usuario implements Entidade<Integer>, Serializable, UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IdUsuario")
    private Integer id;

    @Column(name = "NomUsuario")
    @NotBlank(message = "Nome do usuário não pode estar em branco")
    @Size(min = 1, max = 1000, message = "Tamanho máximo do Nome é de 1000 caracteres")
    private String nome;

    @Column(name = "DescLogin", unique = true)
    @NotBlank(message = "Login é obrigatório")
    private String login;

    @Column(name = "PwdSenha", unique = true)
    @NotBlank(message = "Senha é obrigatória")
    private String senha;

    @Column(name = "EmlUsuario")
    @Size(max = 200, message = "Tamanho máximo do E-mail é de 200 caracteres")
    @Email(message = "E-mail inválido.")
    private String email;

    @Column(name = "ChaveApi")
    @Size(max = 32, message = "Tamanho máximo da Chave API é de 32 caracteres")
    private String chaveApi;

    @Column(name = "FlgAtivo")
    private boolean ativo = true;

    @Audited(targetAuditMode = NOT_AUDITED)
    @ManyToMany
    @JoinTable(name = "PermissoesDoUsuario", schema = Constantes.DB_SCHEMA,
            joinColumns = {@JoinColumn(name = "IdUsuario")},
            inverseJoinColumns = {@JoinColumn(name = "IdPermissao")})
    private List<Permissao> permissoes = new ArrayList<>();

    @JsonIgnore
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return permissoes;
    }

    @JsonIgnore
    @Override
    public String getPassword() {
        return getSenha();
    }

    @JsonIgnore
    @Override
    public String getUsername() {
        return getLogin();
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isEnabled() {
        return isAtivo();
    }

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public boolean hasPermissao(Permissao permissao) {
        return isNotEmpty(permissoes) && permissoes.stream().anyMatch(p -> p.equals(permissao));
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public List<Permissao> getPermissoes() {
        return permissoes;
    }

    public void setPermissoes(List<Permissao> permissoes) {
        this.permissoes = permissoes;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getChaveApi() {
        return chaveApi;
    }

    public void setChaveApi(String chaveApi) {
        this.chaveApi = chaveApi;
    }

    public String sugerirChaveApi() {
        return PasswordUtils.hashMD5(getLogin() + LocalDateTime.now().toString());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Usuario usuario = (Usuario) o;
        return id.equals(usuario.id);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        if (nome == null) {
            return "";
        }

        return nome;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public Map<Sistema, List<Permissao>> getPermissoesPorSistema() {
        if (permissoes != null) {
            return permissoes.stream().collect(Collectors.groupingBy(Permissao::getSistema, Collectors.toList()));
        }
        return new HashMap<>();
    }
}