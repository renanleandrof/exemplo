package br.com.ecge.autorizador.negocio;

import br.com.ecge.autorizador.Constantes;
import com.fasterxml.jackson.annotation.JsonIgnore;
import infraestrutura.persistencia.jpa.Entidade;
import org.hibernate.envers.Audited;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.io.Serializable;

@Audited
@Entity
@Table(name = "Permissao", schema = Constantes.DB_SCHEMA)
public class Permissao implements Entidade<Integer>, GrantedAuthority, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IdPermissao", unique = true, nullable = false)
    private Integer id;

    @Column(name = "DescPermissao")
    private String nome;

    @ManyToOne
    @JoinColumn(name = "IdSistema")
    private Sistema sistema;

    @Column(name = "FlgAtivo")
    private boolean ativo = true;

    public Permissao() {
        /* constutor padrão do hibernate */
    }

    public Permissao(int id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Sistema getSistema() {
        return sistema;
    }

    @JsonIgnore
    public String getNomeSistema() {
        return sistema.getNome();
    }

    public void setSistema(Sistema sistema) {
        this.sistema = sistema;
    }

    public void setIdSistema(Integer id ) {
        this.sistema = new Sistema();
        this.sistema.setId(id);
    }

    @Override
    public boolean equals(Object o) {
        return this == o || !(o == null || getClass() != o.getClass()) && id.equals(((Permissao) o).id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    @JsonIgnore
    public String getAuthority() {
        return nome;
    }

    @JsonIgnore
    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }
}