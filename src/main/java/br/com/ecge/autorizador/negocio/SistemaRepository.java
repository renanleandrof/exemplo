package br.com.ecge.autorizador.negocio;

import infraestrutura.persistencia.jpa.RepositoryJpa;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class SistemaRepository extends RepositoryJpa<Sistema, Integer> {

    private static final QSistema sistema = QSistema.sistema;

    public List<Sistema> getPorTermo(String termo) {
        return getJPAQuery()
                .select(sistema)
                .from(sistema)
                .where(
                        sistema.nome.contains(termo)
                )
                .limit(10)
                .fetch();
    }

    public List<Sistema> getPorIds(List<Integer> ids) {
        return getJPAQuery()
                .selectFrom(sistema)
                .where(sistema.id.in(ids))
                .fetch();
    }
}
