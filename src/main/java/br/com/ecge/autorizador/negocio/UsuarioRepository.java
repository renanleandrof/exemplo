package br.com.ecge.autorizador.negocio;

import infraestrutura.persistencia.jpa.RepositoryJpa;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static infraestrutura.persistencia.jpa.JPAUtils.initializeObject;


@Repository
public class UsuarioRepository extends RepositoryJpa<Usuario, Integer> {
    private QUsuario usuario = QUsuario.usuario;


    @Transactional
    public Usuario getPorLogin(String login) {
        Usuario usuario = getJPAQuery()
                .select(this.usuario)
                .from(this.usuario)
                .where(this.usuario.login.eq(login))
                .fetchOne();
        if (usuario != null) {
            initializeObject(usuario);
        }
        return usuario;
    }

    @Transactional
    public Usuario getPorChaveApi(String chaveApi) {
        Usuario entidade = getJPAQuery()
                .select(this.usuario)
                .from(this.usuario)
                .where(this.usuario.chaveApi.eq(chaveApi))
                .fetchOne();
        if (entidade != null) {
            initializeObject(entidade);
        }
        return entidade;
    }

    public List<Usuario> getPorTermo(String termo) {
        return getJPAQuery()
                .select(usuario)
                .from(usuario)
                .where(
                        usuario.nome.contains(termo)
                )
                .limit(10)
                .fetch();
    }

    public List<Usuario> getPorIds(List<Integer> ids) {
        return getJPAQuery()
                .selectFrom(usuario)
                .where(usuario.id.in(ids))
                .fetch();
    }

}
