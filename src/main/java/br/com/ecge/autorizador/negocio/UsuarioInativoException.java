package br.com.ecge.autorizador.negocio;

import org.springframework.security.core.AuthenticationException;

public class UsuarioInativoException extends AuthenticationException {

    public UsuarioInativoException() {
        super("Usuário inativo. Solicite a regularização a um administrador.");
    }

}
