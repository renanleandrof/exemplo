package br.com.ecge.autorizador.web;

import br.com.ecge.autorizador.aplicacao.BuscadorDePermissao;
import br.com.ecge.autorizador.aplicacao.BuscadorDeUsuario;
import br.com.ecge.autorizador.aplicacao.GerenciadorDeUsuario;
import br.com.ecge.autorizador.aplicacao.UsuarioForm;
import br.com.ecge.autorizador.negocio.Usuario;
import infraestrutura.persistencia.jpa.EntidadeInvalidaException;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import static br.com.ecge.autorizador.Constantes.*;

@Controller
@RequestMapping
public class GerenciarUsuarioController {

    private final GerenciadorDeUsuario gerenciadorDeUsuario;
    private final BuscadorDeUsuario buscadorDeUsuario;
    private final BuscadorDePermissao buscadorDePermissao;

    @Autowired
    public GerenciarUsuarioController(GerenciadorDeUsuario gerenciadorDeUsuario, BuscadorDeUsuario buscadorDeUsuario,
                                      BuscadorDePermissao buscadorDePermissao) {
        this.gerenciadorDeUsuario = gerenciadorDeUsuario;
        this.buscadorDeUsuario = buscadorDeUsuario;
        this.buscadorDePermissao = buscadorDePermissao;
    }

    @PreAuthorize("hasAuthority('GERENCIAR_USUARIOS')")
    @RequestMapping(value="/auth/usuarios/novo", method= RequestMethod.GET)
    public String exibirTelaCadastro(Model m) {
        return viewCadastrar(m, new Usuario());
    }

    @PreAuthorize("hasAuthority('GERENCIAR_USUARIOS')")
    @RequestMapping(value="/auth/usuarios/{id}", method= RequestMethod.GET)
    public String exibirTelaEdicao(Model m, @PathVariable Integer id) {
        return viewCadastrar(m, buscadorDeUsuario.getParaEdicao(id));
    }

    @PreAuthorize("hasAuthority('GERENCIAR_USUARIOS')")
    @ApiOperation(value = "Salvar Usuário", notes = "Salvar um usuário novo ou editar um existente.", tags = SWAGGER_TAG_AUTORIZADOR)
    @RequestMapping(value="/api/auth/usuarios/salvar", method= RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<String> salvar(@RequestBody UsuarioForm usuario) {
        try {
            Usuario usuarioBD = gerenciadorDeUsuario.salvar(usuario);
            return new ResponseEntity<>(usuarioBD.getId().toString(), HttpStatus.OK);
        } catch (EntidadeInvalidaException e) {
            return new ResponseEntity<>(StringUtils.join(e.getMensagens(), QUEBRA_DE_LINHA_DE_MENSAGEM), HttpStatus.BAD_REQUEST);
        }
    }

    @PreAuthorize("hasAuthority('GERENCIAR_USUARIOS')")
    @ApiOperation(hidden = true, value = "")
    @ResponseBody
    @RequestMapping(value="/api/auth/usuarios/{id}/resetar-senha", method= RequestMethod.POST)
    public ResponseEntity<String> resetarSenha(@PathVariable Integer id) {
        return new ResponseEntity<>(gerenciadorDeUsuario.resetarSenha(id), HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('GERENCIAR_USUARIOS')")
    @RequestMapping(value = {"/auth/usuarios/alternar-ativacao/{id}", "/auth/usuarios/ativar/{id}", "/auth/usuarios/inativar/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> alterarAtivacao(@PathVariable Integer id) {
        gerenciadorDeUsuario.alternarAtivacao(id);
        return new ResponseEntity<>(OPERACAO_REALIZADA_COM_SUCESSO, HttpStatus.OK);
    }

    private String viewCadastrar(Model m, Usuario usuario) {
        m.addAttribute("usuario", usuario);
        m.addAttribute("permissoesPorSistema", buscadorDePermissao.recuperarTodosPorSistema());
        
        return "usuario/usuario";
    }

}