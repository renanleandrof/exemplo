package br.com.ecge.autorizador.web;

import br.com.ecge.autorizador.Constantes;
import br.com.ecge.autorizador.aplicacao.BuscadorDeUsuario;
import br.com.ecge.autorizador.aplicacao.UsuarioDTO;
import br.com.ecge.autorizador.aplicacao.UsuarioFiltro;
import br.com.ecge.autorizador.negocio.Usuario;
import infraestrutura.DataTablesResponseFactory;
import infraestrutura.datatables.DataTablesResponse;
import infraestrutura.persistencia.querybuilder.RespostaConsulta;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping
public class BuscarUsuarioController {

    private final BuscadorDeUsuario buscador;

    @Autowired
    public BuscarUsuarioController(BuscadorDeUsuario buscador) {this.buscador = buscador;}

    @RequestMapping(value = {"/popup/usuario", "/popup/Usuario"}, method = RequestMethod.GET)
    public String popup(UsuarioFiltro filtro, Model m) {
        m.addAttribute("modoPopup", true);
        return listar(filtro, m);
    }

    @PreAuthorize("hasAuthority('CONSULTAR_USUARIOS')")
    @RequestMapping(value = "/auth/usuarios", method = RequestMethod.GET)
    public String listar(UsuarioFiltro filtro, Model m) {
        m.addAttribute("filtro", filtro);
        return "usuario/usuarios";
    }

    @PreAuthorize("hasAuthority('CONSULTAR_USUARIOS')")
    @RequestMapping(value = "/auth/usuarios/{idUsuario}/detalhar", method = RequestMethod.GET)
    @ResponseBody
    public Usuario detalharUsuario(@PathVariable Integer idUsuario) {
        return buscador.get(idUsuario);
    }

    @PreAuthorize("hasAuthority('CONSULTAR_USUARIOS')")
    @ApiOperation(value = "Consulta de Usuários", tags = Constantes.SWAGGER_TAG_AUTORIZADOR)
    @RequestMapping(value = "/api/auth/usuarios", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public DataTablesResponse<UsuarioDTO> grid(UsuarioFiltro filtro) {
        RespostaConsulta<UsuarioDTO> resposta = buscador.buscar(filtro);
        return DataTablesResponseFactory.build(resposta);
    }

}