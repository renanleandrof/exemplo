package br.com.ecge.autorizador.web;

import br.com.ecge.autorizador.aplicacao.BuscadorDeUsuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController {

    private BuscadorDeUsuario buscadorDeUsuario;

    @Autowired
    public HomeController(BuscadorDeUsuario buscadorDeUsuario){
        this.buscadorDeUsuario = buscadorDeUsuario;
    }

    @RequestMapping(value = "/", method= RequestMethod.GET)
    public ModelAndView paginaInicial(@RequestParam(required = false, defaultValue = "true") boolean autologin) {
        if (buscadorDeUsuario.getUsuarioLogado() != null) {
            return new ModelAndView("redirect:/auth");
        }
        ModelMap model = new ModelMap();
        return new ModelAndView("index/index", model);
    }

    @RequestMapping(value = "/auth", method= RequestMethod.GET)
    public String paginaInicialLogado() {
        return "redirect:/auth/meus-dados";
    }


    @RequestMapping(value = "/auth/trylogin", method= RequestMethod.GET)
    public ResponseEntity<String> tryLogin() {
        if (buscadorDeUsuario.getUsuarioLogado() != null) {
            return new ResponseEntity<>("SUCESSO", HttpStatus.OK);
        }
        return new ResponseEntity<>("FORBIDDEN", HttpStatus.FORBIDDEN);
    }

    @RequestMapping(value = "/_exception", method= RequestMethod.GET)
    public ModelAndView produzirException() {
        throw new RuntimeException("Exception de teste provocada de propósito.");
    }

}