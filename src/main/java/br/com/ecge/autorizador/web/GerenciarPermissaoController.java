package br.com.ecge.autorizador.web;

import br.com.ecge.autorizador.Constantes;
import br.com.ecge.autorizador.aplicacao.BuscadorDePermissao;
import br.com.ecge.autorizador.aplicacao.GerenciadorDePermissao;
import br.com.ecge.autorizador.negocio.Permissao;
import infraestrutura.persistencia.jpa.EntidadeInvalidaException;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import static br.com.ecge.autorizador.Constantes.OPERACAO_REALIZADA_COM_SUCESSO;

@Controller
@RequestMapping
public class GerenciarPermissaoController {

    private final GerenciadorDePermissao gerenciadorDePermissao;
    private final BuscadorDePermissao buscadorDePermissao;

    public GerenciarPermissaoController(GerenciadorDePermissao gerenciadorDePermissao, BuscadorDePermissao buscadorDePermissao) {
        this.gerenciadorDePermissao = gerenciadorDePermissao;
        this.buscadorDePermissao = buscadorDePermissao;
    }

    @PreAuthorize("hasAuthority('GERENCIAR_PERMISSOES')")
    @RequestMapping(value="/auth/permissoes/novo", method= RequestMethod.GET)
    public String exibirTelaCadastro(@RequestParam(required = false) Integer idSistema, Model m) {
        m.addAttribute("idSistema", idSistema);
        return viewCadastrar(m, new Permissao());
    }

    @PreAuthorize("hasAuthority('GERENCIAR_PERMISSOES')")
    @RequestMapping(value="/auth/permissoes/{id}", method= RequestMethod.GET)
    public String exibirTelaEdicao(Model m, @PathVariable Integer id) {
        return viewCadastrar(m, buscadorDePermissao.getParaEdicao(id));
    }

    @PreAuthorize("hasAuthority('GERENCIAR_PERMISSOES')")
    @ApiOperation(value = "Salvar Permissao", notes = "Salvar um permissao novo ou editar um existente.", tags = Constantes.SWAGGER_TAG_AUTORIZADOR)
    @RequestMapping(value="/api/auth/permissoes/salvar", method= RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<String> salvar(@RequestBody Permissao permissao) {
        try {
            Permissao permissaoBD = gerenciadorDePermissao.salvar(permissao);
            return new ResponseEntity<>(permissaoBD.getId().toString(), HttpStatus.OK);
        } catch (EntidadeInvalidaException e) {
            return new ResponseEntity<>(StringUtils.join(e.getMensagens(), Constantes.QUEBRA_DE_LINHA_DE_MENSAGEM), HttpStatus.BAD_REQUEST);
        }
    }

    @PreAuthorize("hasAuthority('GERENCIAR_PERMISSOES')")
    @RequestMapping(value = {"/auth/permissoes/alternar-ativacao/{id}", "/auth/permissoes/ativar/{id}", "/auth/permissoes/inativar/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> alterarAtivacao(@PathVariable Integer id) {
        gerenciadorDePermissao.alternarAtivacao(id);
        return new ResponseEntity<>(OPERACAO_REALIZADA_COM_SUCESSO, HttpStatus.OK);
    }

    private String viewCadastrar(Model m, Permissao permissao) {
        m.addAttribute("permissao", permissao);

        return "permissao/permissao";
    }

}