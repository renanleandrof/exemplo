package br.com.ecge.autorizador.web;

import br.com.ecge.autorizador.Constantes;
import br.com.ecge.autorizador.aplicacao.BuscadorDeUsuario;
import br.com.ecge.autorizador.aplicacao.GerenciadorDeUsuario;
import br.com.ecge.autorizador.aplicacao.UsuarioLogadoForm;
import br.com.ecge.autorizador.negocio.Permissao;
import br.com.ecge.autorizador.negocio.Usuario;
import infraestrutura.persistencia.jpa.EntidadeInvalidaException;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

import static br.com.ecge.autorizador.Constantes.OPERACAO_REALIZADA_COM_SUCESSO;
import static br.com.ecge.autorizador.Constantes.QUEBRA_DE_LINHA_DE_MENSAGEM;
import static java.util.stream.Collectors.*;

@Controller
@RequestMapping
public class MeusDadosController {

    private final GerenciadorDeUsuario gerenciadorDeUsuario;
    private final BuscadorDeUsuario buscador;

    @Autowired
    public MeusDadosController(GerenciadorDeUsuario gerenciadorDeUsuario, BuscadorDeUsuario buscador) {
        this.gerenciadorDeUsuario = gerenciadorDeUsuario;
        this.buscador = buscador;
    }

    @RequestMapping(value = "/auth/meus-dados", method = RequestMethod.GET)
    public String meusDados(Model m) {
        return "usuario/meusDados";
    }

    @RequestMapping(value = "/auth/usuarioLogado/salvar", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> salvarUsuarioLogado(UsuarioLogadoForm usuarioLogadoForm) {
        try {
            gerenciadorDeUsuario.alterarDadosUsuarioLogado(usuarioLogadoForm);
            return new ResponseEntity<>(OPERACAO_REALIZADA_COM_SUCESSO, HttpStatus.OK);
        } catch (EntidadeInvalidaException e) {
            return new ResponseEntity<>(StringUtils.join(e.getMensagens(), QUEBRA_DE_LINHA_DE_MENSAGEM), HttpStatus.BAD_REQUEST);
        }

    }

    @ResponseBody
    @RequestMapping(value = "/auth/usuarioLogado/gerarChaveApi", method = RequestMethod.GET)
    public String gerarChaveApi(@ModelAttribute("usuarioLogado") Usuario usuarioLogado) {
        return usuarioLogado.sugerirChaveApi();
    }


    @ApiOperation(value = "Permissões do Usuário", tags = Constantes.SWAGGER_TAG_ENDPOINTS)
    @RequestMapping(value = "/api/auth/minhas-permissoes", method = RequestMethod.GET)
    @ResponseBody
    public Map<String,List<String>> permissoesDoUsuario() {
        List<Permissao> permissoes = buscador.getUsuarioLogado().getPermissoes();
        return permissoes.stream()
                .collect(groupingBy(Permissao::getNomeSistema, mapping(Permissao::getNome, toList())));
    }
}