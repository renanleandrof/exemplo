/*
 * Essa controler é usada apenas para os testes de integração.
 * Só devem ser acessíveis se o spring profile "teste" estiver ativo.
 */
package br.com.ecge.autorizador.web;

import br.com.ecge.autorizador.aplicacao.Autenticador;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping
public class LoginController {

    private final Autenticador autenticador;

    @Autowired
    public LoginController(Autenticador autenticador) {
        this.autenticador = autenticador;
    }

    @RequestMapping(value="/login", method= RequestMethod.GET)
    public String exibirTelaLogin(Model m, HttpSession session) {
        Exception exception = (Exception) session.getAttribute("SPRING_SECURITY_LAST_EXCEPTION");
        if (exception != null) {
            m.addAttribute("alerta", "Usuário ou senha inválidos");
            session.removeAttribute("SPRING_SECURITY_LAST_EXCEPTION");
        }
        m.addAttribute("simulacao", true);
        return "index/index";
    }

//    @RequestMapping(value="/simular", method= RequestMethod.POST)
//    public String realizarLogin(@RequestParam String username, HttpSession session) {
//        Authentication auth = autenticador.authenticate(new UsernamePasswordAuthenticationToken(username, ""));
//        SecurityContextHolder.getContext().setAuthentication(auth);
//        session.setAttribute("principal", username);
//        return "redirect:/auth";
//    }

}