package br.com.ecge.autorizador.web;

import br.com.ecge.autorizador.aplicacao.BuscadorDeUsuario;
import br.com.ecge.autorizador.negocio.Usuario;
import br.com.ecge.autorizador.negocio.UsuarioSemPerfilException;
import infraestrutura.persistencia.jpa.EntidadeNaoEncontradaException;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.orm.jpa.JpaSystemException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.NestedServletException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Controller
@org.springframework.web.bind.annotation.ControllerAdvice
@Order(value = Ordered.HIGHEST_PRECEDENCE)
public class ControllerAdvice {
    private static final String ERROR_INDEX = "error/index";

    private final BuscadorDeUsuario buscadorDeUsuario;

    private final Logger logger = LoggerFactory.getLogger(ControllerAdvice.class);

    @Autowired
    public ControllerAdvice(BuscadorDeUsuario buscadorDeUsuario) {this.buscadorDeUsuario = buscadorDeUsuario;}

    @ModelAttribute(value = "usuarioLogado", binding = false, name = "usuarioLogado")
    public Usuario getUsuarioLogado() {
        return buscadorDeUsuario.getUsuarioLogado();
    }

    @ExceptionHandler(EntidadeNaoEncontradaException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String naoEncontrada() {
        return "error/404";
    }

    @ExceptionHandler({NestedServletException.class, IllegalStateException.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public String conexaoEncerrada() {
        return ERROR_INDEX;
    }

    @ExceptionHandler({HttpRequestMethodNotSupportedException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String methodNotSupported() {
        return "error/400";
    }

    @ExceptionHandler({UsuarioSemPerfilException.class, AuthenticationException.class, AccessDeniedException.class})
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public String semPermissao() {
        return "error/403";
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public String erroDeParseDeJSON(HttpMessageNotReadableException e) {
        return e.getMessage();
    }

    @ExceptionHandler(JpaSystemException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public String erroDeIntegridadeReferencial(JpaSystemException e) {
        if (isUniqueConstraintError(e)) {
            Pattern pattern = Pattern.compile("dbo.(\\S+)");
            Matcher matcher = pattern.matcher(e.getMostSpecificCause().getMessage());
            String elemento = "um Elemento";
            if (matcher.find()) {
                elemento = matcher.group(matcher.groupCount());
            }

            return "Já existe " + elemento + " com esse identificador/código cadastrado! Escolha outro.";
        }
        if (e.getCause().getCause() instanceof ConstraintViolationException) {
            return "Não é possível excluir um elemento já associado a outra entidade no sistema.";
        }
        logger.error(e.getMessage(), e);
        return e.getMessage();
    }

    private boolean isUniqueConstraintError(JpaSystemException e) {
        String message = e.getMostSpecificCause().getMessage();
        return message.contains("Unique index") || message.contains("UNIQUE KEY");
    }


    private String erro500comUniqueId(Model m, Throwable e) {
        String uniqueID = UUID.randomUUID().toString().split("-")[4];
        m.addAttribute("uniqueID", uniqueID);
        if (SecurityContextHolder.getContext().getAuthentication().isAuthenticated()) {
            String usuario = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
            logger.error(uniqueID + " {" + usuario + "}", e);
        } else {
            logger.error(uniqueID, e);
        }
        return ERROR_INDEX;
    }

    @ExceptionHandler({IOException.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public String erroDeIO(Model m, Throwable e) {
        if (e.getMessage().contains("Broken pipe") || e.getMessage().contains("Connection reset by peer")) {
            return ERROR_INDEX;
        }
        return erro500comUniqueId(m, e);
    }

    @RequestMapping(value = "/erro/404", method = {RequestMethod.GET, RequestMethod.POST})
    public String erro404() {
        return "error/404";
    }

    @RequestMapping(value = "/erro/403", method = {RequestMethod.GET, RequestMethod.POST})
    public String erro403() {
        return "error/403";
    }

    @RequestMapping(value = "/erro/400", method = {RequestMethod.GET, RequestMethod.POST})
    public String erro400() {
        return "error/400";
    }

    @RequestMapping(value = "/erro/500", method = {RequestMethod.GET, RequestMethod.POST})
    public String erro500() {
        return "error/5xx";
    }

    @ExceptionHandler(Throwable.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ModelAndView internalServerError(Throwable exception) {
        ModelAndView mav = new ModelAndView("error/index");
        List<String> mensagens = new ArrayList<>();

        String uniqueID = UUID.randomUUID().toString().split("-")[4];
        mav.addObject("uniqueID", uniqueID);

        if (SecurityContextHolder.getContext().getAuthentication().isAuthenticated()) {
            String usuario = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
            logger.error(uniqueID + " {" + usuario + "}", exception);
        } else {
            logger.error(uniqueID, exception);
        }
        mensagens.add("Aconteceu um erro no nosso servidor!");
        mav.addObject("alertas", mensagens);
        return mav;
    }

    @ExceptionHandler({IllegalArgumentException.class, BindException.class, MethodArgumentTypeMismatchException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ModelAndView badRequest(Throwable exception) {
        ModelAndView mav = new ModelAndView("error/400");
        logger.warn("400 - " + exception.getMessage());
        return mav;
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
    }

}