package br.com.ecge.autorizador.web;

import br.com.ecge.autorizador.Constantes;
import br.com.ecge.autorizador.aplicacao.BuscadorDePermissao;
import br.com.ecge.autorizador.aplicacao.PermissaoDTO;
import br.com.ecge.autorizador.aplicacao.PermissaoFiltro;
import infraestrutura.DataTablesResponseFactory;
import infraestrutura.datatables.DataTablesResponse;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping
public class BuscarPermissaoController {
    private final BuscadorDePermissao buscador;

    @Autowired
    public BuscarPermissaoController(BuscadorDePermissao buscador) {this.buscador = buscador;}

    @RequestMapping(value = "/auth/permissoes", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('CONSULTAR_PERMISSOES')")
    public String listar(PermissaoFiltro filtro, Model m) {
        m.addAttribute("filtro", filtro);
        return "permissao/permissoes";
    }

    @ApiOperation(value = "Consulta de Permissões", tags = Constantes.SWAGGER_TAG_AUTORIZADOR)
    @RequestMapping(value = "/api/auth/permissoes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasAuthority('CONSULTAR_PERMISSOES')")
    @ResponseBody
    public DataTablesResponse<PermissaoDTO> grid(PermissaoFiltro filtro) {
        return DataTablesResponseFactory.build(buscador.buscar(filtro));
    }
}