package br.com.ecge.autorizador.web;

import br.com.ecge.autorizador.Constantes;
import br.com.ecge.autorizador.aplicacao.BuscadorDeSistema;
import br.com.ecge.autorizador.aplicacao.SistemaDTO;
import infraestrutura.DataTablesResponseFactory;
import infraestrutura.datatables.DataTablesResponse;
import infraestrutura.persistencia.querybuilder.RespostaConsulta;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping
public class BuscarSistemaController {

    private final BuscadorDeSistema buscador;

    @Autowired
    public BuscarSistemaController(BuscadorDeSistema buscador) {this.buscador = buscador;}

    @RequestMapping(value = "/auth/sistemas", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('CONSULTAR_SISTEMAS')")
    public String listar(SistemaFiltro filtro, Model m) {
        m.addAttribute("filtro", filtro);
        return "sistema/sistemas";
    }

    @ApiOperation(value = "Consulta de Sistemas", tags = Constantes.SWAGGER_TAG_AUTORIZADOR)
    @RequestMapping(value = "/api/auth/sistemas", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasAuthority('CONSULTAR_SISTEMAS')")
    @ResponseBody
    public DataTablesResponse<SistemaDTO> grid(SistemaFiltro filtro) {
        RespostaConsulta<SistemaDTO> resposta = buscador.buscar(filtro);
        return DataTablesResponseFactory.build(resposta);
    }

}