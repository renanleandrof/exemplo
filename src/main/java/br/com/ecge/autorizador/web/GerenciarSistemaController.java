package br.com.ecge.autorizador.web;

import br.com.ecge.autorizador.aplicacao.BuscadorDeSistema;
import br.com.ecge.autorizador.aplicacao.GerenciadorDeSistema;
import br.com.ecge.autorizador.negocio.Sistema;
import infraestrutura.persistencia.jpa.EntidadeInvalidaException;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import static br.com.ecge.autorizador.Constantes.*;

@Controller
@RequestMapping
public class GerenciarSistemaController {

    private final GerenciadorDeSistema gerenciador;
    private final BuscadorDeSistema buscador;

    @Autowired
    public GerenciarSistemaController(GerenciadorDeSistema gerenciador, BuscadorDeSistema buscador) {
        this.gerenciador = gerenciador;
        this.buscador = buscador;
    }

    @PreAuthorize("hasAuthority('GERENCIAR_SISTEMAS')")
    @RequestMapping(value="/auth/sistemas/novo", method= RequestMethod.GET)
    public String exibirTelaCadastro(Model m) {
        return viewCadastrar(m, new Sistema());
    }

    @PreAuthorize("hasAuthority('GERENCIAR_SISTEMAS')")
    @RequestMapping(value="/auth/sistemas/{id}", method= RequestMethod.GET)
    public String exibirTelaEdicao(Model m, @PathVariable Integer id) {
        return viewCadastrar(m, buscador.getParaEdicao(id));
    }

    @PreAuthorize("hasAuthority('GERENCIAR_SISTEMAS')")
    @ApiOperation(value = "Salvar Sistema", notes = "Salvar um sistema novo ou editar um existente.", tags = SWAGGER_TAG_AUTORIZADOR)
    @RequestMapping(value="/api/auth/sistemas/salvar", method= RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<String> salvar(@RequestBody Sistema form) {
        try {
            Sistema sistema = gerenciador.salvar(form);
            return new ResponseEntity<>(sistema.getId().toString(), HttpStatus.OK);
        } catch (EntidadeInvalidaException e) {
            return new ResponseEntity<>(StringUtils.join(e.getMensagens(), QUEBRA_DE_LINHA_DE_MENSAGEM), HttpStatus.BAD_REQUEST);
        }
    }

    @PreAuthorize("hasAuthority('GERENCIAR_SISTEMAS')")
    @RequestMapping(value = {"/auth/sistemas/alternar-ativacao/{id}", "/auth/sistemas/ativar/{id}", "/auth/sistemas/inativar/{id}"}, method = RequestMethod.POST)
    public ResponseEntity<String> alterarAtivacao(@PathVariable Integer id) {
        gerenciador.alternarAtivacao(id);
        return new ResponseEntity<>(OPERACAO_REALIZADA_COM_SUCESSO, HttpStatus.OK);
    }

    private String viewCadastrar(Model m, Sistema sistema) {
        m.addAttribute("sistema", sistema);
        return "sistema/sistema";
    }

}