package br.com.ecge.autorizador;

import br.com.ecge.autorizador.web.APIAuthenticationFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityScheme;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@Configuration
@EnableSwagger2
@Profile(value = "swagger")
public class SwaggerConfig implements WebMvcConfigurer {

    @Bean
    public Docket docket() {
        return new Docket(springfox.documentation.spi.DocumentationType.SWAGGER_2)
                                .select()
                                .apis(RequestHandlerSelectors.basePackage("br.com.ecge"))
                                .paths(PathSelectors.ant("/api/**"))
                                .build()
                                .securitySchemes(Collections.singletonList(apiKey()))
                                .apiInfo(apiInfo());
    }

    @Bean
    public SecurityScheme apiKey() {
        return new ApiKey(APIAuthenticationFilter.HEADER_CHAVE_API, APIAuthenticationFilter.HEADER_CHAVE_API, "header");
    }

    private ApiInfo apiInfo() {
        Contact contato = new Contact("ECGE",
                                        "http://ecge.com.br",
                                        "contato@ecge.com.br");
        return new ApiInfo(
                "API REST",
                "API REST",
                "1.0",
                "",
                contato,
                "",
                "",
                Collections.emptyList()
        );
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/swagger-ui.html").setViewName("forward:/static/api.html");
    }

    @Override
    public void addResourceHandlers(final ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
    }
}