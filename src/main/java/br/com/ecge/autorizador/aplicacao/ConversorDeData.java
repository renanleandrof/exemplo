package br.com.ecge.autorizador.aplicacao;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import infraestrutura.DateUtils;

import java.io.IOException;
import java.time.LocalDate;
import java.util.regex.Pattern;

public class ConversorDeData extends StdDeserializer<LocalDate> {
    public ConversorDeData() {
        this(null);
    }

    private ConversorDeData(Class<?> vc) {
        super(vc);
    }

    @Override
    public LocalDate deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        String strData = jsonParser.getText();
        if(strData != null && !strData.isEmpty()) {
            String regexPadraoBR = "^(3[01]|[12][0-9]|0[1-9])/(1[0-2]|0[1-9])/[0-9]{4}$";
            String regexPadraoUS = "^[0-9]{4}-(1[0-2]|0[1-9])-(3[01]|[12][0-9]|0[1-9])$";

            if (Pattern.matches(regexPadraoBR, strData)) return DateUtils.parseLocalDate(strData, DateUtils.DDMMYYYY_FORMATTER);
            if (Pattern.matches(regexPadraoUS, strData)) return LocalDate.parse(strData, DateUtils.YYYYMMDD_FORMATTER);

            throw new ConversaoDeDataException("Erro ao converter data { " + jsonParser.getCurrentName() + ": "
                    + strData + " }. Utilize os formatos dd/MM/yyyy ou yyyy-MM-dd.");
        }
        return null;
    }
}