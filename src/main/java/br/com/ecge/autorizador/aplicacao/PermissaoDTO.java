package br.com.ecge.autorizador.aplicacao;

public class PermissaoDTO {

    private final Integer id;
    private final String nome;
    private final String sistema;
    private final Boolean ativo;

    public PermissaoDTO(Integer id, String nome, String sistema, Boolean ativo) {
        this.id = id;
        this.nome = nome;
        this.sistema = sistema;
        this.ativo = ativo;
    }

    public Integer getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public String getSistema() {
        return sistema;
    }
}
