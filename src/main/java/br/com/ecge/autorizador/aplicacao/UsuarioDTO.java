package br.com.ecge.autorizador.aplicacao;

import com.querydsl.core.annotations.QueryProjection;

public class UsuarioDTO {

    private Integer id;
    private String nome;
    private String login;
    private String email;
    private boolean ativo;

    @QueryProjection
    public UsuarioDTO(Integer id, String nome, String login, String email) {
        this.id = id;
        this.nome = nome;
        this.login = login;
        this.email = email;
    }

	@QueryProjection
	public UsuarioDTO(Integer id, String nome, String login, String email, boolean ativo) {
		this.id = id;
		this.nome = nome;
		this.login = login;
		this.email = email;
		this.ativo = ativo;
	}

	public Integer getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public String getLogin() {
		return login;
	}

	public String getEmail() {
		return email;
	}

	public boolean isAtivo() {
		return ativo;
	}
}