package br.com.ecge.autorizador.aplicacao;

import br.com.ecge.autorizador.negocio.Usuario;
import br.com.ecge.autorizador.negocio.UsuarioInativoException;
import br.com.ecge.autorizador.negocio.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class Autenticador implements UserDetailsService {

    private final UsuarioRepository usuarios;

    @Autowired
    public Autenticador(UsuarioRepository usuarios) {
        this.usuarios = usuarios;
    }

    @Transactional
    public Authentication authenticate(Authentication authentication) {
        Usuario usuario = usuarios.getPorLogin(authentication.getName());
        if (usuario == null) {
            throw new CredenciaisInvalidasException();
        }

        if (!usuario.isAtivo()) {
            throw new UsuarioInativoException();
        }

        configurarUsuarioAutenticado(usuario);
        return new UsernamePasswordAuthenticationToken(usuario, "", usuario.getAuthorities());
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Usuario usuario = usuarios.getPorLogin(s);
        if (usuario == null) {
            throw new UsernameNotFoundException("Usuário inválido.");
        }

        return usuario;
    }

    public void recarregarInformacoesDoUsuarioAutenticado(Usuario usuario) {
        if (SecurityContextHolder.getContext().getAuthentication().isAuthenticated()) {
            SecurityContextHolder.getContext().setAuthentication(configurarUsuarioAutenticado(usuario));
        }
    }

    private UsernamePasswordAuthenticationToken configurarUsuarioAutenticado(Usuario usuario) {
        return new UsernamePasswordAuthenticationToken(usuario, "", usuario.getAuthorities());
    }
}
