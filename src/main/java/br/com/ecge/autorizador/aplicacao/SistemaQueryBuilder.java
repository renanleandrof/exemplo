package br.com.ecge.autorizador.aplicacao;

import br.com.autorizador.sqlentities.SSistema;
import br.com.ecge.autorizador.web.SistemaFiltro;
import com.querydsl.core.types.Expression;
import com.querydsl.core.types.Projections;
import com.querydsl.jpa.sql.JPASQLQuery;
import infraestrutura.persistencia.querybuilder.QueryBuilderJPASQL;
import org.springframework.stereotype.Service;

@Service
public class SistemaQueryBuilder extends QueryBuilderJPASQL<SistemaFiltro, SistemaDTO> {
    private static final SSistema sistema = SSistema.Sistema;

    @Override
    public JPASQLQuery<SistemaDTO> gerarQuery(SistemaFiltro filtro) {
        JPASQLQuery<SistemaDTO> query = new JPASQLQuery<>(entityManager, sqlTemplate);

        query.select(Projections.constructor(SistemaDTO.class,
                sistema.idSistema,
                sistema.nomSistema,
                sistema.flgAtivo
        ));

        query.from(sistema);

        filtrarSePreenchido(query, filtro.getNome(), () -> sistema.nomSistema.contains(filtro.getNome()));
        filtrarSePreenchido(query, filtro.getAtivo(), () -> sistema.flgAtivo.eq(filtro.getAtivo()));

        return query;
    }

    @Override
    public Expression<? extends Comparable> getOrderByExpression(String coluna) {
        switch (coluna) {
            case "nome" : return sistema.nomSistema;
            default : return sistema.idSistema;
        }
    }
}