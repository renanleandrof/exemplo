package br.com.ecge.autorizador.aplicacao;

import br.com.ecge.autorizador.negocio.Sistema;
import br.com.ecge.autorizador.negocio.SistemaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class GerenciadorDeSistema {

    private final SistemaRepository repository;

    @Autowired
    public GerenciadorDeSistema(SistemaRepository repository) {
        this.repository = repository;
    }

    @Transactional
    public void alternarAtivacao(Integer id) {
        Sistema sistema = repository.get(id);
        sistema.setAtivo(!sistema.isAtivo());
        repository.put(sistema);
    }

    @Transactional
    public Sistema salvar(Sistema sistema) {
        return repository.put(sistema);
    }


}
