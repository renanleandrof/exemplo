package br.com.ecge.autorizador.aplicacao;

import br.com.autorizador.sqlentities.SPermissao;
import br.com.autorizador.sqlentities.SSistema;
import com.querydsl.core.types.Expression;
import com.querydsl.core.types.Projections;
import com.querydsl.jpa.sql.JPASQLQuery;
import infraestrutura.persistencia.querybuilder.QueryBuilderJPASQL;
import org.springframework.stereotype.Service;

@Service
public class PermissaoQueryBuilder extends QueryBuilderJPASQL<PermissaoFiltro, PermissaoDTO> {

    private SPermissao permissao = SPermissao.Permissao;
    private SSistema sistema = SSistema.Sistema;

    @Override
    public JPASQLQuery<PermissaoDTO> gerarQuery(PermissaoFiltro filtro) {
        JPASQLQuery<PermissaoDTO> query = new JPASQLQuery<>(entityManager, sqlTemplate);

        query.select(Projections.constructor(PermissaoDTO.class,
                permissao.idPermissao,
                permissao.descPermissao,
                sistema.nomSistema,
                permissao.flgAtivo
        ));

        query.from(permissao);
        query.innerJoin(sistema).on(permissao.idSistema.eq(sistema.idSistema));

        filtrarSePreenchido(query, filtro.getNome(), () -> permissao.descPermissao.contains(filtro.getNome()));
        filtrarSePreenchido(query, filtro.getSistema(), () -> permissao.idSistema.eq(filtro.getSistema()));
        filtrarSePreenchido(query, filtro.getAtivo(), () -> permissao.flgAtivo.eq(filtro.getAtivo()));

        return query;
    }

    @Override
    public Expression<? extends Comparable> getOrderByExpression(String coluna) {
        switch (coluna){
            case "nome": return permissao.descPermissao;
            case "sistema": return sistema.nomSistema;
            default: return permissao.idPermissao;
        }
    }
}
