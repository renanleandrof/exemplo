package br.com.ecge.autorizador.aplicacao;

import br.com.ecge.autorizador.negocio.PermissaoRepository;
import br.com.ecge.autorizador.negocio.Usuario;
import br.com.ecge.autorizador.negocio.UsuarioRepository;
import infraestrutura.PasswordUtils;
import infraestrutura.persistencia.jpa.EntidadeInvalidaException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;

@Service
public class GerenciadorDeUsuario {

    private final UsuarioRepository usuarioRepository;
    private final PermissaoRepository permissaoRepository;
    private final BuscadorDeUsuario buscadorDeUsuario;
    private BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

    @Autowired
    public GerenciadorDeUsuario(UsuarioRepository usuarioRepository, PermissaoRepository permissaoRepository, BuscadorDeUsuario buscadorDeUsuario) {
        this.usuarioRepository = usuarioRepository;
        this.permissaoRepository = permissaoRepository;
        this.buscadorDeUsuario = buscadorDeUsuario;
    }

    @Transactional
    public void alternarAtivacao(Integer id) {
        Usuario usuario = usuarioRepository.get(id);
        usuario.setAtivo(!usuario.isAtivo());
        usuarioRepository.put(usuario);
    }

    @Transactional
    public void alterarDadosUsuarioLogado(UsuarioLogadoForm form) {
        Usuario usuarioLogado = buscadorDeUsuario.getUsuarioLogado();

        usuarioLogado.setEmail(form.getEmailUsuarioLogado());
        if (isNotEmpty(form.getChaveApiUsuarioLogado())) {
            usuarioLogado.setChaveApi(PasswordUtils.hashMD5(form.getChaveApiUsuarioLogado()));
        }

        if (isNotEmpty(form.getNovaSenha())) {
            if (!BCrypt.checkpw(form.getSenhaAtual(), usuarioLogado.getSenha())) {
                throw new EntidadeInvalidaException("Senha atual inválida.");
            }

            usuarioLogado.setSenha(bCryptPasswordEncoder.encode(form.getNovaSenha()));
        }

        usuarioRepository.put(usuarioLogado);
    }

    @Transactional
    public Usuario salvar(UsuarioForm form) {
        Usuario usuario;
        Usuario usuarioDoLogin = usuarioRepository.getPorLogin(form.getLogin());

        if (form.getId() == null) {
            if (usuarioDoLogin != null) {
                throw new EntidadeInvalidaException("Login de Usuário já cadastrado.");
            }
            usuario = new Usuario();
            usuario.setSenha(bCryptPasswordEncoder.encode(PasswordUtils.gerarSenhaRandomica()));
            usuario.setAtivo(true);
        } else {
            if(usuarioDoLogin != null && !usuarioDoLogin.getId().equals(form.getId())) {
                throw new EntidadeInvalidaException("Login de Usuário já cadastrado.");
            }
            usuario = buscadorDeUsuario.getParaEdicao(form.getId());
        }

        usuario.setNome(form.getNome());
        usuario.setLogin(form.getLogin());
        usuario.setEmail(form.getEmail());

        definirPermissoes(usuario, form);

        return usuarioRepository.put(usuario);
    }

    private void definirPermissoes(Usuario usuario, UsuarioForm usuarioForm) {
        usuario.getPermissoes().clear();
        usuario.getPermissoes().addAll(permissaoRepository.getPorIds(usuarioForm.getPermissoes()));
    }

    @Transactional
    public String resetarSenha(Integer id) {
        Usuario usuario = usuarioRepository.get(id);
        String senhaRandomica = PasswordUtils.gerarSenhaRandomica();
        usuario.setSenha(bCryptPasswordEncoder.encode(senhaRandomica));
        usuarioRepository.put(usuario);
        return senhaRandomica;
    }
}
