package br.com.ecge.autorizador.aplicacao;

public class UsuarioLogadoForm {
    private String emailUsuarioLogado;
    private String chaveApiUsuarioLogado;

    private String senhaAtual;
    private String novaSenha;

    public String getEmailUsuarioLogado() {
        return emailUsuarioLogado;
    }

    public void setEmailUsuarioLogado(String emailUsuarioLogado) {
        this.emailUsuarioLogado = emailUsuarioLogado;
    }

    public String getChaveApiUsuarioLogado() {
        return chaveApiUsuarioLogado;
    }

    public void setChaveApiUsuarioLogado(String chaveApiUsuarioLogado) {
        this.chaveApiUsuarioLogado = chaveApiUsuarioLogado;
    }

    public String getSenhaAtual() {
        return senhaAtual;
    }

    public void setSenhaAtual(String senhaAtual) {
        this.senhaAtual = senhaAtual;
    }

    public String getNovaSenha() {
        return novaSenha;
    }

    public void setNovaSenha(String novaSenha) {
        this.novaSenha = novaSenha;
    }
}
