package br.com.ecge.autorizador.aplicacao;


import infraestrutura.persistencia.querybuilder.Filtro;

public class PermissaoFiltro extends Filtro {
    private String nome;
    private Integer sistema;
    private Boolean ativo;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getSistema() {
        return sistema;
    }

    public void setSistema(Integer sistema) {
        this.sistema = sistema;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }
}
