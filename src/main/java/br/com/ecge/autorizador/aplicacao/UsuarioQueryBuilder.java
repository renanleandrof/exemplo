package br.com.ecge.autorizador.aplicacao;

import br.com.autorizador.sqlentities.SPermissoesDoUsuario;
import br.com.autorizador.sqlentities.SUsuario;
import com.querydsl.core.types.Expression;
import com.querydsl.jpa.sql.JPASQLQuery;
import infraestrutura.persistencia.querybuilder.QueryBuilderJPASQL;
import org.springframework.stereotype.Service;

@Service
public class UsuarioQueryBuilder extends QueryBuilderJPASQL<UsuarioFiltro, UsuarioDTO> {
    private static final SUsuario usuario = SUsuario.Usuario;
    private static final SPermissoesDoUsuario permissoesDoUsuario = SPermissoesDoUsuario.PermissoesDoUsuario;

    @Override
    public JPASQLQuery<UsuarioDTO> gerarQuery(UsuarioFiltro filtro) {
        JPASQLQuery<UsuarioDTO> query = new JPASQLQuery<>(entityManager, sqlTemplate);

        query.select(new QUsuarioDTO(usuario.idUsuario,
        		usuario.nomUsuario,
        		usuario.descLogin,
        		usuario.emlUsuario,
        		usuario.flgAtivo
        ));

        query.from(usuario);

        filtrarSePreenchido(query, filtro.getId(), () -> usuario.idUsuario.eq(filtro.getId()));
        filtrarSePreenchido(query, filtro.getNome(), () -> usuario.nomUsuario.contains(filtro.getNome()));
        filtrarSePreenchido(query, filtro.getLogin(), () -> usuario.descLogin.contains(filtro.getLogin()));
        filtrarSePreenchido(query, filtro.getAtivo(), () -> usuario.flgAtivo.eq(filtro.getAtivo()));
        filtrarSeNaoForVazio(query, filtro.getPermissoes(), permissoesDoUsuario.idPermissao);

        adicionarJoinSeNecessario(query, permissoesDoUsuario, usuario.idUsuario.eq(permissoesDoUsuario.idUsuario));

        return query;
    }

    @Override
    public Expression<? extends Comparable> getOrderByExpression(String coluna) {
        switch (coluna) {
            case "nome" : return usuario.nomUsuario;
            case "login" : return usuario.descLogin;
            default : return usuario.idUsuario;
        }
    }
}