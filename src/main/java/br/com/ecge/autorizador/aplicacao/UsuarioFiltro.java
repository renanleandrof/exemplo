package br.com.ecge.autorizador.aplicacao;

import infraestrutura.persistencia.querybuilder.Filtro;
import io.swagger.annotations.ApiParam;

import java.util.List;

public class UsuarioFiltro extends Filtro {
    private Integer id;
	private String nome;
    private String login;
    private Boolean ativo;
	@ApiParam(value = "Lista de ID's das permissões para filtrar")
    private List<Integer> permissoes;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

    public List<Integer> getPermissoes() {
		return permissoes;
	}

	public void setPermissoes(List<Integer> permissoes) {
		this.permissoes = permissoes;
	}
}
