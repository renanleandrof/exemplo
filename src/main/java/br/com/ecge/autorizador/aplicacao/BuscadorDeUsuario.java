package br.com.ecge.autorizador.aplicacao;

import br.com.ecge.autorizador.negocio.Permissao;
import br.com.ecge.autorizador.negocio.Usuario;
import br.com.ecge.autorizador.negocio.UsuarioRepository;
import infraestrutura.PasswordUtils;
import infraestrutura.persistencia.querybuilder.RespostaConsulta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class BuscadorDeUsuario {

    private final UsuarioQueryBuilder queryBuilder;
    private final UsuarioRepository repository;

    @Autowired
    public BuscadorDeUsuario(UsuarioQueryBuilder queryBuilder, UsuarioRepository repository) {
        this.queryBuilder = queryBuilder;
        this.repository = repository;
    }

    public RespostaConsulta<UsuarioDTO> buscar(UsuarioFiltro filtro) {
        return queryBuilder.build(filtro);
    }

    public Usuario get(Integer id) {
        return repository.get(id);
    }

    public Usuario getParaEdicao(Integer id) {
        return repository.getEagerLoaded(id);
    }

    public Usuario getUsuarioLogado() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.isAuthenticated()) {
            Object principal = authentication.getPrincipal();
            if (principal instanceof Usuario) {
                return (Usuario) principal;
            }
        }
        return null;
    }

    public Usuario getUsuarioPorChaveApi(String chaveApi) {
        return repository.getPorChaveApi(PasswordUtils.hashMD5(chaveApi));
    }

    public Usuario buscarPorLogin(String login) {
        return repository.getPorLogin(login);
    }

    public List<Usuario> getPorIds(List<Integer> ids) {
        return repository.getPorIds(ids);
    }


    public List<Permissao> getPermissoesDoUsuario(Integer idUsuario) {
        return repository.get(idUsuario).getPermissoes().stream()
                .filter(p -> p.isAtivo() && p.getSistema().isAtivo())
                .collect(Collectors.toList());
    }

    public List<Permissao> getPermissoesDoUsuarioPorNomeSistema(Integer idUsuario, String nomeSistema) {
        return repository.get(idUsuario).getPermissoes().stream()
                .filter(p -> p.isAtivo()
                        && p.getSistema().isAtivo()
                        && p.getSistema().getNome().equals(nomeSistema))
                .collect(Collectors.toList());
    }

    public List<Permissao> getPermissoesDoUsuarioPorIdSistema(Integer idUsuario, Integer idSistema) {
        return repository.get(idUsuario).getPermissoes().stream()
                .filter(p -> p.isAtivo()
                        && p.getSistema().isAtivo()
                        && p.getSistema().getId().equals(idSistema))
                .collect(Collectors.toList());
    }
}
