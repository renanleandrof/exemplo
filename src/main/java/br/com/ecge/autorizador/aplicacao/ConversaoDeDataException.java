package br.com.ecge.autorizador.aplicacao;

public class ConversaoDeDataException extends RuntimeException {
    public ConversaoDeDataException(String mensagem) {
        super(mensagem);
    }
}