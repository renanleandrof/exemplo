package br.com.ecge.autorizador.aplicacao;

import br.com.ecge.autorizador.negocio.Sistema;
import br.com.ecge.autorizador.negocio.SistemaRepository;
import br.com.ecge.autorizador.web.SistemaFiltro;
import infraestrutura.persistencia.querybuilder.RespostaConsulta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BuscadorDeSistema {

    private final SistemaQueryBuilder queryBuilder;
    private final SistemaRepository repository;

    @Autowired
    public BuscadorDeSistema(SistemaQueryBuilder queryBuilder, SistemaRepository repository) {
        this.queryBuilder = queryBuilder;
        this.repository = repository;
    }

    public RespostaConsulta<SistemaDTO> buscar(SistemaFiltro filtro) {
        return queryBuilder.build(filtro);
    }

    public Sistema getParaEdicao(Integer id) {
        return repository.getEagerLoaded(id);
    }

}
