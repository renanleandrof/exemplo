package br.com.ecge.autorizador.aplicacao;

import br.com.ecge.autorizador.negocio.Permissao;
import br.com.ecge.autorizador.negocio.PermissaoRepository;
import br.com.ecge.autorizador.negocio.Sistema;
import infraestrutura.persistencia.querybuilder.RespostaConsulta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class BuscadorDePermissao {
    private final PermissaoRepository repository;
    private final PermissaoQueryBuilder queryBuilder;

    @Autowired
    public BuscadorDePermissao(PermissaoRepository repository, PermissaoQueryBuilder queryBuilder) {
        this.repository = repository;
        this.queryBuilder = queryBuilder;
    }

    public Map<Sistema, List<Permissao>> recuperarTodosPorSistema() {
        return repository.getAll().stream().collect(Collectors.groupingBy(Permissao::getSistema, Collectors.toList()));
    }

    public RespostaConsulta<PermissaoDTO> buscar(PermissaoFiltro filtro) {
        return queryBuilder.build(filtro);
    }

    public Permissao getParaEdicao(Integer id) {
        return repository.getEagerLoaded(id);
    }

    public List<Permissao> getTodos() {
        return repository.getAll();
    }
}
