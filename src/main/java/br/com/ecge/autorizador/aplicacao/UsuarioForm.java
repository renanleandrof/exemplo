package br.com.ecge.autorizador.aplicacao;

import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.List;

public class UsuarioForm {

    @ApiModelProperty(value = "Informe um id caso seja edição de usuário. Não informe nada caso seja criação de novos usuários.")
    private Integer id;
    private String nome;
    private String login;
    private String email;
    @ApiModelProperty(value = "Lista de ID's das permissoes do usuário")
    private List<Integer> permissoes = new ArrayList<>();

    public UsuarioForm() {/*Construtor padrão pro spring*/}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Integer> getPermissoes() {
        return permissoes;
    }

    public void setPermissoes(List<Integer> permissoes) {
        this.permissoes = permissoes;
    }

}
