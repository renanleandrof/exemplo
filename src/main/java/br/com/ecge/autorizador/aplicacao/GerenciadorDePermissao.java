package br.com.ecge.autorizador.aplicacao;

import br.com.ecge.autorizador.negocio.Permissao;
import br.com.ecge.autorizador.negocio.PermissaoRepository;
import br.com.ecge.autorizador.negocio.SistemaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class GerenciadorDePermissao {

    private final PermissaoRepository repository;
    private final SistemaRepository sistemaRepository;

    @Autowired
    public GerenciadorDePermissao(PermissaoRepository repository, SistemaRepository sistemaRepository) {
        this.repository = repository;
        this.sistemaRepository = sistemaRepository;
    }

    @Transactional
    public void alternarAtivacao(Integer id) {
        Permissao permissao = repository.get(id);
        permissao.setAtivo(!permissao.isAtivo());
        repository.put(permissao);
    }


    @Transactional
    public Permissao salvar(Permissao permissao) {
        if (permissao.getSistema() != null && permissao.getSistema().getId() != null) {
            permissao.setSistema(sistemaRepository.get(permissao.getSistema().getId()));
        }
        return repository.put(permissao);
    }


}
