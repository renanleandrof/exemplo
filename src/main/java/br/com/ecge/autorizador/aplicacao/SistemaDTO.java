package br.com.ecge.autorizador.aplicacao;

public class SistemaDTO {

    private final Integer id;
    private final String nome;
    private final boolean ativo;

    public SistemaDTO(Integer id, String nome, boolean ativo) {
        this.id = id;
        this.nome = nome;
        this.ativo = ativo;
    }

    public Integer getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public boolean isAtivo() {
        return ativo;
    }
}
