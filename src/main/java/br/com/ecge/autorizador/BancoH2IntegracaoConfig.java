package br.com.ecge.autorizador;

import org.h2.tools.Server;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile(value = "h2integracao")
public class BancoH2IntegracaoConfig extends BancoH2Config {

    @Override
    public Server h2Server() {
        return null;
    }
}
