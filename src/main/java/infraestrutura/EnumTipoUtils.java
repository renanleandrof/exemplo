package infraestrutura;

import java.util.EnumSet;

/**
 * Classe utilitária contendo funções para facilitar o manuseio de objetos Enum e Tipo.
 *
 * @since 1.3.0
 */
public final class EnumTipoUtils {

    private EnumTipoUtils() {} //Hide do construtor

    /**
     * Converte o Id para o Enum
     *
     * @param id id a ser convertido
     * @param clazz Enum que deve ser gerado
     * @return Enum do tipo T e Tipo
     */
	public static <T extends Enum<T> & Tipo> T parse(Integer id, Class<T> clazz) {
        if (id == null) {
            return null;
        }

        if (clazz == null) {
            throw new IllegalArgumentException("Clazz não pode ser null");
        }

        for (T enumConstant : EnumSet.allOf(clazz)) {
            if (enumConstant.getId().equals(id)) {
                return enumConstant;
            }
        }

		throw new IllegalArgumentException("Id de Enum inválido: #" + id);
	}

}