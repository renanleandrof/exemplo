package infraestrutura;


import infraestrutura.datatables.DataTablesResponse;
import infraestrutura.persistencia.querybuilder.RespostaConsulta;

public final class DataTablesResponseFactory {

    private DataTablesResponseFactory() {/* Hide do construtor de casse utilitária */ }

    public static <R> DataTablesResponse<R> build(RespostaConsulta<R> resposta) {
        DataTablesResponse<R> dataTablesResponse = new DataTablesResponse<>();
        dataTablesResponse.setRecordsTotal(resposta.getTotalRegistros());
        dataTablesResponse.setRecordsFiltered(resposta.getTotalRegistros());
        dataTablesResponse.setData(resposta.getRegistros());
        return dataTablesResponse;
    }
}
