package infraestrutura;

import java.text.DateFormatSymbols;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.TextStyle;
import java.util.Locale;
import java.util.Set;

/**
 * Classe utilitária contendo uma série de funções para formatação
 * de datas e Strings nos padrões "dd/MM/yyyy" e "dd/MM/yyyy HH:mm:ss".
 *
 * @since 1.0.0
 */
public final class DateUtils {

    public static final String LANGUAGE = "pt";
    public static final String COUNTRY = "BR";
    public static final Locale PT_BR = new Locale(LANGUAGE, COUNTRY);

    private DateUtils() { /* Não devem existir instâncias desta classe! */ }

    public static final DateTimeFormatter YYYYMMDD_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    public static final DateTimeFormatter DDMMYYYY_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    public static final DateTimeFormatter DDMMMMYYYY_FORMATTER = DateTimeFormatter.ofPattern("dd 'de' MMMM 'de' yyyy", PT_BR);
    public static final DateTimeFormatter DDMMYYYY_HHMMSS_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
    public static final DateTimeFormatter DDMMMMYYYY_HHMMSS_FORMATTER = DateTimeFormatter.ofPattern("dd 'de' MMMM 'de' yyyy 'às' HH:mm:ss", PT_BR);
    private static final DateTimeFormatter MES_ANO_FORMATTER = DateTimeFormatter.ofPattern("MM/yyyy");


    /**
     * Converte para uma String no formato dd/MM/yyyy.
     *
     * @param data Data a ser convertida para String.
     * @return Data no formato dd/MM/yyyy.
     */
    public static String toString(LocalDate data) {
        if (data == null) {
            return null;
        }
        return data.format(DDMMYYYY_FORMATTER);
    }

    /**
     * Converte para uma String no formato dd de MM de yyyy.
     *
     * @param data Data a ser convertida para String.
     * @return Data no formato dd de MM de yyyy.
     */
    public static String toStringMesPorExtenso(LocalDate data) {
        if (data == null) {
            return null;
        }
        return data.format(DDMMMMYYYY_FORMATTER).toLowerCase();
    }

    /**
     * Converte para uma String no formato dd/MM/yyyy HH:mm:ss.
     *
     * @param data Data a ser convertida para String.
     * @return Data no formato dd/MM/yyyy HH:mm:ss.
     */
    public static String toString(LocalDateTime data) {
        if (data == null) {
            return null;
        }
        return data.format(DDMMYYYY_HHMMSS_FORMATTER);
    }

    /**
     * Converte para uma String no formato dd de MM de yyyy.
     *
     * @param data Data a ser convertida para String.
     * @return Data no formato dd de MM de yyyy.
     */
    public static String toStringMesPorExtenso(LocalDateTime data) {
        if (data == null) {
            return null;
        }
        return data.format(DDMMMMYYYY_HHMMSS_FORMATTER).toLowerCase();
    }

    /**
     * Converte para uma String no formato MM/yyyy.
     */
    public static String toStringMesAno(LocalDate data) {
        if (data == null) {
            return null;
        }
        return data.format(MES_ANO_FORMATTER);
    }

    /**
     * Converte a string passada para {@link LocalDate}.
     *
     * @param data Data no formato dd/MM/yyyy a ser convertida para LocalDate.
     * @return LocalDate representando a string passada como argumento.
     */
    public static LocalDate parseLocalDate(String data) {
        if (data != null && data.length() > 0) {
            try {
                return LocalDate.parse(data, DDMMYYYY_FORMATTER);
            } catch (IllegalArgumentException | DateTimeParseException ignored) {
                throw new DateConversionException("Problemas ao se realizar a conversão de datas");
            }
        }
        
        return null;
    }

    /**
     * Converte a string no formato ISO passada para {@link LocalDate}.
     *
     * @param data Data no formato yyyy-MM-dd a ser convertida para LocalDate.
     * @return LocalDate representando a string passada como argumento.
     */
    public static LocalDate parseLocalDateISO(String data) {
        if (data != null && data.length() > 0) {
            try {
                return LocalDate.parse(data, YYYYMMDD_FORMATTER);
            } catch (IllegalArgumentException | DateTimeParseException ignored) {
                throw new DateConversionException("Problemas ao se realizar a conversão de datas");
            }
        }

        return null;
    }

    /**
     * Converte a string passada para {@link LocalDate}.
     *
     * @param data Data no formato do parâmetro fomatter a ser convertida para LocalDate.
     * @param formatter
     * @return LocalDate representando a string passada como argumento.
     */
    public static LocalDate parseLocalDate(String data, DateTimeFormatter formatter) {
        if (data != null && data.length() > 0) {
            try {
                return LocalDate.parse(data, formatter);
            } catch (IllegalArgumentException | DateTimeParseException ignored) {
                throw new DateConversionException("Problemas ao se realizar a conversão de datas");
            }
        }

        return null;
    }

    /**
     * Converte a string passada para {@link LocalDateTime}.
     *
     * @param data Data no formato dd/MM/yyyy HH:mm:ss a ser convertida para LocalDateTime.
     * @return LocalDateTime representando a string passada como argumento.
     */
    public static LocalDateTime parseLocalDateTime(String data) {
        if (data != null && data.length() > 0) {
            try {
                return LocalDateTime.parse(data, DDMMYYYY_HHMMSS_FORMATTER);
            } catch (IllegalArgumentException | DateTimeParseException ignored) {
                throw new DateConversionException("Problemas ao se realizar a conversão de datas");
            }
        }
        
        return null;
    }

    /**
     * Converte a string passada para {@link LocalDateTime}.
     *
     * @param data Data no formato yyyy-MM-ddTHH:mm a ser convertida para LocalDateTime.
     * @return LocalDateTime representando a string passada como argumento.
     */
    public static LocalDateTime parseLocalDateTimeISO(String data) {
        if (data != null && data.length() > 0) {
            try {
                return LocalDateTime.parse(data, DateTimeFormatter.ISO_DATE_TIME);
            } catch (IllegalArgumentException | DateTimeParseException ignored) {
                throw new DateConversionException("Problemas ao se realizar a conversão de datas");
            }
        }

        return null;
    }

    /**
     * Retorna o texto referente ao número do mês. Exemplo: 1 - Janeiro - 12 - Dezembro
     * @param mes de 1 a 12
     * @return Mês por extenso.
     */
    public static String getTextoMes(int mes) {
        if (mes < Month.JANUARY.getValue() || mes > Month.DECEMBER.getValue()) {
            throw new IllegalArgumentException("Mes deve ser numero entre 1 e 12");
        }
        return new DateFormatSymbols(PT_BR).getMonths()[mes - 1];
    }

    public static String getTextoMes(LocalDate data) {
        if (data != null) {
            return data.getMonth().getDisplayName(TextStyle.FULL, PT_BR);
        }
        throw new IllegalArgumentException("Data não pode ser nula");
    }

    /**
     * Determina se uma data é um final de semana.
     */
    public static boolean isFinalDeSemana(LocalDate data) {
        return data.getDayOfWeek() == DayOfWeek.SATURDAY || data.getDayOfWeek() == DayOfWeek.SUNDAY;
    }

    /**
     * Calcula o total de dias úteis existente entre uma data inicial e outra final
     * @param dataIni
     * @param dataFim
     * @param feriados
     * @return Número de dias úteis
     */
    public static Long calcularDiasUteis(LocalDate dataIni, LocalDate dataFim, Set<LocalDate> feriados) {
        if (dataIni == null || dataFim == null || feriados == null) {
            throw new IllegalArgumentException("Todos os parâmetros devem ser diferentes de null.");
        }

        if (dataIni.isAfter(dataFim)) {
            throw new IllegalArgumentException("A data inicial deve ser menor ou igual a data final.");
        }

        Long diasUteis = 0L;
        LocalDate dia = dataIni;

        do {
            if (!isFinalDeSemana(dia) &&
                !feriados.contains(dia)) {
                diasUteis++;
            }

            dia = dia.plusDays(1);
        } while (!dia.isAfter(dataFim));

        return diasUteis;
    }

}