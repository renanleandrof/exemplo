package infraestrutura;

import com.google.common.base.CaseFormat;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.dsl.PathBuilder;
import com.querydsl.jpa.impl.JPAQuery;

/**
 * Classe utilitária contendo uma série de funções para facilitar o manuseio de objetos Query do QueryDSL.
 *
 * @since 1.1.2
 */
public final class JPAQueryUtils {

    public static final String ASC = "asc";

    private JPAQueryUtils() { /* Não devem existir instâncias desta classe! */ }

    /**
     * Adiciona os parametros de paginação em um JPAQuery
     *
     * @param pagina pagina a ser obtida
     * @param tamanhoPagina tamanho das paginas
     * @param query objeto onde será inserida a paginação
     */
    public static void adicionaPaginacao(int pagina, int tamanhoPagina, JPAQuery query) {
        query.limit(tamanhoPagina).offset((long) (pagina - 1) * (long) tamanhoPagina);
    }

    /**
     * Adiciona os parametros de paginação em um JPAQuery
     *
     * @param offset quantidade de registros a serem pulados
     * @param tamanhoPagina tamanho das paginas
     * @param query objeto onde será inserida a paginação
     */
    public static void adicionaPaginacao(JPAQuery query, int tamanhoPagina, int offset) {
        query.limit(tamanhoPagina).offset(offset);
    }

    /**
     * Adicionar os parametros de sort em um JPAQuery
     *
     * @param sortColumn coluna a ser ordenada
     * @param order Order.ASC ou Order.DESC
     * @param query Objeto onde será inserido o sort
     * @param classe Class da entidade que está sendo alvo do query
     */
    @SuppressWarnings("unchecked")
    public static void adicionaSort(String sortColumn, Order order, JPAQuery query, Class<?> classe) {
        PathBuilder orderByExpression = new PathBuilder(classe, CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_CAMEL, classe.getSimpleName()));
        query.orderBy(new OrderSpecifier(order, orderByExpression.get(sortColumn)));
    }

    /**
     * Adicionar os parametros de sort em um JPAQuery
     *
     * @param sortColumn coluna a ser ordenada
     * @param order string com asc ou desc
     * @param query Objeto onde será inserido o sort
     * @param classe Class da entidade que está sendo alvo do query
     */
    @SuppressWarnings("unchecked")
    public static void adicionaSort(String sortColumn, String order, JPAQuery query, Class<?> classe) {
        PathBuilder orderByExpression = new PathBuilder(classe, CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_CAMEL, classe.getSimpleName()));
        if (ASC.equals(order)) {
            query.orderBy(new OrderSpecifier(Order.ASC, orderByExpression.get(sortColumn)));
        } else {
            query.orderBy(new OrderSpecifier(Order.DESC, orderByExpression.get(sortColumn)));
        }
    }

}