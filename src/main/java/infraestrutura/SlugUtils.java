package infraestrutura;

import org.apache.commons.lang3.StringUtils;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

public final class SlugUtils {

    private static final String SEPARADOR_SLUG = "-";

    private SlugUtils() {
        /*Hide do construtor de classe utilitária*/
    }

    public static String slugify(String codigo, String nome) {
        return codigo + SEPARADOR_SLUG + slugify(nome);
    }

    public static String slugify(Integer id, String nome) {
        return id + SEPARADOR_SLUG + slugify(nome);
    }

    static String slugify(String nome) {
        if (isNotBlank(nome)) {
            return StringUtils.stripAccents(nome.trim().toLowerCase()).replaceAll("[^a-z]", SEPARADOR_SLUG);
        }
        return "";
    }

}
