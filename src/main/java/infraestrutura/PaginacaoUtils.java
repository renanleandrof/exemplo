package infraestrutura;

/**
 * Classe utilitária contendo funções para cálculos necessários em paginação.
 *
 * @since 1.0.0
 */
public final class PaginacaoUtils {

    private PaginacaoUtils() { /* Não devem existir instâncias desta classe! */ }

    public static long calculaTotalPaginas(int tamanhoPagina, long totalRegistros) {
        return (long) Math.ceil((double) totalRegistros / (double) tamanhoPagina);
    }

    public static int verificaPagina(int pagina, long totalPaginas) {
        if (pagina < 1 || pagina > totalPaginas) {
            pagina = 1;
        }
        return pagina;
    }

    public static int calculaPagina(int primeiroItem, int tamanhoPagina) {
        return (primeiroItem / tamanhoPagina) + 1;
    }
    
}