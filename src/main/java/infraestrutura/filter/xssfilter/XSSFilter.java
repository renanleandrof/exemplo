package infraestrutura.filter.xssfilter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * <p>
 * Filtro para proteger a aplicação de entradas maliciosas de XSS.<br>
 * Esse filtro envolve o request num wrapper que evita o XSS.
 * </p>
 *
 * Adicione o filtro no web.xml da aplicação:
 * <pre>{@code
 *
 * <filter>
 *     <filter-name>XSSFilter</filter-name>
 *     <filter-class>infraestrutura.filter.xssfilter.XSSFilter</filter-class>
 * </filter>
 * <filter-mapping>
 *     <filter-name>XSSFilter</filter-name>
 *     <servlet-name>Spring</servlet-name>
 * </filter-mapping>
 *
 * }</pre>
 *
 * @since 1.3.0
 */
public class XSSFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException { }

    @Override
    public void destroy() { }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        chain.doFilter(new XSSRequestWrapper((HttpServletRequest) request), response);
    }

}