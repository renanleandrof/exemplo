package infraestrutura.filter.ajax;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * <p>
 * Filtro que desabilita o cache em requisicoes Ajax que nao tiveram o cache setado.
 * </p>
 *
 * Adicione o filtro no web.xml da aplicação:
 * <pre>{@code
 *
 * <filter>
 *     <filter-name>AjaxNoCacheFilter</filter-name>
 *     <filter-class>infraestrutura.filter.ajax.AjaxNoCacheFilter</filter-class>
 * </filter>
 * <filter-mapping>
 *     <filter-name>AjaxNoCacheFilter</filter-name>
 *     <servlet-name>Spring</servlet-name>
 * </filter-mapping>
 *
 * }</pre>
 *
 * @since 1.7.0
 */
public class AjaxNoCacheFilter implements Filter {

    public static final String CABECALHO_EXPIRES = "Expires";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException { }

    @Override
    public void destroy() { }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (ehRequisicaoAjax(request) && cabecalhoExpiresNaoFoiSetadoPreviamente(response)) {
            setarCabecalhoExpiresParaCacheExpirarImediatamente(response);
        }
        chain.doFilter(request, response);
    }

    private boolean ehRequisicaoAjax(ServletRequest request) {
        String requestedWithHeader = ((HttpServletRequest) request).getHeader("X-Requested-With");
        return "XMLHttpRequest".equals(requestedWithHeader);
    }

    private boolean cabecalhoExpiresNaoFoiSetadoPreviamente(ServletResponse response) {
        return !((HttpServletResponse) response).containsHeader(CABECALHO_EXPIRES);
    }

    private void setarCabecalhoExpiresParaCacheExpirarImediatamente(ServletResponse response) {
        ((HttpServletResponse) response).setHeader(CABECALHO_EXPIRES, "-1");
    }

}