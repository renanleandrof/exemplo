package infraestrutura;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Classe que constroi um {@link java.util.Map}&lt;String, String&gt; pra ser usado nas &lt;select&gt;s das telas.
 */
public final class TipoSelectBuilder {

    public static final String OPCAO_PADRAO_TODOS = "Todos";
    public static final String OPCAO_PADRAO_SELECIONE = "(Selecione)";

    private TipoSelectBuilder() { /* Não devem existir instâncias desta classe! */ }

    public static Map<String, String> build(List<? extends Tipo> entidades, String nomeOpcaoPadrao) {
        Map<String, String> selectList = new LinkedHashMap<String, String>();

        if (nomeOpcaoPadrao != null){
        	selectList.put("", nomeOpcaoPadrao);
        }

        if (entidades != null && entidades.size() > 0) {
            gerarLista(entidades, selectList);
        }
        return selectList;
    }


    public static Map<String, String> build(List<? extends Tipo> entidades) {
        Map<String, String> selectList = new LinkedHashMap<String, String>();
        gerarLista(entidades, selectList);
        return selectList;
    }

    private static void gerarLista(List<? extends Tipo> entidades, Map<String, String> selectList) {
        for (Tipo entidade : entidades) {
            //Se for enum, usa o NAME como chave, pro Conversor do Spring funcionar na volta.
            if (entidade instanceof Enum) {
                selectList.put(((Enum) entidade).name(), entidade.getDescricao());
            } else {
                selectList.put(entidade.getId().toString(), entidade.getDescricao());
            }
        }
    }

	public static <E extends Enum<E> & Tipo> Map<String, String> gerarSelectComOpcaoPadraoTodos(Class<E> classeEnum) {
		return buildDeEnum(classeEnum, OPCAO_PADRAO_TODOS);
	}

	public static <E extends Enum<E> & Tipo> Map<String, String> gerarSelectComOpcaoPadraoSelecione(Class<E> classeEnum) {
		return buildDeEnum(classeEnum, OPCAO_PADRAO_SELECIONE);
	}

	public static <E extends Enum<E> & Tipo> Map<String, String> buildDeEnum(Class<E> classeEnum, final String nomeOpcaoPadrao) {
		return build(Arrays.asList(classeEnum.getEnumConstants()), nomeOpcaoPadrao);
	}

}