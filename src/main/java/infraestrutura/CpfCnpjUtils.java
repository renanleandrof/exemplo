package infraestrutura;

import javax.swing.text.MaskFormatter;
import java.text.ParseException;

import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.leftPad;

/**
 * Classe utilitária contendo uma série de funções para formatação
 * de CPF e CNPJ.
 *
 * @since 1.0.0
 */
public final class CpfCnpjUtils {

    public static final int TAMANHO_CNPJ = 14;
    public static final int TAMANHO_CPF = 11;

    private static final String PAD_STRING = "0";
    private static final int FATOR_SOMA_CNPJ = 11;
    private static final int MAIOR_DIGITO = 9;
    private static final int MENOR_DIGITO = 0;
    private static final int TAMANHO_CNPJ_SEM_DIGITOS = 12;
    private static final int TAMANHO_CPF_SEM_DIGITO = 9;
    private static final int[] PESOS_CPF = {11, 10, 9, 8, 7, 6, 5, 4, 3, 2};
    private static final int[] PESOS_CNPJ = {6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2};
    private static final String MASCARA_CNPJ = "##.###.###/####-##";
    private static final String MASCARA_CPF = "###.###.###-##";
    private static final String REGEX_PONTO_E_BARRA = "(\\.)|(-)|(/)";
    private static final String VAZIO = "";

    private CpfCnpjUtils() { /* Não devem existir instâncias desta classe! */ }

    public static String formatarPeloTamanho(String cnpjOuCpf) {
        if (cnpjOuCpf == null) {
            return "";
        }

        String cnpjOuCpfSemEspacos = cnpjOuCpf.trim();

        if (cnpjOuCpfSemEspacos.length() == TAMANHO_CNPJ) {
            return formatarCnpj(cnpjOuCpfSemEspacos);
        }

        if (cnpjOuCpfSemEspacos.length() == TAMANHO_CPF) {
            return formatarCpf(cnpjOuCpfSemEspacos);
        }

        return cnpjOuCpf;
    }

    public static String formatarPeloTamanhoComSigiloNoCpf(String cnpjOuCpf) {
        if (cnpjOuCpf == null) {
            return "";
        }

        String cnpjOuCpfSemEspacos = cnpjOuCpf.trim();

        if (cnpjOuCpfSemEspacos.length() == TAMANHO_CNPJ) {
            return formatarCnpj(cnpjOuCpfSemEspacos);
        }

        if (cnpjOuCpfSemEspacos.length() == TAMANHO_CPF) {
            return formatarCpfComSigilo(cnpjOuCpfSemEspacos);
        }

        return cnpjOuCpf;
    }

    public static String formatarCnpj(String cnpj) {
        String cnpjFormatado;
        try {
            cnpjFormatado = formatarStringComMascara(leftPad(cnpj, TAMANHO_CNPJ, PAD_STRING), MASCARA_CNPJ);
        } catch (ParseException ex) {
            cnpjFormatado = cnpj;
        }
        return cnpjFormatado;
    }

    public static String formatarCpf(String cpf) {
        String formatado;
        try {
            formatado = formatarStringComMascara(leftPad(cpf, TAMANHO_CPF, PAD_STRING), MASCARA_CPF);
        } catch (ParseException ex) {
            formatado = cpf;
        }
        return formatado;
    }

    public static String formatarCpfComSigilo(String cpf) {
        if (cpf == null) {
            return "";
        }
        String formatado = formatarCpf(cpf);
        if (formatado.length() < 14) { return ""; }
        return "***" + formatado.substring(3,7) +formatado.substring(7,12)+ "**";
    }

    private static String formatarStringComMascara(String texto, String mascara) throws ParseException {
        if (texto == null) {
            return null;
        }

        MaskFormatter formatter = new MaskFormatter(mascara);
        formatter.setValueContainsLiteralCharacters(false);
        return formatter.valueToString(texto);
    }

    public static String desformatarCpfCnpj(String cpfCnpj) {
        if (isEmpty(cpfCnpj)) {
            return null;
        }
        return cpfCnpj.replaceAll(REGEX_PONTO_E_BARRA, VAZIO);
    }

    private static int calcularDigito(String str, int[] peso) {
        int soma = 0;
        for (int indice = str.length() - 1, digito; indice >= 0; indice--) {
            digito = Integer.parseInt(str.substring(indice, indice + 1));
            soma += digito * peso[peso.length - str.length() + indice];
        }
        soma = FATOR_SOMA_CNPJ - soma % FATOR_SOMA_CNPJ;
        return soma > MAIOR_DIGITO ? MENOR_DIGITO : soma;
    }

    public static boolean validarCnpj(String cnpj) {
        if (isEmpty(cnpj)) {
            return true;
        }

        if (cnpj.length() != TAMANHO_CNPJ) {
            return false;
        }

        Integer digito1 = calcularDigito(cnpj.substring(0, TAMANHO_CNPJ_SEM_DIGITOS), PESOS_CNPJ);
        Integer digito2 = calcularDigito(cnpj.substring(0, TAMANHO_CNPJ_SEM_DIGITOS) + digito1, PESOS_CNPJ);

        return cnpj.equals(cnpj.substring(0, TAMANHO_CNPJ_SEM_DIGITOS) + digito1.toString() + digito2.toString());
    }

    public static boolean validarCpf(String cpf) {
        if (isEmpty(cpf)) {
            return true;
        }

        if (cpf.length() != TAMANHO_CPF) {
            return false;
        }

        Integer digito1 = calcularDigito(cpf.substring(0, TAMANHO_CPF_SEM_DIGITO), PESOS_CPF);
        Integer digito2 = calcularDigito(cpf.substring(0, TAMANHO_CPF_SEM_DIGITO) + digito1, PESOS_CPF);

        return cpf.equals(cpf.substring(0, TAMANHO_CPF_SEM_DIGITO) + digito1.toString() + digito2.toString());
    }
   

}