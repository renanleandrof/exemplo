package infraestrutura;

import ch.qos.logback.classic.html.HTMLLayout;
import ch.qos.logback.classic.spi.ILoggingEvent;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.http.HttpServletRequest;

/**
 * Essa classe é um layout para ser usado no logback.
 * Ela imprime detalhes do request no corpo do html.
 *
 * Exemplo de logback.xml
 *
 * <?xml version="1.0" encoding="UTF-8"?>
 * <configuration>
 *     <include resource="org/springframework/boot/logging/logback/base.xml" />
 *     <springProfile name="notificar-erros">
 *         <appender name="mail" class="ch.qos.logback.classic.net.SMTPAppender">
 *             <smtpHost>10.208.8.144</smtpHost>
 *             <username>app-exception</username>
 *             <password>@pp&amp;xc3pt!0n</password>
 *             <smtpPort>25</smtpPort>
 *             <asynchronousSending>true</asynchronousSending>
 *             <from>app-exception@cgu.gov.br</from>
 *             <to>cosis@cgu.gov.br</to>
 *             <subject>[@contextProvider.serverName@] %m %rEx</subject>
 *             <layout class="br.gov.cgu.eaud.infraestrutura.ExceptionHTMLLayout"/>
 *             <filter class="ch.qos.logback.classic.filter.ThresholdFilter">
 *                 <level>ERROR</level>
 *             </filter>
 *             <filter class="ch.qos.logback.core.filter.EvaluatorFilter">
 *                 <evaluator>
 *                     <expression>return  (message.contains("Cannot call getWriter()"));
 *                     </expression>
 *                 </evaluator>
 *                 <OnMismatch>NEUTRAL</OnMismatch>
 *                 <OnMatch>DENY</OnMatch>
 *             </filter>
 *         </appender>
 *
 *         <logger name="org.hibernate.engine.jdbc.spi" level="OFF"/>
 *         <logger name="org.hibernate.internal.ExceptionMapperStandardImpl" level="OFF"/>
 *         <root level="INFO">
 *             <appender-ref ref="mail" />
 *         </root>
 *     </springProfile>
 * </configuration>
 *
 * Exemplo de chamada que dispara o layout:
 *
 * ((ch.qos.logback.classic.Logger) logger).log(null,"", 40, "{} ({})", new Object[]{uniqueID, usuario, request}, exception );
 *
 */
public class ExceptionHTMLLayout extends HTMLLayout {

    @Override
    public String doLayout(ILoggingEvent event) {
        String html = super.doLayout(event);
        try {
            if (event.getArgumentArray()[2] instanceof HttpServletRequest) {
                HttpServletRequest request = (HttpServletRequest) event.getArgumentArray()[2];
                String requestDetails = "<tr><td colspan='6'><h4>Detalhes do Request:</h4>" +
                        request.getMethod() + " "
                        + request.getRequestURL().toString() + " <br/>"
                        + new ObjectMapper().writeValueAsString(request.getParameterMap()) + "<br/><br/><h4>Detalhes da Exception: </h4></td></tr>";
                return requestDetails + html;
            }
            return html + " <br/> *** Exception sem request associado";
        } catch (Exception e) {
            return html + " <br/> *** Erro ao obter detalhes do request. Exception: " + event.getMessage();
        }
    }
}