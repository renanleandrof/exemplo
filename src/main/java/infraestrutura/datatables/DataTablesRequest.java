package infraestrutura.datatables;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.List;

//https://datatables.net/
public class DataTablesRequest {

    private int draw;
    private int start;
    private int length;

    private DataTablesSearch search;
    private List<DataTablesOrder> order;
    private List<DataTablesColumn> columns;

    private static ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    private boolean paginacaoSimples;

    /** Hide do construtor - use o .parse para construir a partir de um json. */
    private DataTablesRequest() {}

    public static DataTablesRequest parse(String json) {
        try {
            return mapper.readValue(json, DataTablesRequest.class);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public int getDraw() {
        return draw;
    }

    public int getStart() {
        return start;
    }

    public int getLength() {
        return length;
    }

    public DataTablesSearch getSearch() {
        return search;
    }

    public List<DataTablesOrder> getOrder() {
        return order;
    }

    public List<DataTablesColumn> getColumns() {
        return columns;
    }

    public boolean isPaginacaoSimples() { return paginacaoSimples; }

    public void setDraw(int draw) {
        this.draw = draw;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public void setSearch(DataTablesSearch search) {
        this.search = search;
    }

    public void setOrder(List<DataTablesOrder> order) {
        this.order = order;
    }

    public void setColumns(List<DataTablesColumn> columns) {
        this.columns = columns;
    }

    public void setPaginacaoSimples(boolean paginacaoSimples) {
        this.paginacaoSimples = paginacaoSimples;
    }
}
