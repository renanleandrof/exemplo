package infraestrutura;

public class DateConversionException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public DateConversionException(String msg) {
        super(msg);
    }
}
