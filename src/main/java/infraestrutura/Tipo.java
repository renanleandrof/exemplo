package infraestrutura;

/**
 * Descreve um objeto que tem um <b>identificador</b> (ID) inteiro e uma <b>descrição</b> (string).
 *
 * @since 1.0.0
 */
public interface Tipo {

    Integer getId();

    String getDescricao();

}