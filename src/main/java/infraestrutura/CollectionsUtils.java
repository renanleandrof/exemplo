package infraestrutura;

import java.util.Collection;

/**
 * Classe utilitária para collections
 *
 * @since 1.3.2
 */
public final class CollectionsUtils {

    private CollectionsUtils() { /* Não devem existir instâncias desta classe! */ }

    /**
     * Adiciona  todos elementos de uma collection em uma outra collection.
     * Não faz nada se a collection de objetos for nula
     * @param collection collection que recebera os objetos
     * @param objetosQueSeraoAdicionados collection de objetos Que Serao Adicionados a lista
     */
    public static void addTodosComNullCheck(Collection collection, Collection objetosQueSeraoAdicionados ) {
        if (objetosQueSeraoAdicionados != null) {
            collection.addAll(objetosQueSeraoAdicionados);
        }
    }
}