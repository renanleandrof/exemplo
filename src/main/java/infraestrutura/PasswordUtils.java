package infraestrutura;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;

/**
 * Classe utilitária contendo uma série de funções para geração e verificação
 * de senhas seguras.
 *
 * @since 1.0.0
 */
public final class PasswordUtils {

    private static Logger logger = LoggerFactory.getLogger(PasswordUtils.class);

    private static final int TAMANHO_PADRAO_SENHA_RANDOMICA = 8;

    private PasswordUtils() { /* Não devem existir instâncias desta classe! */ }

    // The higher the number of iterations the more
    // expensive computing the hash is for us
    // and also for a brute force attack.
    private static final int ITERATIONS = 1024;
    private static final int SALT_LEN = 32;
    private static final int DESIRED_KEY_LEN = 256;

    /**
     * Computes a salted PBKDF2 hash of given plaintext password.<br>
     * <br>
     * Suitable for storing in a database.<br>
     * Empty passwords are not supported.<br>
     *
     * @param password Senha em texto claro.
     * @return String que representa o SALT + Senha-criptografada.
     */
    public static String getSaltedHash(String password) {
        try {
            byte[] salt = SecureRandom.getInstance("SHA1PRNG").generateSeed(SALT_LEN);
            // store the salt with the password
            return Base64.encodeBase64String(salt) + "$" + hash(password, salt);
        } catch (Exception e) {
            logger.error("Erro no GetInstance(SHA1PRNG)", e);
            throw new RuntimeException(e);
        }
    }

    /**
     * Checks whether given plaintext password corresponds
     * to a stored salted hash of the password.
     *
     * @param password Senha em texto claro.
     * @param stored String SALT + Senha-cripografada gerada previamente.
     * @return Se a senha em texto claro corresponde ao salt+senha criptografada passados.
     */
    public static boolean check(String password, String stored)  {
        try {
            String[] saltAndPass = stored.split("\\$");
            if (saltAndPass.length != 2) {
                return false;
            }

            String hashOfInput = hash(password, Base64.decodeBase64(saltAndPass[0]));
            return hashOfInput.equals(saltAndPass[1]);
        } catch (Exception e) {
            logger.error("Erro do algoritmo de Password", e);
            throw new RuntimeException(e);
        }
    }

    // using PBKDF2 from Sun, an alternative is https://github.com/wg/scrypt
    // cf. http://www.unlimitednovelty.com/2012/03/dont-use-bcrypt.html
    private static String hash(String password, byte[] salt) throws NoSuchAlgorithmException, InvalidKeySpecException  {
        if (password == null || password.length() == 0) {
            throw new IllegalArgumentException("Senha não pode ser vazia.");
        }

        SecretKeyFactory f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        SecretKey key = f.generateSecret(new PBEKeySpec(
                        password.toCharArray(), salt, ITERATIONS, DESIRED_KEY_LEN)
        );
        return Base64.encodeBase64String(key.getEncoded());
    }


    public static String gerarSenhaRandomica() {
        return RandomStringUtils.randomAlphanumeric(TAMANHO_PADRAO_SENHA_RANDOMICA);
    }


    public static String hashMD5(String senha) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(senha.getBytes(), 0, senha.length());
            
            StringBuffer sb = new StringBuffer();
            
            for(byte b : md.digest()) { 
                sb.append(String.format("%02x", b&0xff)); 
            }
            
            return sb.toString();
            
        } catch (Exception e) {
            logger.error("Erro no GetInstance(MD5)", e);
            throw new RuntimeException(e);
        }
    }

}