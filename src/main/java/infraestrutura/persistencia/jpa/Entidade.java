package infraestrutura.persistencia.jpa;

public interface Entidade<K> {
    K getId();
}