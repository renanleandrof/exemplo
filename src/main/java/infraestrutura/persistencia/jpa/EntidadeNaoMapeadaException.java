package infraestrutura.persistencia.jpa;

public class EntidadeNaoMapeadaException extends RuntimeException {
    public EntidadeNaoMapeadaException(String s, Throwable e) {
        super(s, e);
    }
}
