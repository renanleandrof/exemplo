package infraestrutura.persistencia.jpa;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @param <E> entidade JPA que implemente Entidade K
 * @param <K> tipo da chave da Entidade E
 */
public class RepositoryJpa<E extends Entidade<K>, K> extends AbstractRepositoryJPA<E, K> {

    @PersistenceContext
    protected EntityManager entityManager;

    @Override
    protected EntityManager getEntityManager() { return this.entityManager; }
}