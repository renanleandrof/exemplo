package infraestrutura.persistencia.jpa.h2;

import org.hibernate.dialect.H2Dialect;
import org.hibernate.dialect.function.SQLFunctionTemplate;
import org.hibernate.type.StandardBasicTypes;

public class PortalH2Dialect extends H2Dialect {

    public PortalH2Dialect() {
        super();
        registerFunction("always_null", new SQLFunctionTemplate(StandardBasicTypes.STRING, "cast(null as char)"));
        registerFunction("diffDataCorrenteDataInt", new SQLFunctionTemplate(StandardBasicTypes.INTEGER, "(CURRENT_DATE() - parsedatetime(?1, 'yyyyMMdd'))"));
    }
}
