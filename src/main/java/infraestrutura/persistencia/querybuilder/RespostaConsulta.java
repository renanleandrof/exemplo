package infraestrutura.persistencia.querybuilder;

import java.util.List;

public class RespostaConsulta<T> {

    private final List<T> registros;
    private final Long totalRegistros;

    public RespostaConsulta(List<T> registros, Long totalRegistros) {
        this.registros = registros;
        this.totalRegistros = totalRegistros;
    }

    public List<T> getRegistros() {
        return registros;
    }

    public Long getTotalRegistros() {
        return totalRegistros;
    }

}