package infraestrutura;

import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Classe utilitária contendo uma série de funções para formatação
 * de valores monetários.
 *
 * @since 1.0.0
 */
public final class ValorUtils {

    private ValorUtils() { /* Não devem existir instâncias desta classe! */ }

    private static final String BRASIL = "BR";
    private static final String PORTUGUES = "pt";

    private static final String SIMBOLO_MOEDA = "";
    private static final float CEM_POR_CENTO_FLOAT = 100.0f;
    private static final int CEM_POR_CENTO = 100;

    public static BigDecimal getValorMonetario(String valor) {
		try {
			return new BigDecimal(valor.replace(".", "")
                                       .replace(",", ".")
                                       .replace("R$ ",""));
		} catch (Exception ex) {
			return BigDecimal.ZERO;
		}
	}

    public static String getValorMonetarioComSimboloRealFormatado(BigDecimal valor) {
        return formatar(valor, 2,true);
    }
    
    public static String getValorMonetarioFormatado(BigDecimal valor) { return formatar(valor, 2,false); }

    public static String getValorFormatado(Long valor) { return formatar(valor, 0,false); }

    public static String getValorFormatado(Integer valor) { return formatar(valor, 0,false); }

    private static String formatar(Object valor, Integer casasDecimais,Boolean exibirSimboloReal) {
        NumberFormat format = DecimalFormat.getCurrencyInstance(new Locale(PORTUGUES, BRASIL));
        DecimalFormatSymbols decimalFormatSymbols = ((DecimalFormat) format).getDecimalFormatSymbols();
        if(!exibirSimboloReal){
            decimalFormatSymbols.setCurrencySymbol(SIMBOLO_MOEDA);
        }
        ((DecimalFormat) format).setDecimalFormatSymbols(decimalFormatSymbols);
        format.setMinimumFractionDigits(casasDecimais);

        try {
            return format.format(valor).trim();
        } catch (Exception e) {
            String resultado = exibirSimboloReal ? "R$ ":"";
            resultado += casasDecimais == 0 ? "0" : "0," + StringUtils.repeat("0", casasDecimais);
            return resultado;
        }
    }
    public static String getPercentualFormatado(BigDecimal valor, BigDecimal total) {
        if (valor == null || valor.compareTo(BigDecimal.ZERO) == 0 || total == null || total.compareTo(BigDecimal.ZERO) == 0) {
            return "0,00%";
        }
        return String.format("%.02f", valor.multiply(new BigDecimal(CEM_POR_CENTO)).divide(total, 2, RoundingMode.HALF_UP)) + "%";
    }

    public static String getPercentualFormatado(Long valor, Long total) {
        if (valor == null || valor.equals(0L) || total == null || total.equals(0L)) {
            return "0,00%";
        }
        return String.format("%.02f", (valor * CEM_POR_CENTO_FLOAT) / total) + "%";
    }

    public static String getPercentualFormatado(Integer valor, Integer total) {
        if (valor == null || valor.equals(0) || total == null || total.equals(0)) {
            return "0,00%";
        }
        return String.format("%.02f", (valor * CEM_POR_CENTO_FLOAT) / total) + "%";
    }

    public static Double getPercentual(Long valor, Long total) {
        if (valor == null || valor.equals(0L) || total == null || total.equals(0L)) {
            return 0d;
        }
        return (double) valor * CEM_POR_CENTO_FLOAT / total;
    }

    public static Double getPercentual(Integer valor, Integer total) {
        if (valor == null || valor.equals(0) || total == null || total.equals(0)) {
            return 0d;
        }
        return (double) valor * CEM_POR_CENTO_FLOAT / total;
    }

    public static boolean isNullOuZero(Object valor) {
        if( valor instanceof BigDecimal ) {
            BigDecimal bd = (BigDecimal) valor;
            return bd.signum() == 0;
        }
        return valor == null
                || valor.equals(0)
                || Long.valueOf(0) == valor
                || "0,00".equals(valor)
                || "0.00".equals(valor)
                || "R$ 0,00".equals(valor)
                || "R$ 0.00".equals(valor)
                || "0,00%".equals(valor)
                || "0.00%".equals(valor)
                || "0".equals(valor);
    }
}