package infraestrutura;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public final class TestProperties {

	private static final Log LOGGER = LogFactory.getLog(TestProperties.class);

	private TestProperties() {}

	static {
		loadPropertiesFiles();
	}

	private static void loadPropertiesFiles() {
		try {
			loadPropertiesFileFromClasspath();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void loadPropertiesFileFromClasspath() throws IOException {
		properties = new Properties();
		InputStream in = TestProperties.class.getClassLoader().getResourceAsStream("componentes-test.properties");
		if (in == null) {
            LOGGER.error("Arquivo 'componentes-test.properties' nao foi encontrado no classpath. " +
                    "Funcionalidades dos componentes de testes que requiram este arquivo não funcionarão.");
		} else {
            properties.load(in);
            in.close();
        }
	}

	private static Properties properties;

	public static String get(String property) {
		return StringUtils.defaultString(properties.getProperty(property), "");
	}

}