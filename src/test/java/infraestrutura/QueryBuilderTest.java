package infraestrutura;

import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.sql.SQLTemplates;
import infraestrutura.persistencia.jpa.SQLTemplatesFactory;
import infraestrutura.persistencia.querybuilder.Filtro;
import infraestrutura.persistencia.querybuilder.QueryBuilderJPASQL;
import org.junit.Before;
import org.springframework.beans.PropertyAccessor;
import org.springframework.beans.PropertyAccessorFactory;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

public abstract class QueryBuilderTest<F extends Filtro, DTO> {

    @Before
    public void before() {
        System.setProperty("spring.profiles.active", "teste");
        SQLTemplates template = SQLTemplatesFactory.build(SQLTemplatesFactory.Banco.SQLSERVER);
        ReflectionTestUtils.setField(getQueryBuilder(), "sqlTemplate", template);
    }

    protected void testarQuery(String colunaOrdenacao, F filtro, int qntRegistrosEsperada) {
        List<DTO> resultado = executarQuery(colunaOrdenacao, filtro);

        assertThat("O resultado não tem o tamanho esperado!", resultado, hasSize(qntRegistrosEsperada));
        assertOrdenacao(resultado, colunaOrdenacao);
    }

    protected List<DTO> executarQuery(String colunaOrdenacao, F filtro) {
        return getQueryBuilder()
                .gerarQuery(filtro)
                .orderBy(new OrderSpecifier<>(Order.DESC, getQueryBuilder().getOrderByExpression(colunaOrdenacao)))
                .fetch();
    }

    protected abstract void assertOrdenacao(List<DTO> resultado, String colunaOrdenacao);

    public abstract QueryBuilderJPASQL<F, DTO> getQueryBuilder();
    public abstract Class<F> getFiltroClass();

    protected F criarFiltro(String[] fields, Object[] values) {
        try {
            F filtro = getFiltroClass().newInstance();
            PropertyAccessor myAccessor = PropertyAccessorFactory.forBeanPropertyAccess(filtro);

            if (fields != null) {
                for (int i = 0; i < fields.length; i++) {
                    myAccessor.setPropertyValue(fields[i], values[i]);
                }
            }
            return filtro;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}