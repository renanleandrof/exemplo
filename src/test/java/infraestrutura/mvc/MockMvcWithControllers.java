package infraestrutura.mvc;

import org.springframework.mock.web.MockServletContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.XmlWebApplicationContext;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.lang.reflect.Method;

/**
 * Sets up an application context and adds the given @Controller classes to it.
 */
public final class MockMvcWithControllers {

	private MockMvcWithControllers() {};

	public static MockMvc buildMockMvcForControllers(final String appContextPath, final Object... controllers) {
		WebApplicationContext wac = instantiateXmlWebAppCtx(appContextPath);
		registerControllers(wac, controllers);
		return MockMvcBuilders.webAppContextSetup(wac).build();
	}

	/**
	 * Creates a WebApplicationContext based on the XML of the path given.
	 * @param pathForApplicationContext Full path to the application context XML file. 
	 */
	private static XmlWebApplicationContext instantiateXmlWebAppCtx(final String pathForApplicationContext) {
		XmlWebApplicationContext xmlWebAppContext = new XmlWebApplicationContext();
		xmlWebAppContext.setConfigLocation(pathForApplicationContext);
		xmlWebAppContext.setServletContext(new MockServletContext("file:src/main/webapp"));
		xmlWebAppContext.refresh();
		return xmlWebAppContext;
	}
	
	/**
	 * Calls RequestMappingHandlerMapping#detectHandlerMethods() on each controller given.
	 */
	private static void registerControllers(WebApplicationContext wac, final Object... controllers) {
		RequestMappingHandlerMapping rmhm = wac.getBean(RequestMappingHandlerMapping.class);
		
		Method detectHandlerMethods = ReflectionUtils.findMethod(RequestMappingHandlerMapping.class,
																		"detectHandlerMethods", Object.class);
		boolean wasAccessible = detectHandlerMethods.isAccessible();
		detectHandlerMethods.setAccessible(true);
		for (Object controller : controllers) {
			ReflectionUtils.invokeMethod(detectHandlerMethods, rmhm, controller);
		}
		detectHandlerMethods.setAccessible(wasAccessible);
	}

}