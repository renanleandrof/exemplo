package infraestrutura.sqltemplate;

import com.querydsl.sql.SQLTemplates;
import org.junit.rules.MethodRule;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.Statement;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;

public class InjetarSQLTemplateRule implements MethodRule {

    private SQLTemplates sqlTemplate;

    InjetarSQLTemplateRule(SQLTemplates sqlTemplate) {
        this.sqlTemplate = sqlTemplate;
    }

    @Override
    public Statement apply(Statement base, FrameworkMethod method, Object instanciaDaClasseDeTestes) {
        Class<?> classe = instanciaDaClasseDeTestes.getClass();
        for (Field field : classe.getDeclaredFields()) {
            if (field.isAnnotationPresent(InjetarSQLTemplate.class)) {
                injetar(instanciaDaClasseDeTestes, field);
            }
        }
        return base;
    }

    private void injetar(Object instanciaDaClasseDeTestes, Field fieldComSQLTemplate) {
        ReflectionUtils.makeAccessible(fieldComSQLTemplate);
        Object instanciaDaClasseComOSQLTemplate = ReflectionUtils.getField(fieldComSQLTemplate, instanciaDaClasseDeTestes);
        Field fieldSQLTemplate = ReflectionUtils.findField(instanciaDaClasseComOSQLTemplate.getClass(), "sqlTemplate", SQLTemplates.class);
        ReflectionUtils.makeAccessible(fieldSQLTemplate);
        ReflectionUtils.setField(fieldSQLTemplate, instanciaDaClasseComOSQLTemplate, this.sqlTemplate);
    }
}