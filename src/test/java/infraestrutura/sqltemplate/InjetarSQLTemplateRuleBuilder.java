package infraestrutura.sqltemplate;

import com.querydsl.sql.H2Templates;
import com.querydsl.sql.SQLServer2012Templates;

public class InjetarSQLTemplateRuleBuilder {

    public static InjetarSQLTemplateRule construirSQLTemplateRuleComTemplateDoSQLServer() {
        return new InjetarSQLTemplateRule(SQLServer2012Templates.builder().printSchema().build());
    }

    public static InjetarSQLTemplateRule construirSQLTemplateRuleComTemplateDoH2() {
        return new InjetarSQLTemplateRule(H2Templates.builder().printSchema().build());
    }
}