package infraestrutura.datasource;



import infraestrutura.datasource.factory.DataSourceFactory;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

public class DataSourceMap {

    private DataSourceFactory dataSourceFactory;
    private Map<IdentificadorDataSource, DataSource> dataSources = new HashMap<>();

    public DataSourceMap(DataSourceFactory dataSourceFactory) {
        this.dataSourceFactory = dataSourceFactory;
    }

    public DataSource obter(IdentificadorDataSource identificadorDataSource) {
        DataSource dataSource = this.dataSources.get(identificadorDataSource);
        if (dataSource == null) {
            dataSource = identificadorDataSource.criarDataSource(dataSourceFactory);
            this.dataSources.put(identificadorDataSource, dataSource);
        }
        return dataSource;
    }

}