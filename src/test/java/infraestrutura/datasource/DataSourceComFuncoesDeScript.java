package infraestrutura.datasource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Decora o {@link DataSourceComFuncoesExecutar} com a possibilidade de execucao de scripts esquema e scripts padrao.
 */
public class DataSourceComFuncoesDeScript extends DataSourceComFuncoesExecutar {

    private static final Log LOGGER = LogFactory.getLog(DataSourceComFuncoesExecutar.class);

    private final String[] scriptsEsquema;
    private final String[] scriptsPadrao;

    public DataSourceComFuncoesDeScript(IdentificadorDataSource identificadorDataSource,
                                        String[] scriptsEsquema, String[] scriptsPadrao) {
        super(identificadorDataSource);

        this.scriptsEsquema = scriptsEsquema;
        this.scriptsPadrao = scriptsPadrao;

        executarScriptsEsquema();
    }

    public void executarScriptsEsquema() {
        this.limparBase();
        LOGGER.debug("Executando scripts esquema.");
        for (String scriptDB : this.scriptsEsquema) {
            this.executarScript(scriptDB);
        }
    }

    public void executarScriptsPadrao() {
        LOGGER.debug("Executando scripts padrão.");
        for (String scriptDB : this.scriptsPadrao) {
            this.executarScript(scriptDB);
        }
    }

    public void limparBase() {
        LOGGER.debug("Limpando base de testes.");
        this.dataSourceFactory.limparBase(this.dataSource);
    }

}