package infraestrutura.datasource.factory;


import javax.sql.DataSource;

public interface DataSourceFactory {

    DataSource criarDataSourceJDBC(String nomeDatabase);

    /**
     * Ver javadoc de {@link DataSourceComFuncoesDeScript#executarScript(String)}.
     *
     * @param dataSource DataSource onde o script serah executado.
     * @param localScript Local do script (com prefixo {@code file:} ou {@code classpath:}) que serah executado.
     */
    void executarScript(DataSource dataSource, String localScript);

    void limparBase(DataSource dataSource);

}