package infraestrutura.datasource.factory;


public class DataSourceFactoryManager {

    public enum TipoDataSourceEmbarcado { H2 }

    private static DataSourceFactoryManager instancia;

    public static DataSourceFactoryManager getInstancia() {
        if (instancia == null) {
            instancia = new DataSourceFactoryManager();
        }
        return instancia;
    }

    public DataSourceFactory criarFactory(TipoDataSourceEmbarcado tipoDataSourceEmbarcado) {
        switch (tipoDataSourceEmbarcado) {
            case H2:
                return new H2DataSourceFactory();
            default:
                throw new IllegalArgumentException("O Tipo de DataSource Embarcado '"+tipoDataSourceEmbarcado+"' não é suportado.");
        }
    }

    /**
     * Sobrescreve a instancia deste singleton.
     * Utilizado por conta dos testes.
     * @param dataSourceFactoryManager Instancia a sobrescrever a instancia global.
     */
    public static void sobrescreverInstanciaSingleton(DataSourceFactoryManager dataSourceFactoryManager) {
        instancia = dataSourceFactoryManager;
    }

}