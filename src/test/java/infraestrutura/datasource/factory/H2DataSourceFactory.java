package infraestrutura.datasource.factory;

import infraestrutura.annotation.RemoverCatalogoDasEntidades;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.sql.Statement;

@SuppressWarnings({"SqlDialectInspection", "SqlNoDataSourceInspection"})
public class H2DataSourceFactory implements DataSourceFactory {

    @Override
    public DataSource criarDataSourceJDBC(String nomeDatabase) {
        RemoverCatalogoDasEntidades.remover();
        EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
        EmbeddedDatabase dataSource = builder.setName(nomeDatabase + ";IGNORECASE=TRUE")
                                             .setType(EmbeddedDatabaseType.H2)
                                             .setScriptEncoding("UTF-8").build();
        otimizarParaTestes(dataSource);
        return dataSource;
    }

    /**
     * Otimizacoes no h2, conforme http://www.h2database.com/html/performance.html#fast_import
     *
     * SET CACHE_SIZE nao tem efeito em dbs em memoria, entao nao coloquei.
     *
     * Os demais nao fizeram muita diferenca...
     */
    private void otimizarParaTestes(EmbeddedDatabase dataSource) {
        try {
            // http://www.h2database.com/html/grammar.html#set_log
            dataSource.getConnection().createStatement().execute("SET LOG 0");
//            dataSource.getConnection().createStatement().execute("SET LOCK_MODE 0");
//            dataSource.getConnection().createStatement().execute("SET UNDO_LOG 0");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void executarScript(DataSource dataSource, String localScript) {
        try {
            Statement statement = dataSource.getConnection().createStatement();
            statement.execute("RUNSCRIPT FROM '" + localScript + "'");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void limparBase(DataSource dataSource) {
        try {
            Statement statement = dataSource.getConnection().createStatement();
            statement.execute("DROP ALL OBJECTS;");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}