package infraestrutura.datasource;

import org.apache.commons.lang3.StringUtils;
import org.apache.naming.java.javaURLContextFactory;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NameAlreadyBoundException;
import javax.naming.NamingException;

public class JNDIHelper {

    private final InitialContext context;

    public JNDIHelper() {
        System.setProperty(Context.INITIAL_CONTEXT_FACTORY, javaURLContextFactory.class.getName());
        System.setProperty(Context.URL_PKG_PREFIXES, "org.apache.naming");

        try {
            this.context = new InitialContext();
        } catch (NamingException e) {
            throw new RuntimeException(e);
        }
    }

    public void sobrescreverObjetoJndi(String nomeJNDI, Object objeto) {
        desfazerBindJndi(nomeJNDI);
        removerContexto(nomeJNDI);
        fazerBindJndi(nomeJNDI, objeto);
    }

    private void desfazerBindJndi(String dataSourceJndi) {
        try {
            context.unbind(dataSourceJndi);
        } catch (NamingException ignored) { }
    }

    private void removerContexto(String nomeJNDI) {
        String contextoInicial = nomeJNDI.substring(0, nomeJNDI.lastIndexOf(":") + 1);
        if (StringUtils.isNotBlank(contextoInicial)) {
            try {
                context.destroySubcontext(contextoInicial);
            } catch (NamingException ignored) {}
        }
    }

    private void fazerBindJndi(String nomeJNDI, Object objeto) {
        try {
            try {
                criarContextoJNDI(nomeJNDI);
            } catch (NameAlreadyBoundException ignored) { }
            context.bind(nomeJNDI, objeto);
        } catch (NamingException e) {
            throw new RuntimeException(e);
        }
    }

    private void criarContextoJNDI(String nomeJNDI) throws NamingException {
        String contexto = "";

        int index = nomeJNDI.lastIndexOf(":/");
        if (index != -1) {
            contexto = criarContextoInicial(nomeJNDI, index) + "/";
        } else {
            index = nomeJNDI.lastIndexOf(":");
            if (index != -1) {
                contexto = criarContextoInicial(nomeJNDI, index);
            }
        }

        String[] contextos = nomeJNDI.replace(contexto, "").split("/");
        for (int i = 0; i < contextos.length - 1; i++) {
            contexto += contextos[i];
            context.createSubcontext(contexto);
            contexto += "/";
        }
    }

    private String criarContextoInicial(String nomeJNDI, int index) throws NamingException {
        String contextoInicial;
        contextoInicial = nomeJNDI.substring(0, index + 1);
        context.createSubcontext(contextoInicial);
        return contextoInicial;
    }
}