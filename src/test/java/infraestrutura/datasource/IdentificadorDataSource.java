package infraestrutura.datasource;
import infraestrutura.datasource.factory.DataSourceFactory;

import javax.sql.DataSource;

public class IdentificadorDataSource {

    private final String nomeJndiDoDataSource;
    private final String nomeCatalogo;

    public IdentificadorDataSource(String nomeJndiDoDataSource, String nomeCatalogo) {
        this.nomeJndiDoDataSource = nomeJndiDoDataSource;
        this.nomeCatalogo = nomeCatalogo;
    }

    public String getNomeJndiDoDataSource() {
        return nomeJndiDoDataSource;
    }

    public String getNomeCatalogo() {
        return nomeCatalogo;
    }

    DataSource criarDataSource(DataSourceFactory dataSourceFactory) {
        return dataSourceFactory.criarDataSourceJDBC(getNomeCatalogo());
    }

    @Override
    public String toString() {
        return "JNDI: '" + nomeJndiDoDataSource + "'; Catalogo: '" + nomeCatalogo + '\'';
    }

    /**
     * equals() e hashCode() foram sobrescritos para esta classe poder ser usada como chaves em hashmaps e afins.
     * O código foi gerando utilizando o template do IntelliJ (ALT+INSERT).
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IdentificadorDataSource that = (IdentificadorDataSource) o;
        return nomeJndiDoDataSource.equals(that.nomeJndiDoDataSource) && nomeCatalogo.equals(that.nomeCatalogo);
    }

    @Override
    public int hashCode() {
        int result = nomeJndiDoDataSource.hashCode();
        result = 31 * result + nomeCatalogo.hashCode();
        return result;
    }
}