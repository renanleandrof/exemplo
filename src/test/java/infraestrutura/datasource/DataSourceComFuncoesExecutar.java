package infraestrutura.datasource;

import infraestrutura.datasource.factory.DataSourceFactory;
import infraestrutura.datasource.factory.DataSourceFactoryManager;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import static infraestrutura.datasource.factory.DataSourceFactoryManager.TipoDataSourceEmbarcado.H2;


public class DataSourceComFuncoesExecutar {

    private static final Log LOGGER = LogFactory.getLog(DataSourceComFuncoesExecutar.class);

    protected final DataSourceFactory dataSourceFactory = DataSourceFactoryManager.getInstancia().criarFactory(H2);
    private final DataSourceMap dataSourceMap = new DataSourceMap(dataSourceFactory);

    private final JNDIHelper jndiHelper = new JNDIHelper();

    private final IdentificadorDataSource identificadorDataSource;

    protected DataSource dataSource;

    public DataSourceComFuncoesExecutar(IdentificadorDataSource identificadorDataSource) {
        this.identificadorDataSource = identificadorDataSource;

        inicializarDataSource();
    }

    private void inicializarDataSource() {
        obterDataSourceDoJndiECatalogoSolicitados();
        sobrescreverJndiComDataSource();
    }

    private void obterDataSourceDoJndiECatalogoSolicitados() {
        LOGGER.info(String.format("Obtendo DataSource (%s).", this.identificadorDataSource));
        this.dataSource = dataSourceMap.obter(this.identificadorDataSource);
    }

    private void sobrescreverJndiComDataSource() {
        LOGGER.info("Registrando DataSource no JNDI " + identificadorDataSource.getNomeJndiDoDataSource() + ".");
        jndiHelper.sobrescreverObjetoJndi(identificadorDataSource.getNomeJndiDoDataSource(), this.dataSource);
    }

    public Connection getConexao() throws SQLException {
        return this.dataSource.getConnection();
    }

    /**
     * <p>
     * Executa o script passado no DataSource de testes.
     * </p>
     * <p>
     * Para executar scripts no sistema de arquivos, utilize o prefixo {@code file:}.<br>
     * Para scripts no classpath, utilize o prefixo {@code classpath:}.
     * </p>
     * <p>
     * O caminho especificado no prefixo {@code file:} pode ser absoluto ou relativo. Exemplos:
     * </p>
     * <pre>
     *      .executarScript("file:src/test/resources/dados/script.sql");
     *      .executarScript("file:C:/qualquerCoisa/outracoisa/tralala.sql");
     * </pre>
     *
     * <p>
     * Jah o prefixo {@code classpath:} somente tem sentido com caminho relativo. Exemplo:
     * </p>
     * <pre>
     *      .executarScript("classpath:dados/script.sql");
     * </pre>
     *
     * @param localScript Local do script a ser executado. Deve conter o prefixo {@code file:} ou {@code classpath:}.
     */
    public void executarScript(String localScript) {
        if (!localScript.startsWith("file:") && !localScript.startsWith("classpath:")) {
            throw new IllegalArgumentException("\n\nO local do script ter especificado um prefixo. Exemplos:\n" +
                    "file:caminho/do/script.sql\nou \nclasspath:caminho/do/script.sql\n\n");
        }
        LOGGER.debug("Executando script: "+localScript);
        this.dataSourceFactory.executarScript(this.dataSource, localScript);
    }

    public int executeScalar(String sql) throws Exception {
        ResultSet rs = executeQuery(sql);
        rs.next();
        return rs.getInt(1);
    }

    public ResultSet executeQuery(String sql) throws Exception {
        return getConexao().createStatement().executeQuery(sql);
    }

    public DataSource getDataSource() {
        return dataSource;
    }
}