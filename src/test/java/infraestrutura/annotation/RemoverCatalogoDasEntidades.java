package infraestrutura.annotation;

import org.reflections.Reflections;
import org.reflections.scanners.FieldAnnotationsScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;

import javax.persistence.JoinTable;
import javax.persistence.Table;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Altera a anotação {@link Table } de todas as entidades anotadas para definir como String vazia o valor do atributo
 * <b>catalog</b>.
 *
 */
public class RemoverCatalogoDasEntidades {

    public static void remover() {
        Reflections reflections = new Reflections("br.gov.cgu", new FieldAnnotationsScanner(),
                new TypeAnnotationsScanner(), new SubTypesScanner());
        removerParametroCatalogoDasAnotacoesTable(reflections);
        removerParametroCatalogoDasAnotacoesJoinTable(reflections);
    }

    private static void removerParametroCatalogoDasAnotacoesTable(Reflections reflections) {
        Set<Class<?>> classesAnotadasComTable = reflections.getTypesAnnotatedWith(Table.class);
        Map<String, Object> valorAntigoDasAnotacoes = new HashMap<>();
        alterarValorDoParametroCatalogoDaAnotacaoTable(classesAnotadasComTable, valorAntigoDasAnotacoes);
    }

    private static void alterarValorDoParametroCatalogoDaAnotacaoTable(Set<Class<?>> classesAnotadasComTable,
                                                                Map<String, Object> valorAntigoDasAnotacoes) {
        for (Class classe : classesAnotadasComTable) {
            Annotation[] anotacoes = classe.getAnnotationsByType(Table.class);
            if (anotacoes.length > 0) {
                final Table anotacaoTable = (Table) classe.getAnnotation(Table.class);
                valorAntigoDasAnotacoes.put(classe.getName(), alterarValorDaAnotacao(anotacaoTable, "catalog", ""));
            }
        }
    }

    private static void removerParametroCatalogoDasAnotacoesJoinTable(Reflections reflections) {
        Set<Field> fieldsAnotadosComJoinTable = reflections.getFieldsAnnotatedWith(JoinTable.class);
        Map<String, Object> valorAntigoDasAnotacoes = new HashMap<>();
        alterarValorDoParametroCatalogoDaAnotacaoJoinTable(fieldsAnotadosComJoinTable, valorAntigoDasAnotacoes);
    }

    private static void alterarValorDoParametroCatalogoDaAnotacaoJoinTable(Set<Field> fieldsAnotadosComJoinTable,
                                                                       Map<String, Object> valorAntigoDasAnotacoes) {
        for (Field field : fieldsAnotadosComJoinTable) {
            Annotation[] anotacoes = field.getAnnotationsByType(JoinTable.class);
            if (anotacoes.length > 0) {
                final JoinTable anotacaoJoinTable = field.getAnnotation(JoinTable.class);
                valorAntigoDasAnotacoes.put(field.getName(), alterarValorDaAnotacao(anotacaoJoinTable, "catalog", ""));
            }
        }
    }

    @SuppressWarnings("unchecked")
    private static Object alterarValorDaAnotacao(Annotation annotation, String key, Object newValue){
        Object handler = Proxy.getInvocationHandler(annotation);
        Field f;
        try {
            f = handler.getClass().getDeclaredField("memberValues");
        } catch (NoSuchFieldException | SecurityException e) {
            throw new IllegalStateException(e);
        }
        f.setAccessible(true);
        Map<String, Object> memberValues;
        try {
            memberValues = (Map<String, Object>) f.get(handler);
        } catch (IllegalArgumentException | IllegalAccessException e) {
            throw new IllegalStateException(e);
        }
        Object oldValue = memberValues.get(key);
        if (oldValue == null || oldValue.getClass() != newValue.getClass()) {
            throw new IllegalArgumentException();
        }
        memberValues.put(key,newValue);
        return oldValue;
    }
}