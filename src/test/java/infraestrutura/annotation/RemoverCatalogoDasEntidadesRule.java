package infraestrutura.annotation;

import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import javax.persistence.Table;

/**
 * Altera a anotação {@link Table } de todas as entidades anotadas para definir como String vazia o valor do atributo
 * <b>catalog</b>.
 *
 * Na classe de teste essa Rule deve ser {@code public static} e anotada com o  {@link org.junit.ClassRule}.
 */
public class RemoverCatalogoDasEntidadesRule implements TestRule {

    @Override
    public Statement apply(Statement statement, Description description) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
               RemoverCatalogoDasEntidades.remover();
                statement.evaluate();
            }
        };
    }
}
