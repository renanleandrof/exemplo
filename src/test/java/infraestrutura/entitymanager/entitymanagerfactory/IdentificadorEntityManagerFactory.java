package infraestrutura.entitymanager.entitymanagerfactory;


import infraestrutura.datasource.IdentificadorDataSource;

public class IdentificadorEntityManagerFactory {

    private final String nomePersistenceUnit;
    private final String dataSourceJndi;
    private final String nomeCatalogo;

    public IdentificadorEntityManagerFactory(String nomePersistenceUnit, String dataSourceJndi, String nomeCatalogo) {
        this.nomePersistenceUnit = nomePersistenceUnit;
        this.dataSourceJndi = dataSourceJndi;
        this.nomeCatalogo = nomeCatalogo;
    }

    public String persistenceUnit() {
        return nomePersistenceUnit;
    }

    public IdentificadorDataSource identificadorDataSource() {
        return new IdentificadorDataSource(dataSourceJndi, nomeCatalogo);
    }


    /**
     * equals() e hashCode() foram sobrescritos para esta classe poder ser usada como chaves em hashmaps e afins.
     * O código foi gerando utilizando o template do IntelliJ (ALT+INSERT).
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IdentificadorEntityManagerFactory that = (IdentificadorEntityManagerFactory) o;
        return nomePersistenceUnit.equals(that.nomePersistenceUnit) &&
                dataSourceJndi.equals(that.dataSourceJndi) &&
                nomeCatalogo.equals(that.nomeCatalogo);
    }

    @Override
    public int hashCode() {
        int result = nomePersistenceUnit.hashCode();
        result = 31 * result + dataSourceJndi.hashCode();
        result = 31 * result + nomeCatalogo.hashCode();
        return result;
    }

}