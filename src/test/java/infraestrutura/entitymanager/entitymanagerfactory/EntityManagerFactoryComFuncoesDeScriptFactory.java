package infraestrutura.entitymanager.entitymanagerfactory;

import infraestrutura.datasource.DataSourceComFuncoesDeScript;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

import javax.persistence.EntityManagerFactory;
import javax.persistence.ValidationMode;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/**
 * Mantém um hash de todas as {@link EntityManagerFactoryComFuncoesDeScriptFactory} criadas, para que os testes
 * não precisem ficar recriando a todomomento (jah que a criacao eh uma operação cara).
 */
public class EntityManagerFactoryComFuncoesDeScriptFactory {

    private static final Log LOGGER = LogFactory.getLog(EntityManagerFactoryComFuncoesDeScriptFactory.class);

    private static Map<IdentificadorEntityManagerFactory, EntityManagerFactory> emfMap = new HashMap<>();

    public static EntityManagerFactoryComFuncoesDeScript criar(IdentificadorEntityManagerFactory identificadorEntityManagerFactory,
                                                               String[] scriptsEsquema, String[] scriptsPadrao) {
        DataSourceComFuncoesDeScript dataSourceComFuncoesDeScript = new DataSourceComFuncoesDeScript(
                                                                        identificadorEntityManagerFactory.identificadorDataSource(),
                                                                        scriptsEsquema, scriptsPadrao);
        EntityManagerFactory entityManagerFactory = emfMap.get(identificadorEntityManagerFactory);
        if (entityManagerFactory == null) {
            entityManagerFactory = instanciarEntityManagerFactory(identificadorEntityManagerFactory.persistenceUnit(), dataSourceComFuncoesDeScript.getDataSource());
            emfMap.put(identificadorEntityManagerFactory, entityManagerFactory);
        }
        return new EntityManagerFactoryComFuncoesDeScript(entityManagerFactory, dataSourceComFuncoesDeScript);
    }

    private static EntityManagerFactory instanciarEntityManagerFactory(String nomePersistenceUnit, DataSource dataSource) {
        LOGGER.debug("Criando EntityManagerFactory para o Persistence Unit '" + nomePersistenceUnit + "'...");
        TestEntityManagerFactoryBean beanFactory = new TestEntityManagerFactoryBean();
        beanFactory.setPersistenceUnitName(nomePersistenceUnit);
        beanFactory.setValidationMode(ValidationMode.NONE);
        beanFactory.setPackagesToScan("br.com.ecge.*");
        beanFactory.setPersistenceProviderClass(HibernatePersistenceProvider.class);
        beanFactory.setDataSource(dataSource);
        LOGGER.debug("EntityManagerFactory para o Persistence Unit '"+nomePersistenceUnit+"' criado.");
        try {
            return beanFactory.getNativeEntityManagerFactory();
        } catch (NullPointerException|IllegalStateException e) {
            beanFactory.afterPropertiesSet();
            return beanFactory.getNativeEntityManagerFactory();
        }
    }
}

//É necessário extender a classe pra tornar o CREATE publico.
class TestEntityManagerFactoryBean extends LocalContainerEntityManagerFactoryBean {
    @Override
    public EntityManagerFactory createNativeEntityManagerFactory() {
        return super.createNativeEntityManagerFactory();
    }
}