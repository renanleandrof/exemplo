package infraestrutura.entitymanager.entitymanagerfactory;


import infraestrutura.datasource.DataSourceComFuncoesDeScript;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public class EntityManagerFactoryComFuncoesDeScript {

    private final EntityManagerFactory entityManagerFactory;
    private final DataSourceComFuncoesDeScript dataSourceComFuncoesDeScript;

    private EntityManager entityManager;

    public EntityManagerFactoryComFuncoesDeScript(EntityManagerFactory entityManagerFactory,
                                                  DataSourceComFuncoesDeScript dataSourceComFuncoesDeScript) {
        this.entityManagerFactory = entityManagerFactory;
        this.dataSourceComFuncoesDeScript = dataSourceComFuncoesDeScript;
    }

    public EntityManager obterEntityManager() {
        if (this.entityManager == null) {
            executarScriptsEsquema();
            this.entityManager = entityManagerFactory.createEntityManager();
        }
        return this.entityManager;
    }

    public void executarScriptsEsquema() {
        dataSourceComFuncoesDeScript.executarScriptsEsquema();
    }

    public void executarScriptsPadrao() {
        dataSourceComFuncoesDeScript.executarScriptsPadrao();
    }

    public void executarScript(String localScript) {
        dataSourceComFuncoesDeScript.executarScript(localScript);
    }

}