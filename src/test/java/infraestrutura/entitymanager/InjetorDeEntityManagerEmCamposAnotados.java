package infraestrutura.entitymanager;

import org.springframework.util.ReflectionUtils;
import org.springframework.util.ReflectionUtils.FieldCallback;
import org.springframework.util.ReflectionUtils.FieldFilter;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

/**
 * Esta classe:
 * 
 * - Procura na instancia da classe de teste os campos anotados com @InjetarEntityManager
 * 
 * - Vai nos campos anotados com @InjetarEntityManager, pega o objeto atribuido a estes campos
 * e, neles, localiza os campos anotados com @PersistenceContext e seta em tais campos um
 * EntityManager.
 */
class InjetorDeEntityManagerEmCamposAnotados {
	
	public static void injetarCamposAnotados(Object instanciaDaClasseDeTestes, EntityManager entityManagerAInjetar) {
		
		Class<?> clazz = instanciaDaClasseDeTestes.getClass();
		for (Field f : clazz.getDeclaredFields()) {
			if (f.isAnnotationPresent(InjetarEntityManager.class)) {
				injetar(instanciaDaClasseDeTestes, f, entityManagerAInjetar);
			}
		}
	}

	private static void injetar(final Object instanciaDaClasseDeTestes,
								final Field f,
								final EntityManager entityManagerAInjetar) {
		ReflectionUtils.makeAccessible(f);
		Object instanciaDaClasseSobTestes = ReflectionUtils.getField(f, instanciaDaClasseDeTestes);
		Class<?> classeSobTestes = instanciaDaClasseSobTestes.getClass();

        FieldFilter fieldFilter = field -> {
            boolean campoNaoEhStatic = !Modifier.isStatic(field.getModifiers());
            boolean campoEstahAnotadoComPersistenceContext = field.getAnnotation(PersistenceContext.class) != null;
            return campoNaoEhStatic && campoEstahAnotadoComPersistenceContext;
        };

        FieldCallback fieldCallback = field -> {
            ReflectionUtils.makeAccessible(field);
            ReflectionUtils.setField(field, instanciaDaClasseSobTestes, entityManagerAInjetar);
        };

        ReflectionUtils.doWithFields(classeSobTestes, fieldCallback, fieldFilter);
	}

}