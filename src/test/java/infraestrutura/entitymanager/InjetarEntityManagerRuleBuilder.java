package infraestrutura.entitymanager;

import infraestrutura.TestProperties;
import org.apache.commons.lang.ArrayUtils;

import java.util.Optional;

public class InjetarEntityManagerRuleBuilder {

    private String nomePersistenceUnit;
    private String dataSourceJNDI;
    private String nomeCatalogo;
    private String[] scriptsEsquema;
    private String[] scriptsPadrao;

    public InjetarEntityManagerRuleBuilder(String nomePersistenceUnit, String dataSourceJNDI, String nomeCatalogo,
                                           String[] scriptsEsquema, String[] scriptsPadrao) {
        this.nomePersistenceUnit = nomePersistenceUnit;
        this.dataSourceJNDI = dataSourceJNDI;
        this.nomeCatalogo = nomeCatalogo;
        this.scriptsEsquema = scriptsEsquema;
        this.scriptsPadrao = scriptsPadrao;
    }

    public InjetarEntityManagerRuleBuilder() {
        this(
                TestProperties.get("db.testes.persistenceUnit"),
                TestProperties.get("db.testes.dataSourceJNDI"),
                TestProperties.get("db.testes.nomeCatalogo"),
                TestProperties.get("db.testes.scriptsEsquema").split(","),
                TestProperties.get("db.testes.scriptsPadrao").split(",")
        );
    }

    public static InjetarEntityManagerRuleBuilder profilePadrao() {
        return new InjetarEntityManagerRuleBuilder();
    }

    public static InjetarEntityManagerRuleBuilder profile(String nomeProfile) {
        // TODO falhar se nenhuma propriedade do profile requisitado existir!
        InjetarEntityManagerRuleBuilder builder = new InjetarEntityManagerRuleBuilder();
        builder.nomePersistenceUnit = substituirComProfileSeExistir(nomeProfile, "persistenceUnit", builder.nomePersistenceUnit);
        builder.dataSourceJNDI = substituirComProfileSeExistir(nomeProfile, "dataSourceJNDI", builder.dataSourceJNDI);
        builder.nomeCatalogo = substituirComProfileSeExistir(nomeProfile, "nomeCatalogo", builder.nomeCatalogo);
        builder.scriptsEsquema = substituirComProfileSeExistir(nomeProfile, "scriptsEsquema", builder.scriptsEsquema);
        builder.scriptsPadrao = substituirComProfileSeExistir(nomeProfile, "scriptsPadrao", builder.scriptsPadrao);
        return builder;
    }

    public InjetarEntityManagerRule build() {
        return new InjetarEntityManagerRule(
            this.nomePersistenceUnit,
            this.dataSourceJNDI,
            this.nomeCatalogo,
            this.scriptsEsquema,
            this.scriptsPadrao
        );
    }

    private static String substituirComProfileSeExistir(String nomeProfile, String nomePropriedade, String valorAtualPropriedade) {
        String valorProfile = getPropriedade(Optional.of(nomeProfile), nomePropriedade);
        if (valorProfile.equals("")) {
            return valorAtualPropriedade;
        }
        return valorProfile;
    }

    private static String getPropriedade(Optional<String> nomeProfile, String nomePropriedade) {
        String property = "db.testes." + nomePropriedade;
        if (nomeProfile.isPresent()) {
            property = "db.testes." + nomeProfile.get() + "." + nomePropriedade;
        }
        return TestProperties.get(property);
    }

    private static String[] substituirComProfileSeExistir(String nomeProfile, String nomePropriedade, String[] valorAtualPropriedade) {
        String valorProfile = substituirComProfileSeExistir(nomeProfile, nomePropriedade, (String) null);
        if (valorProfile == null) {
            return valorAtualPropriedade;
        }
        return valorProfile.split(",");
    }

    public InjetarEntityManagerRuleBuilder persistenceUnit(String nomePersistenceUnit) {
        this.nomePersistenceUnit = nomePersistenceUnit;
        return this;
    }

    public InjetarEntityManagerRuleBuilder dataSourceJNDI(String dataSourceJNDI) {
        this.dataSourceJNDI = dataSourceJNDI;
        return this;
    }

    public InjetarEntityManagerRuleBuilder nomeCatalogo(String nomeCatalogo) {
        this.nomeCatalogo = nomeCatalogo;
        return this;
    }

    public InjetarEntityManagerRuleBuilder scriptsEsquema(String... scriptsEsquema) {
        this.scriptsEsquema = scriptsEsquema;
        return this;
    }

    public InjetarEntityManagerRuleBuilder scriptsPadrao(String... scriptsPadrao) {
        this.scriptsPadrao = scriptsPadrao;
        return this;
    }

    public InjetarEntityManagerRuleBuilder adicionarScriptsEsquema(String... scriptsEsquema) {
        this.scriptsEsquema = (String[]) ArrayUtils.addAll(this.scriptsEsquema, scriptsEsquema);
        return this;
    }

    public InjetarEntityManagerRuleBuilder adicionarScriptsPadrao(String... scriptsPadrao) {
        this.scriptsPadrao = (String[]) ArrayUtils.addAll(this.scriptsPadrao, scriptsPadrao);
        return this;
    }

}