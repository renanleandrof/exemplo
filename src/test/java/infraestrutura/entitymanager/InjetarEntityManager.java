package infraestrutura.entitymanager;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Injetará um {@link EntityManager} em todos os campos deste objeto que estiverem
 * anotados com @{@link PersistenceContext} (se existirem).
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface InjetarEntityManager {
}