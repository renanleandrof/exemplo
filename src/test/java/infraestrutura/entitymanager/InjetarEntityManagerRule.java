package infraestrutura.entitymanager;


import infraestrutura.TestProperties;
import infraestrutura.entitymanager.entitymanagerfactory.EntityManagerFactoryComFuncoesDeScript;
import infraestrutura.entitymanager.entitymanagerfactory.EntityManagerFactoryComFuncoesDeScriptFactory;
import infraestrutura.entitymanager.entitymanagerfactory.IdentificadorEntityManagerFactory;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.rules.MethodRule;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.Statement;

import javax.persistence.EntityManager;

public class InjetarEntityManagerRule implements MethodRule {

    private static final Log LOGGER = LogFactory.getLog(InjetarEntityManagerRule.class);

    /**
	 * <p>Retorna o <code>EntityManager</code> injetado nos objetos anotados com <code>@InjetarEntityManager</code> deste teste.</p>
	 *
     * <p>NOTA: Mesmo que <b>nenhum</b> <code>EntityManager</code> tenha sido injetado, este metodo retornará um <code>EntityManager</code>.
	 * válido.</p>
     *
     * @return o <code>EntityManager</code> injetado nos objetos anotados com <code>@InjetarEntityManager</code> deste teste.
	 */
	public EntityManager getEntityManagerInjetado() {
		return em;
	}

    private EntityManagerFactoryComFuncoesDeScript entityManagerFactoryComFuncoesDeScript;

	private EntityManager em;

	public InjetarEntityManagerRule(String nomePersistenceUnit, String dataSourceJndi, String nomeCatalogo,
                                    String[] scriptsEsquema, String[] scriptsPadrao) {
        LOGGER.debug(String.format("Instanciando InjetarEntityManagerRule para: %s / %s / %s",
                nomePersistenceUnit, dataSourceJndi, nomeCatalogo));
        this.entityManagerFactoryComFuncoesDeScript = EntityManagerFactoryComFuncoesDeScriptFactory.criar(
                new IdentificadorEntityManagerFactory(nomePersistenceUnit, dataSourceJndi, nomeCatalogo), scriptsEsquema,
                scriptsPadrao
        );
	}

    @Override
	public Statement apply(Statement base, FrameworkMethod method, Object instanciaDaClasseDeTestes) {
        LOGGER.debug(String.format("Obtendo EntityManager para teste %s", method));
		this.em = entityManagerFactoryComFuncoesDeScript.obterEntityManager();
        entityManagerFactoryComFuncoesDeScript.executarScriptsEsquema();
        entityManagerFactoryComFuncoesDeScript.executarScriptsPadrao();
		InjetorDeEntityManagerEmCamposAnotados.injetarCamposAnotados(instanciaDaClasseDeTestes, this.em);
		return base;
	}




    /**
     * Use {@link InjetarEntityManagerRuleBuilder#profilePadrao()}, seguido de
     * {@link InjetarEntityManagerRuleBuilder#build()}. Exemplo:
     * <pre>
     * &#64;Rule
     * public InjetarEntityManagerRule injetarEntityManagerRule = InjetarEntityManagerRuleBuilder.profilePadrao().build();
     * </pre>
     */
    @Deprecated
    public InjetarEntityManagerRule() {
        this(
                StringUtils.defaultString(
                        TestProperties.get("db.testes.persistenceUnit"),
                        TestProperties.get("db.testes.nome.persistence.unit") // fallback pra retrocompatibilidade
                ),
                StringUtils.defaultString(
                        TestProperties.get("db.testes.dataSourceJNDI"),
                        TestProperties.get("db.testes.jndi.data.source") // fallback pra retrocompatibilidade
                ),
                StringUtils.defaultString(
                        TestProperties.get("db.testes.nomeCatalogo"),
                        "bancoDeTestesEmMemoria" // fallback pra retrocompatibilidade
                ),
                StringUtils.defaultString(
                        TestProperties.get("db.testes.scriptsEsquema"),
                        "" // fallback pra retrocompatibilidade
                ).split(","),
                StringUtils.defaultString(
                        TestProperties.get("db.testes.scriptsPadrao"),
                        "" // fallback pra retrocompatibilidade
                ).split(",")
        );
    }

}