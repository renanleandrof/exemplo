package br.com.ecge.autorizador.web;

import br.com.ecge.autorizador.aplicacao.BuscadorDeUsuario;
import br.com.ecge.autorizador.aplicacao.UsuarioDTO;
import br.com.ecge.autorizador.negocio.Usuario;
import infraestrutura.mvc.ControllerTest;
import infraestrutura.persistencia.querybuilder.RespostaConsulta;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class BuscarUsuarioControllerTest extends ControllerTest {

    private BuscadorDeUsuario buscador = mock(BuscadorDeUsuario.class);
    private BuscarUsuarioController controller = new BuscarUsuarioController(buscador);

    private BuscadorDeUsuario usuarioService = mock(BuscadorDeUsuario.class);
    private ControllerAdvice advice = new ControllerAdvice(usuarioService);

    @Override
    public Object getController() { return controller; }

    @Override
    protected Object getControllerAdvice() {
        return advice;
    }

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        when(usuarioService.getUsuarioLogado()).thenReturn(mock(Usuario.class));
    }

    @Test
    public void modoPopup_retorna_view_com_variavel() throws Exception {
        mockMvc.perform(get("/popup/Usuario"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("modoPopup"))
                .andExpect(view().name("usuario/usuarios"));
    }

    @Test
    public void listar_retorna_view() throws Exception {
        mockMvc.perform(get("/auth/usuarios/"))
               .andExpect(status().isOk())
               .andExpect(view().name("usuario/usuarios"));
    }

    @Test
    public void grid_retorna_json() throws Exception {
        List<UsuarioDTO> registros = new ArrayList<>();
        registros.add(mockDTOs());
        RespostaConsulta<UsuarioDTO> respostaConsulta = new RespostaConsulta<>(registros, 1L);

        when(buscador.buscar(any())).thenReturn(respostaConsulta);

        mockMvc.perform(get("/api/auth/usuarios").param("dataTablesRequest", "{}"))
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().string(
                        "{\"draw\":0,\"recordsTotal\":1,\"recordsFiltered\":1,\"data\":[{\"id\":1,\"nome\":\"User Name\",\"login\":\"CGU\\\\user\",\"email\":\"user@cgu.gov.br\",\"ativo\":true}],\"error\":null}"));
    }


    private UsuarioDTO mockDTOs() {
        return new UsuarioDTO(1, "User Name", "CGU\\user", "user@cgu.gov.br", true);
    }
}
