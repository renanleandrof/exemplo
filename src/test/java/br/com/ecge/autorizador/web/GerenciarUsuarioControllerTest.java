package br.com.ecge.autorizador.web;

import br.com.ecge.autorizador.Constantes;
import br.com.ecge.autorizador.aplicacao.BuscadorDePermissao;
import br.com.ecge.autorizador.aplicacao.BuscadorDeUsuario;
import br.com.ecge.autorizador.aplicacao.GerenciadorDeUsuario;
import br.com.ecge.autorizador.negocio.Permissao;
import br.com.ecge.autorizador.negocio.Usuario;
import br.com.ecge.autorizador.negocio.UsuarioSemPerfilException;
import infraestrutura.mvc.ControllerTest;
import infraestrutura.persistencia.jpa.EntidadeInvalidaException;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class GerenciarUsuarioControllerTest extends ControllerTest {

    private static final Permissao ADMINISTRADOR = new Permissao(0, "Administrador");

    private GerenciadorDeUsuario gerenciador = mock(GerenciadorDeUsuario.class);
    private BuscadorDeUsuario buscador = mock(BuscadorDeUsuario.class);
    private BuscadorDePermissao buscadorDePermissao = mock(BuscadorDePermissao.class);

    private GerenciarUsuarioController controller = new GerenciarUsuarioController(gerenciador, buscador,
            buscadorDePermissao);

    private BuscadorDeUsuario usuarioService = mock(BuscadorDeUsuario.class);
    private ControllerAdvice advice = new ControllerAdvice(usuarioService);

    @Override
    public Object getController() { return controller; }

    @Override
    protected Object getControllerAdvice() {
        return advice;
    }

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        when(usuarioService.getUsuarioLogado()).thenReturn(mock(Usuario.class));
    }

    @Test
    public void getNovo__retorna_a_view_com_dto_vazio() throws Exception {
        mockMvc.perform(get("/auth/usuarios/novo"))
               .andExpect(status().isOk())
               .andExpect(model().attributeExists("usuario"))
               .andExpect(view().name("usuario/usuario"));
    }

    @Test
    public void getId__retorna_a_view_com_dto_do_banco() throws Exception {
        Usuario mock = mock(Usuario.class);
        when(buscador.getParaEdicao(3)).thenReturn(mock);

        mockMvc.perform(get("/auth/usuarios/3"))
               .andExpect(status().isOk())
               .andExpect(model().attribute("usuario", mock))
               .andExpect(view().name("usuario/usuario"));
    }

    @Test
    public void post__salva_com_sucesso_e_exibe_mensagem() throws Exception {
        // given
    	Usuario usuario = new Usuario();
    	usuario.setId(10);
    	
        String jsonUsuario = "{\"id\": 10}";

        // when
        when(gerenciador.salvar(any())).thenReturn(usuario);

        // then
        mockMvc.perform(post("/api/auth/usuarios/salvar")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
        				.content(jsonUsuario))
               .andExpect(status().isOk());
    }

    @Test
    public void get__exibe_mensagem_de_sucesso() throws Exception {
        mockMvc.perform(get("/auth/usuarios/1")
                                .param("showMsg", "Minha msg de sucesso!"))
               .andExpect(status().isOk());
    }

    @Test
    public void post__exibe_erro_de_validacao() throws Exception {
        when(gerenciador.salvar(any())).thenThrow(new EntidadeInvalidaException("x"));

        mockMvc.perform(post("/auth/usuarios/1")
        		.param("usuario", "x"))
               .andExpect(status().is4xxClientError());
    }

    @Test
    public void get__exibe_erro_de_permissao() throws Exception {
        when(buscador.getParaEdicao(any())).thenThrow(new UsuarioSemPerfilException(ADMINISTRADOR));

        mockMvc.perform(get("/auth/usuarios/1")
		        		.param("usuario", "x"))
               .andExpect(status().isForbidden());
    }
    
    @Test
    public void post__deve_informar_que_a_entidade_eh_invalida() throws Exception {
        String jsonUsuario = "{\"id\": 10}";

        EntidadeInvalidaException e = new EntidadeInvalidaException("Nome é campo obrigatório.");
        // when
        when(gerenciador.salvar(any())).thenThrow(e);

        // then
        mockMvc.perform(post("/api/auth/usuarios/salvar")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonUsuario))
               .andExpect(status().isBadRequest())
               .andExpect(content().string("Nome é campo obrigatório."));
    }

    @Test
    public void resetar_senha_deve_retornar_nova_senha() throws Exception {
        // when
        when(gerenciador.resetarSenha(3)).thenReturn("ABC123");

        // then
        mockMvc.perform(post("/api/auth/usuarios/3/resetar-senha"))
                .andExpect(status().isOk())
                .andExpect(content().string("ABC123"));
    }

    @Test
    public void alterarAtivacao__deve_retornar_msg_de_sucesso_ao_ativar() throws Exception {
        mockMvc.perform(post("/auth/usuarios/ativar/1"))
               .andExpect(status().isOk())
               .andExpect(content().string(Constantes.OPERACAO_REALIZADA_COM_SUCESSO));
    }

    @Test
    public void alterarAtivacao__deve_retornar_msg_de_sucesso_ao_inativar() throws Exception {
        mockMvc.perform(post("/auth/usuarios/inativar/1"))
               .andExpect(status().isOk())
               .andExpect(content().string(Constantes.OPERACAO_REALIZADA_COM_SUCESSO));
    }

    @Test
    public void alterarAtivacao__deve_retornar_msg_de_sucesso_ao_alternar() throws Exception {
        mockMvc.perform(post("/auth/usuarios/alternar-ativacao/1"))
               .andExpect(status().isOk())
               .andExpect(content().string(Constantes.OPERACAO_REALIZADA_COM_SUCESSO));
    }


}