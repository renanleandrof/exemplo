package br.com.ecge.autorizador.web;

import br.com.ecge.autorizador.Constantes;
import br.com.ecge.autorizador.aplicacao.BuscadorDeSistema;
import br.com.ecge.autorizador.aplicacao.GerenciadorDeSistema;
import br.com.ecge.autorizador.negocio.Sistema;
import infraestrutura.mvc.ControllerTest;
import infraestrutura.persistencia.jpa.EntidadeInvalidaException;
import org.junit.Test;
import org.springframework.http.MediaType;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class GerenciarSistemaControllerTest extends ControllerTest {

    private GerenciadorDeSistema gerenciador = mock(GerenciadorDeSistema.class);
    private BuscadorDeSistema buscador = mock(BuscadorDeSistema.class);

    private GerenciarSistemaController controller = new GerenciarSistemaController(gerenciador, buscador);

    @Override
    public Object getController() { return controller; }

    @Test
    public void getNovo__retorna_a_view_com_dto_vazio() throws Exception {
        mockMvc.perform(get("/auth/sistemas/novo"))
               .andExpect(status().isOk())
               .andExpect(model().attributeExists("sistema"))
               .andExpect(view().name("sistema/sistema"));
    }

    @Test
    public void getId__retorna_a_view_com_dto_do_banco() throws Exception {
        Sistema mock = mock(Sistema.class);
        when(buscador.getParaEdicao(3)).thenReturn(mock);

        mockMvc.perform(get("/auth/sistemas/3"))
               .andExpect(status().isOk())
               .andExpect(model().attribute("sistema", mock))
               .andExpect(view().name("sistema/sistema"));
    }

    @Test
    public void post__salva_com_sucesso_e_exibe_mensagem() throws Exception {
        // given
    	Sistema sistema = new Sistema();
    	sistema.setId(10);
    	
        String json = "{\"id\": 10}";

        // when
        when(gerenciador.salvar(any())).thenReturn(sistema);

        // then
        mockMvc.perform(post("/api/auth/sistemas/salvar")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
        				.content(json))
               .andExpect(status().isOk());
    }

    @Test
    public void post__exibe_erro_de_validacao() throws Exception {
        when(gerenciador.salvar(any())).thenThrow(new EntidadeInvalidaException("x"));

        mockMvc.perform(post("/auth/sistemas/1")
        		.param("sistema", "x"))
               .andExpect(status().is4xxClientError());
    }

    @Test
    public void post__deve_informar_que_a_entidade_eh_invalida() throws Exception {
        String jsonUsuario = "{\"id\": 10}";

        EntidadeInvalidaException e = new EntidadeInvalidaException("Nome é campo obrigatório.");
        // when
        when(gerenciador.salvar(any())).thenThrow(e);

        // then
        mockMvc.perform(post("/api/auth/sistemas/salvar")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonUsuario))
               .andExpect(status().isBadRequest())
               .andExpect(content().string("Nome é campo obrigatório."));
    }

    @Test
    public void alterarAtivacao__deve_retornar_msg_de_sucesso_ao_ativar() throws Exception {
        mockMvc.perform(post("/auth/sistemas/ativar/1"))
               .andExpect(status().isOk())
               .andExpect(content().string(Constantes.OPERACAO_REALIZADA_COM_SUCESSO));
    }

    @Test
    public void alterarAtivacao__deve_retornar_msg_de_sucesso_ao_inativar() throws Exception {
        mockMvc.perform(post("/auth/sistemas/inativar/1"))
               .andExpect(status().isOk())
               .andExpect(content().string(Constantes.OPERACAO_REALIZADA_COM_SUCESSO));
    }

    @Test
    public void alterarAtivacao__deve_retornar_msg_de_sucesso_ao_alternar() throws Exception {
        mockMvc.perform(post("/auth/sistemas/alternar-ativacao/1"))
               .andExpect(status().isOk())
               .andExpect(content().string(Constantes.OPERACAO_REALIZADA_COM_SUCESSO));
    }


}