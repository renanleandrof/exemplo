package br.com.ecge.autorizador.web;

import br.com.ecge.autorizador.aplicacao.BuscadorDePermissao;
import br.com.ecge.autorizador.aplicacao.PermissaoDTO;
import infraestrutura.mvc.ControllerTest;
import infraestrutura.persistencia.querybuilder.RespostaConsulta;
import org.junit.Test;
import org.springframework.http.MediaType;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class BuscarPermissaoControllerTest extends ControllerTest {
    private BuscadorDePermissao buscador = mock(BuscadorDePermissao.class);
    private BuscarPermissaoController controller = new BuscarPermissaoController(buscador);

    @Override
    public Object getController() {
        return controller;
    }

    @Test
    public void listar_retorna_view() throws Exception {
        mockMvc.perform(get("/auth/permissoes/"))
                .andExpect(status().isOk())
                .andExpect(view().name("permissao/permissoes"));
    }

    @Test
    public void grid_retorna_json() throws Exception {
        List<PermissaoDTO> registros = new ArrayList<>();
        registros.add(mockDTOs());
        RespostaConsulta<PermissaoDTO> respostaConsulta = new RespostaConsulta<>(registros, 1L);

        when(buscador.buscar(any())).thenReturn(respostaConsulta);

        mockMvc.perform(get("/api/auth/permissoes"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().string(
                        "{\"draw\":0,\"recordsTotal\":1,\"recordsFiltered\":1,\"data\":[{\"id\":1213,\"nome\":\"abcasdasd\",\"sistema\":\"sisetma\",\"ativo\":true}],\"error\":null}"));
    }

    private PermissaoDTO mockDTOs() {
        return new PermissaoDTO(1213,"abcasdasd", "sisetma", true);
    }

}