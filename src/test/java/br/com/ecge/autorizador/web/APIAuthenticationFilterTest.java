package br.com.ecge.autorizador.web;

import br.com.ecge.autorizador.aplicacao.BuscadorDeUsuario;
import br.com.ecge.autorizador.negocio.Usuario;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockFilterChain;
import org.springframework.mock.web.MockFilterConfig;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.servlet.FilterChain;
import java.util.Collections;

import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.util.ReflectionTestUtils.setField;

public class APIAuthenticationFilterTest {

    private BuscadorDeUsuario buscador = mock(BuscadorDeUsuario.class);
    private APIAuthenticationFilter filter = new APIAuthenticationFilter();

    @Before
    public void setUp() throws Exception {
        setField(filter, "buscador", buscador);
    }

    @Test
    public void init_e_destroy_nao_faz_nada() throws Exception {
        filter.init(new MockFilterConfig());
        filter.destroy();
    }

    @Test
    public void doFilter__chama_a_chain_e_finaliza_se_chave_for_vazia() throws Exception {
        MockFilterChain filterChain = new MockFilterChain();
        MockHttpServletResponse servletResponse = new MockHttpServletResponse();
        MockHttpServletRequest servletRequest = new MockHttpServletRequest();
        servletRequest.setRequestURI("/api/auth/blabla");

        filter.doFilter(servletRequest, servletResponse, filterChain);
    }

    @Test
    public void doFilter__responseError_se_chave_nao_pertence_a_nenhum_usuario() throws Exception {
        MockFilterChain filterChain = new MockFilterChain();
        MockHttpServletResponse servletResponse = new MockHttpServletResponse();
        MockHttpServletRequest servletRequest = new MockHttpServletRequest();
        servletRequest.setRequestURI("/api/auth/blabla");

        servletRequest.addHeader(APIAuthenticationFilter.HEADER_CHAVE_API, "ABACADS");
        filter.doFilter(servletRequest, servletResponse, filterChain);

        assertThat(servletResponse.getErrorMessage(), is("A chave de API informada é inválida."));
        assertThat(servletResponse.getStatus(), is(HttpStatus.FORBIDDEN.value()));
    }

    @Test
    public void doFilter_continua_caso_ja_esteja_autenticado() throws Exception {
        FilterChain filterChain = mock(FilterChain.class);
        MockHttpServletResponse servletResponse = new MockHttpServletResponse();
        MockHttpServletRequest servletRequest = new MockHttpServletRequest();
        servletRequest.setRequestURI("/api/auth/blabla");
        SecurityContextHolder.clearContext();
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken("","", Collections.emptyList()));

        filter.doFilter(servletRequest, servletResponse, filterChain);

        verify(filterChain).doFilter(servletRequest, servletResponse);
    }

    @Test
    public void doFilter__troca_usuario_da_sessao_e_desfaz_apos_o_chain() throws Exception {
        final Usuario usuarioDaChave = new Usuario();
        when(buscador.getUsuarioPorChaveApi("ABACADS")).thenReturn(usuarioDaChave);
        SecurityContextHolder.clearContext();

        MockHttpServletResponse servletResponse = new MockHttpServletResponse();
        MockHttpServletRequest servletRequest = new MockHttpServletRequest();
        servletRequest.setRequestURI("/api/auth/blabla");
        FilterChain filterChain = mock(FilterChain.class);
        doAnswer(invocation -> {
            assertThat(SecurityContextHolder.getContext().getAuthentication().getPrincipal(), is(usuarioDaChave));
            return null;
        }).when(filterChain).doFilter(any(), any());

        servletRequest.addHeader(APIAuthenticationFilter.HEADER_CHAVE_API, "ABACADS");
        filter.doFilter(servletRequest, servletResponse, filterChain);

        assertThat(SecurityContextHolder.getContext().getAuthentication(), is(nullValue()));
    }
}