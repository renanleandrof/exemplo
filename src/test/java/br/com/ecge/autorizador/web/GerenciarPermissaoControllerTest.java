package br.com.ecge.autorizador.web;

import br.com.ecge.autorizador.Constantes;
import br.com.ecge.autorizador.aplicacao.BuscadorDePermissao;
import br.com.ecge.autorizador.aplicacao.GerenciadorDePermissao;
import br.com.ecge.autorizador.negocio.Permissao;
import infraestrutura.mvc.ControllerTest;
import infraestrutura.persistencia.jpa.EntidadeInvalidaException;
import org.junit.Test;
import org.springframework.http.MediaType;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class GerenciarPermissaoControllerTest extends ControllerTest {

    private final GerenciadorDePermissao gerenciadorDePermissao = mock(GerenciadorDePermissao.class);
    private final BuscadorDePermissao buscadorDePermissao = mock(BuscadorDePermissao.class);

    private GerenciarPermissaoController controller = new GerenciarPermissaoController(gerenciadorDePermissao, buscadorDePermissao);

    @Override
    public Object getController() {
        return controller;
    }

    @Test
    public void exibirTelaCadastro__preenche_model_e_retorna_view() throws Exception {
        mockMvc.perform(get("/auth/permissoes/novo"))
                .andExpect(status().isOk())
                .andExpect(view().name("permissao/permissao"))
                .andExpect(model().attributeExists("permissao"));
    }

    @Test
    public void exibirTelaEdicao__preenche_model_e_retorna_view() throws Exception {
        Permissao p = mockPermissao();
        when(buscadorDePermissao.getParaEdicao(1234)).thenReturn(p);

        mockMvc.perform(get("/auth/permissoes/1234"))
                .andExpect(status().isOk())
                .andExpect(view().name("permissao/permissao"))
                .andExpect(model().attribute("permissao", p));
    }

    @Test
    public void exibirTelaEdicao__deve_ter_mensagem_de_sucesso_se_parametro_estiver_presente() throws Exception {
        Permissao p = mockPermissao();
        when(buscadorDePermissao.getParaEdicao(1234)).thenReturn(p);

        mockMvc.perform(get("/auth/permissoes/1234"))
                .andExpect(status().isOk())
                .andExpect(view().name("permissao/permissao"))
                .andExpect(model().attribute("permissao", p));
    }

    private Permissao mockPermissao() {
        Permissao permissao = new Permissao();
        permissao.setId(1234);
        return permissao;
    }

    @Test
    public void post__salva_com_sucesso_e_exibe_mensagem() throws Exception {
        // given
        Permissao aviso = mockPermissao();
        aviso.setId(123312);

        String postContent = "{\"id\": 123312}";

        // when
        when(gerenciadorDePermissao.salvar(any())).thenReturn(aviso);

        // then
        mockMvc.perform(post("/api/auth/permissoes/salvar")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(postContent))
                .andExpect(status().isOk())
                .andExpect(content().string("123312"));
    }

    @Test
    public void post__deve_informar_que_o_aviso_eh_invalido() throws Exception {
        String jsonAviso = "{\"id\": 10}";

        EntidadeInvalidaException e = new EntidadeInvalidaException("Titulo é campo obrigatório.");
        // when
        when(gerenciadorDePermissao.salvar(any())).thenThrow(e);

        // then
        mockMvc.perform(post("/api/auth/permissoes/salvar")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonAviso))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("Titulo é campo obrigatório."));
    }


    @Test
    public void alterarAtivacao__deve_retornar_msg_de_sucesso_ao_ativar() throws Exception {
        mockMvc.perform(post("/auth/permissoes/ativar/1"))
                .andExpect(status().isOk())
                .andExpect(content().string(Constantes.OPERACAO_REALIZADA_COM_SUCESSO));
    }

    @Test
    public void alterarAtivacao__deve_retornar_msg_de_sucesso_ao_inativar() throws Exception {
        mockMvc.perform(post("/auth/permissoes/inativar/1"))
                .andExpect(status().isOk())
                .andExpect(content().string(Constantes.OPERACAO_REALIZADA_COM_SUCESSO));
    }

    @Test
    public void alterarAtivacao__deve_retornar_msg_de_sucesso_ao_alternar() throws Exception {
        mockMvc.perform(post("/auth/permissoes/alternar-ativacao/1"))
                .andExpect(status().isOk())
                .andExpect(content().string(Constantes.OPERACAO_REALIZADA_COM_SUCESSO));
    }
}