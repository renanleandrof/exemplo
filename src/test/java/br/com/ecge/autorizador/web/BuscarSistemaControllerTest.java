package br.com.ecge.autorizador.web;

import br.com.ecge.autorizador.aplicacao.BuscadorDeSistema;
import br.com.ecge.autorizador.aplicacao.SistemaDTO;
import infraestrutura.mvc.ControllerTest;
import infraestrutura.persistencia.querybuilder.RespostaConsulta;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import java.util.ArrayList;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class BuscarSistemaControllerTest extends ControllerTest {

    private BuscadorDeSistema buscador = mock(BuscadorDeSistema.class);
    private BuscarSistemaController controller = new BuscarSistemaController(buscador);

    @Override
    public Object getController() { return controller; }


    @Test
    public void listar__deve_retornar_view_com_filtros() throws Exception {

        MvcResult mvcResult = mockMvc.perform(get("/auth/sistemas").param("nome", "clebinho"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("filtro"))
                .andExpect(view().name("sistema/sistemas"))
                .andReturn();

        SistemaFiltro filtro = (SistemaFiltro) mvcResult.getModelAndView().getModel().get("filtro");

        assertThat(filtro.getNome(), is("clebinho"));
    }


    @Test
    public void grid__deve_buscar_dtos_e_retornar_json() throws Exception {
        when(buscador.buscar(any())).thenReturn(mockRespostaConsulta());

        mockMvc.perform(get("/api/auth/sistemas"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().string("{\"draw\":0,\"recordsTotal\":3,\"recordsFiltered\":3,\"data\":[{\"id\":1,\"nome\":\"Clebinho\",\"ativo\":true},{\"id\":2,\"nome\":\"Renan\",\"ativo\":true},{\"id\":3,\"nome\":\"Deletado\",\"ativo\":false}],\"error\":null}"));
    }

    private RespostaConsulta<SistemaDTO> mockRespostaConsulta() {
        ArrayList<SistemaDTO> dtos = new ArrayList<>();

        dtos.add(new SistemaDTO(1, "Clebinho", true));
        dtos.add(new SistemaDTO(2, "Renan", true));
        dtos.add(new SistemaDTO(3, "Deletado", false));

        return new RespostaConsulta<>(dtos, 3L);
    }
}