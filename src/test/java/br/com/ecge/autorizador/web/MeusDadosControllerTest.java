package br.com.ecge.autorizador.web;

import br.com.ecge.autorizador.Constantes;
import br.com.ecge.autorizador.aplicacao.BuscadorDeUsuario;
import br.com.ecge.autorizador.aplicacao.GerenciadorDeUsuario;
import br.com.ecge.autorizador.negocio.Permissao;
import br.com.ecge.autorizador.negocio.Sistema;
import br.com.ecge.autorizador.negocio.Usuario;
import infraestrutura.mvc.ControllerTest;
import infraestrutura.persistencia.jpa.EntidadeInvalidaException;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class MeusDadosControllerTest extends ControllerTest {

    private GerenciadorDeUsuario gerenciador = mock(GerenciadorDeUsuario.class);
    private BuscadorDeUsuario buscador = mock(BuscadorDeUsuario.class);

    private MeusDadosController controller = new MeusDadosController(gerenciador, buscador);

    private BuscadorDeUsuario usuarioService = mock(BuscadorDeUsuario.class);
    private ControllerAdvice advice = new ControllerAdvice(usuarioService);

    @Override
    public Object getController() { return controller; }

    @Override
    protected Object getControllerAdvice() {
        return advice;
    }

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        when(usuarioService.getUsuarioLogado()).thenReturn(mock(Usuario.class));
    }

    @Test
    public void gerarChaveApi__deve_retornar_chaveapi_sugerida()  throws Exception {
        // given
        Usuario userMock = mock(Usuario.class);

        // when
        when(usuarioService.getUsuarioLogado()).thenReturn(userMock);
        when(userMock.sugerirChaveApi()).thenReturn("chave_sugerida");

        mockMvc.perform(get("/auth/usuarioLogado/gerarChaveApi"))
                .andExpect(status().isOk())
                .andExpect(content().string("chave_sugerida"));
    }

    @Test
    public void salvarUsuarioLogado__deve_alterar_dados_do_usuario_logado() throws Exception {
        // given
        Usuario userMock = mock(Usuario.class);

        // when
        when(usuarioService.getUsuarioLogado()).thenReturn(userMock);

        mockMvc.perform(post("/auth/usuarioLogado/salvar")
                .param("emailUsuarioLogado", "email@cgu.gov.br")
                .param("telefoneUsuarioLogado", "(61) 9999-9999")
                .param("chaveApiUsuarioLogado", "chave_sugerida")
                .param("frequenciaRecebimentoEmail", "3"))
                .andExpect(status().isOk())
                .andExpect(content().string(Constantes.OPERACAO_REALIZADA_COM_SUCESSO));
    }

    @Test
    public void salvarUsuarioLogado__deve_retornar_erros() throws Exception {
        // when
        doThrow(new EntidadeInvalidaException("ABC 123")).when(gerenciador).alterarDadosUsuarioLogado(any());

        mockMvc.perform(post("/auth/usuarioLogado/salvar"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("ABC 123"));
    }

    @Test
    public void meusDados__retorna_a_view() throws Exception {
        mockMvc.perform(get("/auth/meus-dados"))
                .andExpect(status().isOk())
                .andExpect(view().name("usuario/meusDados"));
    }

    @Test
    public void permissoesDoUsuario__retorna_mapa_sistema_permissoes() throws Exception {
        Usuario u = new Usuario();
        Sistema s1 = new Sistema();
        s1.setId(1);
        s1.setNome("Sistema 1");
        Sistema s2 = new Sistema();
        s2.setId(2);
        s2.setNome("Sistema 2");


        u.getPermissoes().add(criarPermissao("ABC", s1));
        u.getPermissoes().add(criarPermissao("ABCD", s1));
        u.getPermissoes().add(criarPermissao("ABCAA", s2));
        u.getPermissoes().add(criarPermissao("ABCXX", s2));

        when(buscador.getUsuarioLogado()).thenReturn(u);

        mockMvc.perform(get("/api/auth/minhas-permissoes"))
                .andExpect(status().isOk())
                .andExpect(content().string("{\"Sistema 2\":[\"ABCAA\",\"ABCXX\"],\"Sistema 1\":[\"ABC\",\"ABCD\"]}"));
    }

    private Permissao criarPermissao(String nome, Sistema sistema) {
        Permissao p = new Permissao();
        p.setNome(nome);
        p.setSistema(sistema);
        return p;
    }

}