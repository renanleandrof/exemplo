package br.com.ecge.autorizador.negocio;

import infraestrutura.entitymanager.InjetarEntityManager;
import infraestrutura.entitymanager.InjetarEntityManagerRule;
import infraestrutura.entitymanager.InjetarEntityManagerRuleBuilder;
import infraestrutura.sqltemplate.InjetarSQLTemplate;
import infraestrutura.sqltemplate.InjetarSQLTemplateRule;
import infraestrutura.sqltemplate.InjetarSQLTemplateRuleBuilder;
import org.junit.Rule;
import org.junit.Test;

import java.util.List;

import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class UsuarioRepositoryTest {

    @Rule
    public InjetarEntityManagerRule emRule = InjetarEntityManagerRuleBuilder.profilePadrao().build();

    @Rule
    public InjetarSQLTemplateRule sqlTemplateRule = InjetarSQLTemplateRuleBuilder.construirSQLTemplateRuleComTemplateDoH2();

    @InjetarSQLTemplate
    @InjetarEntityManager
    private static UsuarioRepository repository = new UsuarioRepository();

    @Test
    public void getPorLogin() {
        Usuario admin = repository.getPorLogin("renanlf");

        assertThat(admin.getLogin(), is("renanlf"));
    }

    @Test
    public void getPorChaveApi__deve_recuperar_um_usuario_especifico_com_a_chaveApi_informada() {
        Usuario user = repository.getPorChaveApi("0fadfb8fd4f7cc48f04eafa048c32602");

        assertThat(user.getLogin(), is("ggomest"));
    }


    @Test
    public void getPorTermo__deve_retornar_usuarios_que_tenham_o_termo_no_nome() {
        String termo = "Teste";

        List<Usuario> usuarios = repository.getPorTermo(termo);

        assertThat(usuarios, hasSize(greaterThan(0)));

        for (Usuario usuario : usuarios) {
            assertThat(usuario.getNome(), containsString(termo));
        }
    }

    @Test
    public void getPorIds__deve_recuperar_usuarios_com_os_ids_informados() {
        List<Integer> ids = asList(1, 2, 3);

        List<Usuario> usuarios = repository.getPorIds(ids);

        assertThat(usuarios, hasSize(3));

        for (Usuario usuario : usuarios) {
            assertThat(usuario.getId(), isIn(ids));
        }
    }

}