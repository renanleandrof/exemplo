package br.com.ecge.autorizador.negocio;

import infraestrutura.AssertUtils;
import infraestrutura.entitymanager.InjetarEntityManager;
import infraestrutura.entitymanager.InjetarEntityManagerRule;
import infraestrutura.entitymanager.InjetarEntityManagerRuleBuilder;
import org.junit.Rule;
import org.junit.Test;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.primitives.Ints.asList;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.isIn;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class PermissaoRepositoryTest {
    @Rule
    public InjetarEntityManagerRule emRule = InjetarEntityManagerRuleBuilder.profilePadrao().build();

    @InjetarEntityManager
    private PermissaoRepository repository = new PermissaoRepository();

    @Test
    public void buscar__todos_ordenados_por_nome() {
        // Given

        // When
        List<Permissao> p = repository.buscarTodosOrdenadosPorNome();

        // Then
        assertThat(p, hasSize(9));
        AssertUtils.assertListaOrdenadaPorPropriedade(p, Comparator.comparing(o -> o.getNome().toLowerCase()));
    }

    @Test
    public void getPorIds__retorna_da_lista_informada() {
        // Given
        List<Integer> ids = asList(1, 3, 2);

        // When
        List<Permissao> permissoes = repository.getPorIds(ids);

        // Then
        assertThat(permissoes, hasSize(3));
        for (Permissao p : permissoes) {
            assertThat(p.getId(), isIn(ids));
        }

        assertTrue(ids.stream().sorted().collect(Collectors.toList())
                .equals(permissoes.stream().sorted(Comparator.comparing(Permissao::getId))
                        .map(Permissao::getId).collect(Collectors.toList())));
    }

    @Test
    public void getPorNomes__retorna_da_lista_informada() {
        // Given
        List<String> nomesDosPerfis = Arrays.asList("CONSULTAR_USUARIOS","GERENCIAR_USUARIOS","CONSULTAR_PERMISSOES");

        // When
        List<Permissao> perfis = repository.getPorNomes(nomesDosPerfis);

        // Then
        assertThat(perfis, hasSize(3));
        for (Permissao p : perfis) {
            assertThat(p.getNome(), isIn(nomesDosPerfis));
        }

        assertTrue(nomesDosPerfis.stream().sorted().collect(Collectors.toList())
                      .equals(perfis.stream().sorted(Comparator.comparing(Permissao::getNome))
                                    .map(Permissao::getNome).collect(Collectors.toList())));
    }
}