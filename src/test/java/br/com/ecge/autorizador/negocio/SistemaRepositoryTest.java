package br.com.ecge.autorizador.negocio;

import infraestrutura.entitymanager.InjetarEntityManager;
import infraestrutura.entitymanager.InjetarEntityManagerRule;
import infraestrutura.entitymanager.InjetarEntityManagerRuleBuilder;
import infraestrutura.sqltemplate.InjetarSQLTemplate;
import infraestrutura.sqltemplate.InjetarSQLTemplateRule;
import infraestrutura.sqltemplate.InjetarSQLTemplateRuleBuilder;
import org.junit.Rule;
import org.junit.Test;

import java.util.List;

import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class SistemaRepositoryTest {

    @Rule
    public InjetarEntityManagerRule emRule = InjetarEntityManagerRuleBuilder.profilePadrao().build();

    @Rule
    public InjetarSQLTemplateRule sqlTemplateRule = InjetarSQLTemplateRuleBuilder.construirSQLTemplateRuleComTemplateDoH2();

    @InjetarSQLTemplate
    @InjetarEntityManager
    private static SistemaRepository repository = new SistemaRepository();


    @Test
    public void getPorTermo__deve_retornar_usuarios_que_tenham_o_termo_no_nome() {
        String termo = "ECGE";

        List<Sistema> resultados = repository.getPorTermo(termo);

        assertThat(resultados, hasSize(greaterThan(0)));

        for (Sistema o : resultados) {
            assertThat(o.getNome(), containsString(termo));
        }
    }

    @Test
    public void getPorIds__deve_recuperar_usuarios_com_os_ids_informados() {
        List<Integer> ids = asList(1, 2, 3);

        List<Sistema> resultados = repository.getPorIds(ids);

        assertThat(resultados, hasSize(3));

        for (Sistema s : resultados) {
            assertThat(s.getId(), isIn(ids));
        }
    }

}