package br.com.ecge.autorizador.negocio;

import org.junit.Test;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.Collections;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.*;

public class UsuarioTest {

    @Test
    public void getAutorithies__deve_retornar_lista_de_permissoes() {
        Usuario usuario = new Usuario();
        Permissao t = new Permissao();
        usuario.setPermissoes(Collections.singletonList(t));

        Collection<? extends GrantedAuthority> authorities = usuario.getAuthorities();

        assertThat(authorities.toArray()[0], is(t));
    }

    @Test
    public void getPassword__deve_ser_null_pois_nao_usamos() {
        Usuario usuario = new Usuario();

        assertNull(usuario.getPassword());
    }

    @Test
    public void getUsername__eh_getLogin() {
        Usuario usuario = new Usuario();
        usuario.setLogin("XXXXX");

        assertThat(usuario.getUsername(), is(usuario.getLogin()));
    }

    @Test
    public void metodos_da_interface__devem_ser_true() {
        Usuario usuario = new Usuario();

        assertTrue(usuario.isAccountNonExpired());
        assertTrue(usuario.isAccountNonLocked());
        assertTrue(usuario.isCredentialsNonExpired());
    }

    @Test
    public void isEnabled__inverso_da_flag_ativo() {
        Usuario usuario = new Usuario();
        usuario.setAtivo(false);
        assertFalse(usuario.isEnabled());

        usuario.setAtivo(true);
        assertTrue(usuario.isEnabled());
    }

    @Test
    public void toString_deve_retornar_login() {
        Usuario usuario = new Usuario();
        usuario.setLogin("XXXX");

        assertThat(usuario.getLogin(), is("XXXX"));
    }

    @Test
    public void hasPerfil__false_se_perfis_for_vazio() {
        Usuario usuario = new Usuario();

        assertFalse(usuario.hasPermissao(new Permissao()));
    }

    @Test
    public void hasPerfil__false_se_nao_tem_perfil_na_lista() {
        Usuario usuario = new Usuario();
        Permissao admin = new Permissao();
        admin.setId(1);

        Permissao outroPermissao = new Permissao();
        outroPermissao.setId(2);

        usuario.setPermissoes(Collections.singletonList(admin));

        assertFalse(usuario.hasPermissao(outroPermissao));
    }

    @Test
    public void hasPerfil__true_se_tem_perfil_na_lista() {
        Usuario usuario = new Usuario();
        Permissao admin = new Permissao();
        admin.setId(1);

        usuario.setPermissoes(Collections.singletonList(admin));

        assertTrue(usuario.hasPermissao(admin));
    }

    @Test
    public void sugerirChaveApi__deve_gerar_md5_diferente_a_cada_execucao_com_tamanho_32() throws Exception {
        // given
        Usuario usuario = new Usuario();
        usuario.setLogin("Login");
        
        String md5_a = usuario.sugerirChaveApi();
        Thread.sleep(1);
        String md5_b = usuario.sugerirChaveApi();
        
        assertThat(md5_a, not(equalTo(md5_b)));
        assertThat(md5_a.length(), is(32));
        assertThat(md5_b.length(), is(32));
    }


}