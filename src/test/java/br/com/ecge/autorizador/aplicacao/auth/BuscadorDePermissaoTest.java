package br.com.ecge.autorizador.aplicacao.auth;

import br.com.ecge.autorizador.aplicacao.BuscadorDePermissao;
import br.com.ecge.autorizador.aplicacao.PermissaoDTO;
import br.com.ecge.autorizador.aplicacao.PermissaoFiltro;
import br.com.ecge.autorizador.aplicacao.PermissaoQueryBuilder;
import br.com.ecge.autorizador.negocio.Permissao;
import br.com.ecge.autorizador.negocio.PermissaoRepository;
import br.com.ecge.autorizador.negocio.Sistema;
import infraestrutura.persistencia.querybuilder.RespostaConsulta;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class BuscadorDePermissaoTest {

    private PermissaoRepository repository = mock(PermissaoRepository.class);
    private PermissaoQueryBuilder queryBuilder= mock(PermissaoQueryBuilder.class);
    private BuscadorDePermissao buscador = new BuscadorDePermissao(repository, queryBuilder);

    @Test
    public void recuperarTodos__deve_retornar_resultado_do_repository_agrupado() {
        // given
        List<Permissao> perfis = new ArrayList<>();
        Sistema s1 = new Sistema();
        s1.setId(1);
        s1.setNome("Sistema 1");

        Sistema s2 = new Sistema();
        s1.setId(2);
        s1.setNome("Sistema 2");

        Permissao p1 = criarPermissao(1, "P1", s1);
        perfis.add(p1);
        Permissao p2 = criarPermissao(2, "P2", s1);
        perfis.add(p2);
        Permissao p3 = criarPermissao(3, "P3", s2);
        perfis.add(p3);
        Permissao p4 = criarPermissao(4, "P4", s2);
        perfis.add(p4);

        when(repository.getAll()).thenReturn(perfis);

        // when
        Map<Sistema, List<Permissao>> resposta = buscador.recuperarTodosPorSistema();

        // then
        assertThat(resposta.size(), is(2));
        assertThat(resposta.get(s1), hasItem(is(p1)));
        assertThat(resposta.get(s1), hasItem(is(p2)));
        assertThat(resposta.get(s2), hasItem(is(p3)));
        assertThat(resposta.get(s2), hasItem(is(p4)));
    }

    private Permissao criarPermissao(int id, String nome, Sistema s1) {
        Permissao p1 = new Permissao();
        p1.setId(id);
        p1.setNome(nome);
        p1.setSistema(s1);
        return p1;
    }


    @Test
    public void buscar__deve_retornar_resultado_do_qb() {
        PermissaoFiltro filtro = new PermissaoFiltro();
        RespostaConsulta<PermissaoDTO> respostaMock = new RespostaConsulta<>(Collections.emptyList(), 0L);
        when(queryBuilder.build(filtro)).thenReturn(respostaMock);

        RespostaConsulta<PermissaoDTO> resultado = buscador.buscar(filtro);

        assertThat(resultado, is(respostaMock));
    }

    @Test
    public void getParaEdicao__deve_retoranr_resultado_do_repositorio() {
        Permissao objeto = new Permissao();
        objeto.setId(132);
        when(repository.getEagerLoaded(132)).thenReturn(objeto);

        Permissao resultado = buscador.getParaEdicao(132);

        MatcherAssert.assertThat(resultado, Matchers.is(objeto));
    }

}