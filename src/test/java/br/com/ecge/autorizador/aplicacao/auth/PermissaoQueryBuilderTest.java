package br.com.ecge.autorizador.aplicacao.auth;

import br.com.ecge.autorizador.aplicacao.PermissaoDTO;
import br.com.ecge.autorizador.aplicacao.PermissaoFiltro;
import br.com.ecge.autorizador.aplicacao.PermissaoQueryBuilder;
import infraestrutura.QueryBuilderTest;
import infraestrutura.entitymanager.InjetarEntityManager;
import infraestrutura.entitymanager.InjetarEntityManagerRule;
import infraestrutura.entitymanager.InjetarEntityManagerRuleBuilder;
import infraestrutura.persistencia.querybuilder.QueryBuilderJPASQL;
import org.junit.Rule;
import org.junit.Test;

import java.util.List;

import static infraestrutura.AssertUtils.assertListaOrdenadaPorPropriedade;
import static org.apache.commons.lang3.ObjectUtils.compare;

public class PermissaoQueryBuilderTest extends QueryBuilderTest<PermissaoFiltro, PermissaoDTO>{

    @Rule
    public InjetarEntityManagerRule emRule = InjetarEntityManagerRuleBuilder.profilePadrao().build();

    @InjetarEntityManager
    private PermissaoQueryBuilder queryBuilder = new PermissaoQueryBuilder();

    @Override
    public QueryBuilderJPASQL<PermissaoFiltro, PermissaoDTO> getQueryBuilder() {
        return queryBuilder;
    }

    @Override
    public Class<PermissaoFiltro> getFiltroClass() {
        return PermissaoFiltro.class;
    }

    @Test
    public void testarCombinacoes() {
        testarQuery("qualquerCoisa", criarFiltro(null, null), 9);
        testarQuery("qualquerCoisa", criarFiltro(new String[]{"nome"}, new Object[]{"CONSULTA"}), 3);
        testarQuery("qualquerCoisa", criarFiltro(new String[]{"sistema"}, new Object[]{1}), 6);
        testarQuery("qualquerCoisa", criarFiltro(new String[]{"ativo"}, new Object[]{true}), 8);

        testarQuery("id", criarFiltro(null, null), 9);
        testarQuery("nome", criarFiltro(null, null), 9);
        testarQuery("sistema", criarFiltro(null, null), 9);
    }

    @Override
    protected void assertOrdenacao(List<PermissaoDTO> resultado, String colunaOrdenacao) {
        assertListaOrdenadaPorPropriedade(resultado, (o1, o2) -> {
            switch (colunaOrdenacao) {
                case "nome" : return compare(o2.getNome().toLowerCase(), o1.getNome().toLowerCase());
                case "sistema" : return compare(o2.getSistema().toLowerCase(), o1.getSistema().toLowerCase());
                default : return compare(o2.getId(), o1.getId());
            }
        });
    }
}