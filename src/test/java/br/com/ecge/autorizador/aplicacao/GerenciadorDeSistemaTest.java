package br.com.ecge.autorizador.aplicacao;

import br.com.ecge.autorizador.negocio.Sistema;
import br.com.ecge.autorizador.negocio.SistemaRepository;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GerenciadorDeSistemaTest {

    private final SistemaRepository repository = mock(SistemaRepository.class);

    private GerenciadorDeSistema gerenciador = new GerenciadorDeSistema(repository);

    @Test
    public void salvar__deve_retornar_resultdo_do_repository() throws Exception {
        Sistema sistema = new Sistema();
        sistema.setId(1);

        when(repository.put(any())).thenReturn(sistema);

        Sistema resultado = gerenciador.salvar(sistema);

        assertThat(resultado, is(sistema));
    }


    @Test
    public void toggleAtivo__deve_inverter_flg_ativo() {
        Sistema p = new Sistema();
        p.setAtivo(true);

        when(repository.get(123)).thenReturn(p);
        when(repository.put(any())).thenAnswer(invocation -> {
            assertThat(((Sistema)invocation.getArguments()[0]).isAtivo(), is(false));
            return null;
        });

        gerenciador.alternarAtivacao(123);
    }

    @Test
    public void toggleAtivo__deve_inverter_flg_inativo() {
        Sistema p = new Sistema();
        p.setAtivo(false);

        when(repository.get(123)).thenReturn(p);
        when(repository.put(any())).thenAnswer(invocation -> {
            assertThat(((Sistema)invocation.getArguments()[0]).isAtivo(), is(true));
            return null;
        });

        gerenciador.alternarAtivacao(123);
    }

}