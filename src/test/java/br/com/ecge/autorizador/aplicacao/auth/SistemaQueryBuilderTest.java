package br.com.ecge.autorizador.aplicacao.auth;

import br.com.ecge.autorizador.aplicacao.SistemaDTO;
import br.com.ecge.autorizador.aplicacao.SistemaQueryBuilder;
import br.com.ecge.autorizador.web.SistemaFiltro;
import infraestrutura.QueryBuilderTest;
import infraestrutura.entitymanager.InjetarEntityManager;
import infraestrutura.entitymanager.InjetarEntityManagerRule;
import infraestrutura.entitymanager.InjetarEntityManagerRuleBuilder;
import infraestrutura.persistencia.querybuilder.QueryBuilderJPASQL;
import org.junit.Rule;
import org.junit.Test;

import java.util.List;

import static infraestrutura.AssertUtils.assertListaOrdenadaPorPropriedade;

public class SistemaQueryBuilderTest extends QueryBuilderTest<SistemaFiltro, SistemaDTO> {

    private static final int MAXIMO_REGISTROS_NO_DADOS = 4;

    @Rule
    public InjetarEntityManagerRule emRule = InjetarEntityManagerRuleBuilder.profilePadrao().build();

    @InjetarEntityManager
    private SistemaQueryBuilder queryBuilder = new SistemaQueryBuilder();

    @Test
    public void testarCombinacoes() {
        testarQuery("id", criarSistemaFiltro("Autorizador", null), 1);
        testarQuery("id", criarSistemaFiltro(null,  true), 3);
        testarQuery("id", criarSistemaFiltro(null, false), 1);

        testarQuery("id", criarSistemaFiltro(null, null), MAXIMO_REGISTROS_NO_DADOS);
        testarQuery("nome", criarSistemaFiltro(null, null), MAXIMO_REGISTROS_NO_DADOS);
    }

    private SistemaFiltro criarSistemaFiltro(String nome, Boolean ativos) {
        SistemaFiltro u = new SistemaFiltro();

        u.setNome(nome);
        u.setAtivo(ativos);

        return u;
    }


    @Override
    protected void assertOrdenacao(List<SistemaDTO> resultado, String colunaOrdenacao) {
        assertListaOrdenadaPorPropriedade(resultado, (o1, o2) -> {
            switch (colunaOrdenacao) {
                case "nome" : return o2.getNome().compareToIgnoreCase(o1.getNome());
                default : return o2.getId().compareTo(o1.getId());
            }
        });
    }

    @Override
    public QueryBuilderJPASQL<SistemaFiltro, SistemaDTO> getQueryBuilder() {
        return queryBuilder;
    }

    @Override
    public Class<SistemaFiltro> getFiltroClass() {
        return SistemaFiltro.class;
    }
}