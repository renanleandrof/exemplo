package br.com.ecge.autorizador.aplicacao.auth;

import br.com.ecge.autorizador.aplicacao.UsuarioDTO;
import br.com.ecge.autorizador.aplicacao.UsuarioFiltro;
import br.com.ecge.autorizador.aplicacao.UsuarioQueryBuilder;
import infraestrutura.QueryBuilderTest;
import infraestrutura.entitymanager.InjetarEntityManager;
import infraestrutura.entitymanager.InjetarEntityManagerRule;
import infraestrutura.entitymanager.InjetarEntityManagerRuleBuilder;
import infraestrutura.persistencia.querybuilder.QueryBuilderJPASQL;
import org.junit.Rule;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

import static infraestrutura.AssertUtils.assertListaOrdenadaPorPropriedade;

public class UsuarioQueryBuilderTest extends QueryBuilderTest<UsuarioFiltro, UsuarioDTO> {
    private static final int MAXIMO_REGISTROS_NO_DADOS = 26;

    @Rule
    public InjetarEntityManagerRule emRule = InjetarEntityManagerRuleBuilder.profilePadrao().build();

    @InjetarEntityManager
    private UsuarioQueryBuilder queryBuilder = new UsuarioQueryBuilder();

    @Test
    public void testarCombinacoes() {
    	testarQuery("id", criarFiltro("Teste ADMIN 1", null, null, null), 1);
        testarQuery("id", criarFiltro(null, "ggomest", null, null), 1);
        testarQuery("id", criarFiltro(null, null, Collections.singletonList(2), null), 7);
        testarQuery("id", criarFiltro(null, null, null, true), 25);
        testarQuery("id", criarFiltro(null, null, null, false), 1);

        testarQuery("id", criarFiltro(null, null, null, null), MAXIMO_REGISTROS_NO_DADOS);
        testarQuery("cpf", criarFiltro(null, null, null, null), MAXIMO_REGISTROS_NO_DADOS);
        testarQuery("nome", criarFiltro(null, null, null, null), MAXIMO_REGISTROS_NO_DADOS);
        testarQuery("login", criarFiltro(null, null, null, null), MAXIMO_REGISTROS_NO_DADOS);
    }

    private UsuarioFiltro criarFiltro(String nome, String login, List<Integer> perfis, Boolean ativos) {
        UsuarioFiltro u = new UsuarioFiltro();

        u.setNome(nome);
        u.setLogin(login);
        u.setPermissoes(perfis);
        u.setAtivo(ativos);

        return u;
    }


    @Override
    protected void assertOrdenacao(List<UsuarioDTO> resultado, String colunaOrdenacao) {
        assertListaOrdenadaPorPropriedade(resultado, (o1, o2) -> {
            switch (colunaOrdenacao) {
                case "nome" : return o2.getNome().compareToIgnoreCase(o1.getNome());
                case "login" : return o2.getLogin().compareToIgnoreCase(o1.getLogin());
                default : return o2.getId().compareTo(o1.getId());
            }
        });
    }

    @Override
    public QueryBuilderJPASQL<UsuarioFiltro, UsuarioDTO> getQueryBuilder() {
        return queryBuilder;
    }

    @Override
    public Class<UsuarioFiltro> getFiltroClass() {
        return UsuarioFiltro.class;
    }
}