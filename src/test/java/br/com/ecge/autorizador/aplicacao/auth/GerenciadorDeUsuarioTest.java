package br.com.ecge.autorizador.aplicacao.auth;

import br.com.ecge.autorizador.aplicacao.BuscadorDeUsuario;
import br.com.ecge.autorizador.aplicacao.GerenciadorDeUsuario;
import br.com.ecge.autorizador.aplicacao.UsuarioForm;
import br.com.ecge.autorizador.aplicacao.UsuarioLogadoForm;
import br.com.ecge.autorizador.negocio.Permissao;
import br.com.ecge.autorizador.negocio.PermissaoRepository;
import br.com.ecge.autorizador.negocio.Usuario;
import br.com.ecge.autorizador.negocio.UsuarioRepository;
import infraestrutura.PasswordUtils;
import infraestrutura.persistencia.jpa.EntidadeInvalidaException;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.List;

import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GerenciadorDeUsuarioTest {

    private UsuarioRepository usuarioRepository = mock(UsuarioRepository.class);
    private PermissaoRepository permissaoRepository = mock(PermissaoRepository.class);
    private BuscadorDeUsuario buscadorDeUsuario = mock(BuscadorDeUsuario.class);

    private GerenciadorDeUsuario gerenciador = new GerenciadorDeUsuario(usuarioRepository, permissaoRepository, buscadorDeUsuario);

    @Test(expected = EntidadeInvalidaException.class)
    public void salvar_deve_lancar_exception_se_entidade_for_invalida() {
        Usuario usuario = mock(Usuario.class);
        when(buscadorDeUsuario.getUsuarioLogado()).thenReturn(usuario);
        when(usuarioRepository.put(any())).thenThrow(new EntidadeInvalidaException(""));

        gerenciador.salvar(new UsuarioForm());
    }

    @Test(expected = EntidadeInvalidaException.class)
    public void salvar_deve_lancar_exception_se_login_de_usuario_ja_estiver_cadastrado() {
        Integer id = 123;
        Usuario usuario = mock(Usuario.class);
        usuario.setId(id);
        UsuarioForm usuarioForm = criarUsuarioForm(id);

        when(usuarioRepository.getPorLogin(any())).thenReturn(usuario);

        gerenciador.salvar(usuarioForm);
    }

    @Test
    public void salvar__deve_retornar_usuario_do_repositorio_ao_se_salvar_novo_usuario_com_sucesso() {
        UsuarioForm usuarioForm = criarUsuarioForm(null);
        Usuario usuarioLogado = mock(Usuario.class);
        
        when(buscadorDeUsuario.getUsuarioLogado()).thenReturn(usuarioLogado);
        when(permissaoRepository.getPorIds(any())).thenAnswer(GerenciadorDeUsuarioTest::perfis);
        
        when(usuarioRepository.put(any())).thenAnswer(invocationOnMock -> {
            Usuario user = invocationOnMock.getArgument(0);
            user.setId(123);
            return user;  
        });

        Usuario resultado = gerenciador.salvar(usuarioForm);

        assertThat(resultado.getId(), is(123));
        assertThat(resultado.getEmail(), is(usuarioForm.getEmail()));
        assertThat(resultado.getLogin(), is(usuarioForm.getLogin()));
        assertThat(resultado.getNome(), is(usuarioForm.getNome()));

        assertThat(resultado.getPermissoes(), hasSize(usuarioForm.getPermissoes().size()));
        resultado.getPermissoes().forEach(p -> assertThat(p.getId(), isIn(usuarioForm.getPermissoes())));
    }

    @Test
    public void salvar__deve_retornar_usuario_do_repositorio_ao_se_salvar_usuario_ja_existente_com_sucesso() {
        UsuarioForm usuarioForm = criarUsuarioForm(123);
        Usuario usuarioLogado = mock(Usuario.class);
        Usuario usuarioJaExistenteNoBD = new Usuario();
        usuarioJaExistenteNoBD.setId(123);
        
        when(buscadorDeUsuario.getUsuarioLogado()).thenReturn(usuarioLogado);
        when(buscadorDeUsuario.getParaEdicao(any())).thenReturn(usuarioJaExistenteNoBD);
        when(permissaoRepository.getPorIds(any())).thenAnswer(GerenciadorDeUsuarioTest::perfis);
        
        when(usuarioRepository.put(any())).thenAnswer(
                invocationOnMock -> invocationOnMock.getArgument(0));

        Usuario resultado = gerenciador.salvar(usuarioForm);

        assertThat(resultado.getId(), is(usuarioForm.getId()));
        assertThat(resultado.getEmail(), is(usuarioForm.getEmail()));
        assertThat(resultado.getLogin(), is(usuarioForm.getLogin()));
        assertThat(resultado.getNome(), is(usuarioForm.getNome()));

        assertThat(resultado.getPermissoes(), hasSize(usuarioForm.getPermissoes().size()));
        resultado.getPermissoes().forEach(p -> assertThat(p.getId(), isIn(usuarioForm.getPermissoes())));

    }


    @Test
    public void alterarDadosUsuarioLogado__deve_alterar_dados_do_usuario_logado() {
        Usuario usuarioLogado = new Usuario();
        usuarioLogado.setEmail("emailVelho@cgu.gov.br");
        usuarioLogado.setChaveApi("chaveApiVelha");

        UsuarioLogadoForm usuarioLogadoForm = criarUsuarioLogadoForm();

        when(buscadorDeUsuario.getUsuarioLogado()).thenReturn(usuarioLogado);

        gerenciador.alterarDadosUsuarioLogado(usuarioLogadoForm);

        assertThat(usuarioLogado.getEmail(), is("emailNovo@cgu.gov.br"));
        assertThat(usuarioLogado.getChaveApi(), is(PasswordUtils.hashMD5("chaveApiNova")));
    }

    @Test
    public void alterarDadosUsuarioLogado__deve_alterar_senha_se_campo_vier() {
        Usuario usuarioLogado = new Usuario();
        usuarioLogado.setEmail("emailVelho@cgu.gov.br");
        String senhaAntiga = new BCryptPasswordEncoder().encode("AAAAAAAAAAAAA");
        usuarioLogado.setSenha(senhaAntiga);

        UsuarioLogadoForm form = criarUsuarioLogadoForm();
        form.setSenhaAtual("AAAAAAAAAAAAA");
        form.setNovaSenha("abcd123");

        when(buscadorDeUsuario.getUsuarioLogado()).thenReturn(usuarioLogado);

        gerenciador.alterarDadosUsuarioLogado(form);

        assertThat(usuarioLogado.getEmail(), is("emailNovo@cgu.gov.br"));
        assertThat(usuarioLogado.getSenha(), is(not(senhaAntiga)));
    }

    @Test(expected = EntidadeInvalidaException.class)
    public void alterarDadosUsuarioLogado__deve_lancar_exception_se_senhaAtual_nao_bater() {
        Usuario usuarioLogado = new Usuario();
        usuarioLogado.setEmail("emailVelho@cgu.gov.br");
        String senhaAntiga = new BCryptPasswordEncoder().encode("AAAAAAAAAAAAA");
        usuarioLogado.setSenha(senhaAntiga);

        UsuarioLogadoForm form = criarUsuarioLogadoForm();
        form.setSenhaAtual("SENHA_DIFERENTE");
        form.setNovaSenha("abcd123");

        when(buscadorDeUsuario.getUsuarioLogado()).thenReturn(usuarioLogado);

        gerenciador.alterarDadosUsuarioLogado(form);
    }

    @Test
    public void toggleAtivo__deve_inverter_flg_ativo() {
        Usuario usuarioLogado = mock(Usuario.class);
        Usuario usuarioDoRep = new Usuario();
        usuarioDoRep.setAtivo(true);

        when(usuarioRepository.get(123)).thenReturn(usuarioDoRep);
        when(buscadorDeUsuario.getUsuarioLogado()).thenReturn(usuarioLogado);
        when(usuarioRepository.put(any())).thenAnswer(invocation -> {
            assertThat(((Usuario)invocation.getArguments()[0]).isAtivo(), is(false));
            return null;
        });

        gerenciador.alternarAtivacao(123);
    }

    @Test
    public void toggleAtivo__deve_inverter_flg_inativo() {
        Usuario usuarioLogado = mock(Usuario.class);
        Usuario usuarioDoRep = new Usuario();
        usuarioDoRep.setAtivo(false);

        when(usuarioRepository.get(123)).thenReturn(usuarioDoRep);
        when(buscadorDeUsuario.getUsuarioLogado()).thenReturn(usuarioLogado);
        when(usuarioRepository.put(any())).thenAnswer(invocation -> {
            assertThat(((Usuario)invocation.getArguments()[0]).isAtivo(), is(true));
            return null;
        });

        gerenciador.alternarAtivacao(123);
    }

    private UsuarioForm criarUsuarioForm(Integer idUsuario) {
        UsuarioForm usuarioForm = new UsuarioForm();
        usuarioForm.setId(idUsuario);
        usuarioForm.setEmail("mail@cgu.gov.br");
        usuarioForm.setLogin("CGU\\test_user");
        usuarioForm.setNome("Test User da Silva");
        List<Integer> idPerfis = asList(1, 2);
        usuarioForm.setPermissoes(idPerfis);
        return usuarioForm;
    }

    private static Object perfis(InvocationOnMock invocationOnMock) {
        return asList(new Permissao(1, "1"), new Permissao(2, "1"));
    }

    private UsuarioLogadoForm criarUsuarioLogadoForm() {
        UsuarioLogadoForm usuarioLogadoForm = new UsuarioLogadoForm();
        usuarioLogadoForm.setEmailUsuarioLogado("emailNovo@cgu.gov.br");
        usuarioLogadoForm.setChaveApiUsuarioLogado("chaveApiNova");

        return usuarioLogadoForm;
    }


}