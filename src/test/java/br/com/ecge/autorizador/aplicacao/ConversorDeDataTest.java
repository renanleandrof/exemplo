package br.com.ecge.autorizador.aplicacao;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import infraestrutura.DateUtils;
import org.junit.Test;

import java.io.IOException;
import java.time.LocalDate;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ConversorDeDataTest {
    private final JsonParser jsonParser = mock(JsonParser.class);
    private final DeserializationContext deserializationContext = mock(DeserializationContext.class);
    private final ConversorDeData conversorDeData = new ConversorDeData();

    @Test
    public void deserialize__converter_data_formato_brasileiro() throws IOException {
        String strData = "25/10/2018";

        when(jsonParser.getText()).thenReturn(strData);
        LocalDate data = conversorDeData.deserialize(jsonParser, deserializationContext);

        assertEquals(data, DateUtils.parseLocalDate(strData, DateUtils.DDMMYYYY_FORMATTER));
    }

    @Test
    public void deserialize__converter_data_formato_americano() throws IOException {
        String strData = "2018-10-31";

        when(jsonParser.getText()).thenReturn(strData);
        LocalDate data = conversorDeData.deserialize(jsonParser, deserializationContext);

        assertEquals(data, DateUtils.parseLocalDate(strData, DateUtils.YYYYMMDD_FORMATTER));
    }

    @Test
    public void deserialize__converter_data_vazia() throws IOException {
        when(jsonParser.getText()).thenReturn("");
        LocalDate data = conversorDeData.deserialize(jsonParser, deserializationContext);

        assertNull(data);
    }

    @Test(expected = ConversaoDeDataException.class)
    public void deserialize__lanca_exception_erro_no_formato_da_data() throws IOException {
        String strData = "15-12-2018";

        when(jsonParser.getText()).thenReturn(strData);
        when(jsonParser.getCurrentName()).thenReturn("dataInicio");
        conversorDeData.deserialize(jsonParser, deserializationContext);
    }
}