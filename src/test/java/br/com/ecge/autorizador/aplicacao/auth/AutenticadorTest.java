package br.com.ecge.autorizador.aplicacao.auth;

import br.com.ecge.autorizador.aplicacao.Autenticador;
import br.com.ecge.autorizador.aplicacao.CredenciaisInvalidasException;
import br.com.ecge.autorizador.negocio.Permissao;
import br.com.ecge.autorizador.negocio.Usuario;
import br.com.ecge.autorizador.negocio.UsuarioInativoException;
import br.com.ecge.autorizador.negocio.UsuarioRepository;
import org.junit.Test;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.ArrayList;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AutenticadorTest {

    private final UsuarioRepository usuarioRepository = mock(UsuarioRepository.class);

    private final Autenticador autenticador = new Autenticador(usuarioRepository);

    @Test(expected = CredenciaisInvalidasException.class)
    public void authenticate__deve_lancar_exception_quando_credenciais_invalidas() {
        Authentication authenticationMock  = mock(Authentication.class);

        when(authenticationMock.getName()).thenReturn("login");
        when(usuarioRepository.getPorLogin("login")).thenReturn(null);

        autenticador.authenticate(authenticationMock);
    }

    @Test(expected = UsuarioInativoException.class)
    public void authenticate__deve_lancar_exception_quando_usuario_inativo() {
        Authentication authenticationMock  = mock(Authentication.class);
        Usuario usuarioMock = mock(Usuario.class);

        when(authenticationMock.getName()).thenReturn("login");
        when(usuarioRepository.getPorLogin("login")).thenReturn(usuarioMock);
        when(usuarioMock.isAtivo()).thenReturn(false);

        autenticador.authenticate(authenticationMock);
    }

    @Test
    public void authenticate__deve_autenticar_usuario() {
        // Given
        Authentication authenticationMock  = mock(Authentication.class);

        Usuario usuario = mockUsuario();

        when(authenticationMock.getName()).thenReturn("login");
        when(usuarioRepository.getPorLogin("login")).thenReturn(usuario);
        when(usuarioRepository.put(usuario)).thenReturn(usuario);

        // When
        Authentication auth = autenticador.authenticate(authenticationMock);

        // Then
        assertThat(auth.getName(), is("login"));
        assertThat(auth.getCredentials(), is(""));
        assertThat(auth.getAuthorities(), hasSize(1));
    }

    @Test
    public void recarregar_informacoes_do_usuario_autenticado() {
        Authentication authenticationMock  = mock(Authentication.class);
        SecurityContext securityContext = SecurityContextHolder.getContext();
        securityContext.setAuthentication(authenticationMock);

        when(authenticationMock.isAuthenticated()).thenReturn(true);

        autenticador.recarregarInformacoesDoUsuarioAutenticado(mockUsuario());
    }

    private Usuario mockUsuario() {
        Usuario usuario = new Usuario();
        usuario.setLogin("login");
        usuario.setAtivo(true);
        usuario.setPermissoes(new ArrayList<>());
        Permissao admin = new Permissao(1, "Admin");
        usuario.getPermissoes().add(admin);
        return usuario;
    }
}