package br.com.ecge.autorizador.aplicacao.auth;

import br.com.ecge.autorizador.aplicacao.BuscadorDeSistema;
import br.com.ecge.autorizador.aplicacao.SistemaDTO;
import br.com.ecge.autorizador.aplicacao.SistemaQueryBuilder;
import br.com.ecge.autorizador.negocio.Sistema;
import br.com.ecge.autorizador.negocio.SistemaRepository;
import br.com.ecge.autorizador.web.SistemaFiltro;
import infraestrutura.persistencia.querybuilder.RespostaConsulta;
import org.junit.Test;

import java.util.Collections;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class BuscadorDeSistemaTest {

    private SistemaQueryBuilder queryBuilder = mock(SistemaQueryBuilder.class);
    private SistemaRepository repository = mock(SistemaRepository.class);

    private BuscadorDeSistema buscadorDeSistema = new BuscadorDeSistema(queryBuilder, repository);

    @Test
    public void buscar__deve_retornar_resultado_do_querybuilder() {
        SistemaFiltro filtro = new SistemaFiltro();
        RespostaConsulta<SistemaDTO> mockresultado = new RespostaConsulta<>(Collections.emptyList(), 0L);
        when(queryBuilder.build(filtro)).thenReturn(mockresultado);

        RespostaConsulta<SistemaDTO> resultado = buscadorDeSistema.buscar(filtro);

        assertThat(resultado, is(mockresultado));
    }

    @Test
    public void getParaEdicao__deve_retornar_resultado_do_repository() {
        Sistema mockresultado = new Sistema();
        when(repository.getEagerLoaded(123)).thenReturn(mockresultado);

        Sistema resultado = buscadorDeSistema.getParaEdicao(123);

        assertThat(resultado, is(mockresultado));
    }
}