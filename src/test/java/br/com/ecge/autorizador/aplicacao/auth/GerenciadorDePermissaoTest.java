package br.com.ecge.autorizador.aplicacao.auth;

import br.com.ecge.autorizador.aplicacao.GerenciadorDePermissao;
import br.com.ecge.autorizador.negocio.Permissao;
import br.com.ecge.autorizador.negocio.PermissaoRepository;
import br.com.ecge.autorizador.negocio.Sistema;
import br.com.ecge.autorizador.negocio.SistemaRepository;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GerenciadorDePermissaoTest {

    private final PermissaoRepository repository = mock(PermissaoRepository.class);
    private final SistemaRepository sistemaRepository = mock(SistemaRepository.class);

    private GerenciadorDePermissao gerenciadorDePermissao = new GerenciadorDePermissao(repository, sistemaRepository);

    @Test
    public void salvar__deve_retornar_resultdo_do_repository() throws Exception {
        Permissao permissao = new Permissao();
        Sistema sistema = new Sistema();
        sistema.setId(1);
        permissao.setSistema(sistema);
        Permissao permissaoRetorno = new Permissao();

        when(sistemaRepository.get(anyInt())).thenReturn(new Sistema());
        when(repository.put(permissao)).thenReturn(permissaoRetorno);

        Permissao resultado = gerenciadorDePermissao.salvar(permissao);

        assertThat(resultado, is(permissaoRetorno));
    }


    @Test
    public void toggleAtivo__deve_inverter_flg_ativo() {
        Permissao p = new Permissao();
        p.setAtivo(true);

        when(repository.get(123)).thenReturn(p);
        when(repository.put(any())).thenAnswer(invocation -> {
            assertThat(((Permissao)invocation.getArguments()[0]).isAtivo(), is(false));
            return null;
        });

        gerenciadorDePermissao.alternarAtivacao(123);
    }

    @Test
    public void toggleAtivo__deve_inverter_flg_inativo() {
        Permissao p = new Permissao();
        p.setAtivo(false);

        when(repository.get(123)).thenReturn(p);
        when(repository.put(any())).thenAnswer(invocation -> {
            assertThat(((Permissao)invocation.getArguments()[0]).isAtivo(), is(true));
            return null;
        });

        gerenciadorDePermissao.alternarAtivacao(123);
    }
}