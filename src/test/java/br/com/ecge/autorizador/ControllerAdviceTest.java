package br.com.ecge.autorizador;

import br.com.ecge.autorizador.aplicacao.BuscadorDeUsuario;
import br.com.ecge.autorizador.web.ControllerAdvice;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.Test;
import org.springframework.orm.jpa.JpaSystemException;
import org.springframework.ui.ExtendedModelMap;

import java.io.IOException;
import java.sql.SQLException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.mock;

public class ControllerAdviceTest {
    private BuscadorDeUsuario usuarioService = mock(BuscadorDeUsuario.class);
    private ControllerAdvice controller = new ControllerAdvice(usuarioService);

    @Test
    public void naoEncontrada_deve_retornar_view() {
        String modelAndView = controller.naoEncontrada();

        assertThat(modelAndView, is("error/404"));
    }

    @Test
    public void erroDeIntegridadeReferencial__deve_retornar_integridade_referencial() {
        String modelAndView = controller.erroDeIntegridadeReferencial(new JpaSystemException( new RuntimeException(new ConstraintViolationException(null, new SQLException("Teste"), null))));

        assertThat(modelAndView, is("Não é possível excluir um elemento já associado a outra entidade no sistema."));
    }

    @Test
    public void erroDeIntegridadeReferencial__deve_retornar_500() {
        String modelAndView = controller.erroDeIntegridadeReferencial(new JpaSystemException( new RuntimeException("xpto")));

        assertThat(modelAndView, is("xpto; nested exception is java.lang.RuntimeException: xpto"));
    }

    @Test
    public void erroDeIO__broken_pipe() {
        String modelAndView = controller.erroDeIO(new ExtendedModelMap(), new IOException("java.lang.RuntimeException: java.io.IOException: Broken pipe"));

        assertThat(modelAndView, is("error/index"));
    }

    @Test
    public void erroDeIO__connection_reset_by_peer() {
        String modelAndView = controller.erroDeIO(new ExtendedModelMap(), new IOException("java.lang.RuntimeException: java.io.IOException: Connection reset by peer"));

        assertThat(modelAndView, is("error/index"));
    }
}