package br.com.ecge.autorizador;

import br.com.ecge.autorizador.aplicacao.BuscadorDeUsuario;
import br.com.ecge.autorizador.negocio.Usuario;
import br.com.ecge.autorizador.web.HomeController;
import infraestrutura.mvc.ControllerTest;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

public class HomeControllerTest extends ControllerTest {

    private BuscadorDeUsuario buscador = mock(BuscadorDeUsuario.class);
    private HomeController controller = new HomeController(buscador);

    @Override
    public Object getController() {
        return controller;
    }

    @Test
    public void paginaInicial_retorna_a_home_do_portal() throws Exception {
        // given
        // when
        mockMvc.perform(get(""))
               // then
               .andExpect(status().isOk())
               .andExpect(view().name("index/index"));
    }

    @Test
    public void paginaInicial_redireciona_pra_auth_se_ja_estiver_logado() throws Exception {
        // given
        when(buscador.getUsuarioLogado()).thenReturn(new Usuario());
        // when
        mockMvc.perform(get(""))
                // then
                .andExpect(status().is3xxRedirection());
    }

}