#Projeto Exemplo

##Rodar localmenet:
Compilar com maven:
``mvn clean compile``

Rodar a aplicação:
``java -jar autorizador.jar -Dspring.profiles.active=h2,swagger``

##Rodar no Postgres

- *(Opcional)* Levantar um PostgresSQL local no docker:
``docker run -p 127.0.0.1:5432:5432 --name pgsql -d  -e POSTGRES_PASSWORD=123 postgres:latest``

Atualizar as urls e usuario do banco nos dois comandos abaixo:

- Compilar no maven e rodar o flyway 
``mvn clean compile flyway:migration package``
É possivel sobrescrever os dados do banco com: ``mvn clean compile flyway:migration package --Dflyway.url=jdbc:postgresql://127.0.0.1:5432/postgres --Dflyway.user=postgres --Dflyway.password=123``

- Rodar a aplicação:
``java -jar autorizador.jar -Dspring.profiles.active=postgres,swagger --bd.url=jdbc:postgresql://127.0.0.1:5432/postgres --bd.user=postgres --bd.pwd=123``

##Rodar no Docker

Alterar a última linha do Dockerfile para o comando java -jar adequado com suas URL's e profiles Spring.

## Profiles Spring disponíveis:

- **swagger**: habilita a documentação da API usando o swagger
- **notificar-erros**: habilita o envio de exceptions por email, conforme configuração do logback-spring.xml
- **h2**: usa datasource h2 executando dados.sql e esquema.sql na pasta resources.
- **postgres**: usa datasource Postgres
